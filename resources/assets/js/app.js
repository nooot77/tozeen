
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

<<<<<<< HEAD
import Vue from 'vue/dist/vue.min.js'
=======

import Vue from 'vue/dist/vue.min.js';
>>>>>>> remotes/origin/restore
import VueInternationalization from 'vue-i18n';
import Locales from './vue-i18n-locales.generated';
import Vualidate from 'vuelidate'

Vue.use(VueInternationalization);

Vue.use(Vualidate)







// Register it globally



const lang = document.documentElement.lang.substr(0, 2); 

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locales,
});


import Snotify from 'vue-snotify';
Vue.use(Snotify);

import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

// import 'vue-select/dist/vue-select.css';


import VuejsDialog from "vuejs-dialog"
Vue.use(VuejsDialog)


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



Vue.component('po-index-component', require('./components/PurchaseOrders/index.vue'));
Vue.component('po-create-component', require('./components/PurchaseOrders/create.vue'));
Vue.component('po-edit-component', require('./components/PurchaseOrders/edit.vue'));

Vue.component('sales-index-component', require('./components/Sales/index.vue'));
Vue.component('sales-create-component', require('./components/Sales/create.vue'));
Vue.component('sales-edit-component', require('./components/Sales/edit.vue'));


Vue.component('sales-bills-index-component', require('./components/SalesBills/index.vue'));
Vue.component('sales-bills-create-component', require('./components/SalesBills/create.vue'));
Vue.component('sales-bills-edit-component', require('./components/SalesBills/edit.vue'));

Vue.component('daily-sales-bills-index-component', require('./components/DailySalesBills/index.vue'));
Vue.component('daily-sales-bills-create-component', require('./components/DailySalesBills/create.vue'));
Vue.component('daily-sales-bills-edit-component', require('./components/DailySalesBills/edit.vue'));

Vue.component('buy-orders-index-component', require('./components/BuyOrders/index.vue'));
Vue.component('buy-orders-create-component', require('./components/BuyOrders/create.vue'));
Vue.component('buy-orders-edit-component', require('./components/BuyOrders/edit.vue'));

Vue.component('product-stocks-component', require('./components/productStocks/create.vue'));


Vue.config.productionTip = false;

const app = new Vue({
    el: '#app',
    i18n,
    // render: h => h(app)
});

