// Import axios
import axios from 'axios';
var uriv = window.location.href;


var admin = uriv.indexOf('/admin');
var url = uriv.slice(0,admin+6);






export const homeUrl = (target="") => {
    return url + target;
}

export const getAllSales = () => {

    return axios.get(url+'/sales-orders/all');
}
// Get All PO
export const getAllPO = () => {
    return axios.get(url+'/purchases-orders/all');
}

// Get Order
export const getOrder = (id) => {
    return axios.get(url+'/purchases-orders/getOrder/'+id);
}

// Place new order
export const placeNewOrder = (data) => {
    return axios.post('/purchases-orders/create',data);
}

// Update order
export const updateAction = (id,data) => {
    return axios.put(url+'/purchases-orders/updateAction/'+id,data);
}

// delete item
export const removeItemFromOrder = (id) => {
    return axios.delete(url+'/purchases-orders/removeItemFromOrder/'+id);
}

// Sales

// Get All PO


// Get Order
export const getSalesOrder = (id) => {
    return axios.get(url+'/sales-orders/getOrder/'+id)
}

// Place new order
export const placeNewSalesOrder = (data) => {
    return axios.post(url+'/sales-orders/create/',data)
    
}

// Update order
export const updateSalesAction = (id,data) => {
    return axios.put(url+'/sales-orders/updateAction/'+id,data) 
}

// delete item
export const removeItemFromSalesOrder = (id) => {
    return axios.delete(url+'/sales-orders/removeItemFromOrder/'+id)
}


// Get All PO
export const getAllSB = () => {
    return axios.get(url+'/sales-bills/all')
}

// Get Order
export const getSalesBill = (id) => {
    return axios.get(url+'/sales-bills/getOrder/'+id)
}

// Place new order
export const placeNewSalesBill = (data) => {
    console.log(url+'/sales-bills/create');
    return axios.post(url+'/sales-bills/create',data).then(function (response)
    {
       if(response.status == 200) {
           return window.location.href = url + "/sales-bills/orderlist";
       };
    });

}

// Update order
export const updateSalesBillAction = (id,data) => {
    return axios.put(url+'/sales-bills/updateAction/'+id,data)
}

// delete item
export const removeItemFromSalesBill = (id) => {
    return axios.delete(url+'/sales-bills/removeItemFromOrder/'+id)
}





// Get All PO
export const getAllDSB = () => {
    return axios.get(url+'/daily-sales-bills/all')
}

// Get Order
export const getDailySalesBill = (id) => {
    return axios.get(url+'/daily-sales-bills/getOrder/'+id)
}

// Place new order
export const placeNewDailySalesBill = (data) => {
    return axios.post(url+'/daily-sales-bills/create',data).then(function (response) {
         url +'admin/daily-sales-bills/orderlist';
    });
}

// Update order
export const updateDailySalesBillAction = (id,data) => {
    return axios.put(url+'/daily-sales-bills/updateAction/'+id,data)
}

// delete item
export const removeItemFromDailySalesBill = (id) => {
    return axios.delete(url+'/daily-sales-bills/removeItemFromOrder/'+id)
}






export const getAllBO = () => {
    return axios.get(url+'/buy-orders/all')
}

// Get Order
export const getBuyOrders = (id) => {
    return axios.get(url+'/buy-orders/getOrder/'+id)


}

// Place new order
export const placeNewBuyOrder = (data) => {
    return axios.post(url+'/buy-orders/create',data)
}

// Update order
export const updateBayOrderAction = (id,data) => {
    return axios.put(url+'/buy-orders/updateAction/'+id,data)
}

// delete item
export const removeItemFromBuyOrder = (id) => {
    return axios.delete(url+'/buy-orders/removeItemFromOrder/'+id)
}


// Place new stock qty
export const placeNewStock = (data) => {
    return axios.post(url+'/productStocks/create',data).then(function (response) {
        window.location = response.data.redirect;
    });
}

export const orderlistSalesBills = () => {

    return axios.get(url+'/sales-bills/orderlist/orders');
}