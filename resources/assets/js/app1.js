import Vue from 'vue';
import VueInternationalization from 'vue-i18n';
import Locales from './vue-i18n-locales.generated';
import list from './components_list/sales_bills/list.vue';
import show from './components_list/sales_bills/show.vue';
import {homeUrl} from './BackEnd-Calls.js';
import {orderlistSalesBills} from './BackEnd-Calls';
import moment from './moment-with-locales';















require('./bootstrap');
Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locales,
});




const app =new Vue({
    el:'#app1',
    i18n,
    data: {
        orderlist :[],
        url : homeUrl(),
        order: {},
        mycmp: 'list',
        moment : moment(),


    },
    components: {
        list,
        show


    },
    methods: {
        showOrder : function (index) {
            this.order = this.orderlist[index];
            console.log(this.order);
            this.mycmp='show';

        },

        hideOrder: function () {
            this.mycmp='list';
        },








    },
    computed: {
        binding: function() {
            if (this.mycmp == 'list') {
                return {
                    'showOrder' : this.showOrder,
                    'url' : homeUrl,
                    'list' : this.orderlist,
                    'lang' : lang,
                    'moment':moment,

                };
            } else if(this.mycmp ==='show') {
                return {
                    'back':this.hideOrder,
                    'order':this.order,
                    'lang' : lang,
                    'moment':moment,


                };
            }
        }
    },
    created() {
        'use strict';
        orderlistSalesBills().then((res)=>{
            const data = res.data;
            for (let key in data.list) {
                this.orderlist.push(data.list[key]);
                console.log('run');
            }

        })
            .catch((error)=>console.log(error));


    }



});

