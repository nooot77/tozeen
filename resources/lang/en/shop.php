<?php

return [
	'home' => 'Home',
	'featured' => 'Featured',
	'categories' => 'Categories',
	'blog' => 'Blog',
	'about' => 'About',
	'contact' => 'Contact',
	'items_in_my_cart' => 'Items in my cart',
	'total' => 'Total',
	'view-cart' => 'View cart',
	'checkout' => 'Checkout',
	'allProdcuts' => 'All Prodcuts',
	'filter' => 'Filter',
	'add-to-cart' => 'Add To Cart',
	'products' => 'Products',
	'tags' => 'Tags',
	'shipping' => 'Shipping',
	'callNow' => 'Call Now',
	'pro_desc' => 'Everything currently for sale',
	'tag_desc' => 'Explore product collections',
	'shiping_desc' => 'Free shipping With all order over $50',
	'the_blog' => 'The Blog',
	'readMore' => 'Read More',
	'aboutUs' => 'About Us',
	'contactUs' => 'Contact Us',
	'payment_method' => 'Payment Method',
	'language' => 'Language',
	'myAccount' => 'My Account',
	'Order_tracking' => 'Order tracking',
	'Return_Order' => 'My Account',
	'Help_Support' => 'Help & Support',
	'FAQs' => 'FAQs',
	'price' => 'price',
	'color' => 'color',
	'size' => 'size',
	'more' => 'More',
	'quickView' => 'Quick view',
	'add_to_Wishlist' => 'Add To Wishlist',
	'weight' => 'Weight',
	'fill' => 'Fill',

	// register page / login


	'MEMBER' => 'MEMBER',
	'login' => 'LOGIN',
	'lost_password' => 'lost your password?',
	'remember' => 'REMEMBER ME',
	'REGISTER' => 'REGISTER',
	'full_name' => 'Full Name',
	'Email' => 'Email',
	'Password' => 'Password',
	'OR_REGISTER_WITH' => 'OR REGISTER WITH',
	'facebook' => 'FACEBOOK',
	'google' => 'GOOGLE',
	'change_to_register' => 'CLICK TO SWITCH REGISTER/LOGIN',
	'for_register' => 'Registering for this site allows you to access your order status and history. Just fill in the fields below, and we’ll get a new account set up for you in no time. We will only ask you for information necessary to make the purchase process faster and easier.',




	/// Cart Page

	'CART' => 'CART',
	'PRODUCT' => 'PRODUCT',
	'TOTAL' => 'TOTAL',
	'QUANTITY' => 'QUANTITY',
	'update_cart' => 'Update Cart',
	'CART_TOTALS' => 'CART TOTALS',
	'Subtotal' => 'Subtotal',
	'Shipping' => 'Shipping',
	'Subtotal' => 'Subtotal',
	'PROCEED_TO_CHECKOUT' => 'PROCEED TO CHECKOUT',





];