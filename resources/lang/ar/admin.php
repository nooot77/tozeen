<?php
return [

	'login'                        => 'تسجيل دخول',
	'ar'                           => 'عربى',
	'en'                           => 'انجليزى ',
	'main_lang'                    => 'اللغة الافتراضية ',
	'adminpanel'                   => 'لوحة التحكم',
	'inccorrect_information_login' => 'البريد الالكترونى او كلمة المرور غير صحيحة من فضلك اعد المحاولة',
	'forgot_password'              => 'نسيت كلمة المرور',
	'the_link_reset_sent'          => 'ارسل رابط استعادة الحساب',
	'admin'                        => 'حسابات المشرفين',
	'dashboard'                    => 'الرئيسية',
	'create_admin'                 => 'اضف مشرف جديد',
	'ex_excel'                     => 'تصدير ك Excel',
	'ex_csv'                       => 'تصدير ك CSV',
	'all_record'                   => 'كل السجلات',
	'sProcessing'                  => 'تحميل',
	'sLengthMenu'                  => 'اظهار _MENU_ سجل',
	'sZeroRecords'                 => 'صفر سجل',
	'sEmptyTable'                  => 'جدول خالى',
	'sInfo'                        => 'اظهار _START_ الى  _END_ من _TOTAL_ سجل',
	'sInfoEmpty'                   => 'معلومات خالية',
	'sInfoFiltered'                => 'معلومات منتقاه',
	'sInfoPostFix'                 => '',
	'sSearch'                      => 'بحث',
	'sUrl'                         => '',
	'sInfoThousands'               => '',
	'sLoadingRecords'              => 'تحميل السجلات',
	'sFirst'                       => 'الاول',
	'sLast'                        => 'الاخير',
	'sNext'                        => 'التالى',
	'sPrevious'                    => 'السابق',
	'sSortAscending'               => 'ترتيب بحسب الاقدم',
	'sSortDescending'              => 'ترتيب بحسب الاحدث',
	'edit'                         => 'تعديل',
	'add'                          => 'إضافة',
	'delete'                       => 'حذف',
	'admin_name'                   => 'اسم المشرف',
	'admin_email'                  => 'بريد المشرف',
	'created_at'                   => 'أنشيء فى',
	'updated_at'                   => 'محدث فى',
	'ask_delete_itme'              => 'هل انت موافق على حذف السجلات التالية وهي عددها ',
	'ask_delete_dep'               => 'هل تريد حذف القسم',
	'please_check_some_records'    => 'من فضلك قم باختيار بعض السجلات للحذف',
	'yes'                          => 'حسنا',
	'no'                           => 'لا',
	'close'                        => 'غلق',
	'name'                         => 'الاسم',
	'email'                        => 'البريد الالكترونى',
	'password'                     => 'كلمة السر',
	'record_added'                 => 'تم إضافة السجل بنجاح',
	'save'                         => 'حفظ',
	'updated_record'               => 'تم تحديث البيانات بنجاح',
	'deleted_record'               => 'تم الحذف للبيانات بنجاح',
	'delete_this'                  => 'هل انت موافق على حذف :fname :lname ؟',
	'users'                        => 'الاعضاء',
	'user'                         => 'عضو',
	'company'                      => 'شركة',
	'vendor'                       => 'متجر',
	'level'                        => 'مستوى العضوية',
	'settings'                     => 'الاعدادات',
	'sitename_ar'                  => 'اسم الموقع بالعربى',
	'sitename_en'                  => 'اسم الموقع بالانجليزية',
	'logo'                         => 'شعار الموقع',
	'icon'                         => 'رمز الموقع',
	'description'                  => 'وصف الموقع',
	'keywords'                     => 'الكلمات الدليلية',
	'status'                       => 'الحالة',
	'open'                         => 'مفتوح',
	'message_maintenance'          => 'رسالة الصيانة',
	'countries'                    => 'الدول',
	'create_countries'             => 'اضافة دولة جديدة',
	'country_name_ar'              => 'اسم الدولة عربى',
	'country_name_en'              => 'اسم الدولة انجليزى',
	'country_flag'                 => 'العلم',
	'mob'                          => 'كود الدولة',
	'pcode'                        => 'كود المنتج',
	'code'                         => 'اختار / رمز الدولة',
	'city'                         => 'المدن',
	'ccity'                        => 'اختيار المدينة',
	'cities'                       => 'المدن / المحافظات',
	'city_name_ar'                 => 'اسم المدينة بالعربى',
	'city_name_en'                 => 'اسم المدينة بالانجليزية',
	'country_id'                   => 'الدولة',
	'states'                       => 'المناطق / الاحياء',
	'create_states'                => 'إضافة منطقة جديدة',
	'state_name_ar'                => 'اسم المنطقة بالعربى',
	'state_name_en'                => 'اسم المنطقة بالانجليزية',
	'city_id'                      => 'المدينة',
	'b_city_id'                    => 'الفرع التابع له',

	'clients'                       => 'العملاء',
	'clients_all'                   => 'كل العملاء',
	'clients_add'                   => 'اضافة عميل',
	'client_name_ar'                => 'اسم العميل بالعربي',
	'client_name_en'                => 'اسم العميل بالانجليزي',
	'client_type'                	=> 'نوع العميل',
	'client_merchant'               => 'جملة',
	'client_operator'               => 'مُشغل',
	'client_branch'               	=> 'فرع',
	
	'all_departments'              => 'كل الاقسام',
	'departments'                  => 'الاقسام',
	'department'                   => 'القسم',
	'dep_name_ar'                  => 'اسم القسم بالعربى',
	'dep_name_en'                  => 'اسم القسم بالانجليزية',
	'parent'                       => 'فرعى من',
	'icon'                         => 'الرمز',
	'description'                  => 'الوصف',
	'keyword'                      => 'الكلمات الدليلية',
	'trademarks'                   => 'العلامات التجارية',
	'name_en'                      => 'الاسم بالانجليزية',
	'name_ar'                      => 'الاسم بالعربي',
	'create_trademarks'            => 'إضافة علامة تجارية',
	'trade_icon'                   => 'شعار العلامة التجارية',
	'fname'                        => 'الإسم الاول',
	'lname'                        => 'الإسم الاخير',
	'pic'                          => 'الصورة',
	'manufacturers'                => 'المصنعين',
	'mobile'                       => 'الجوال',
	'facebook'                     => 'فيسبوك',
	'twitter'                      => 'تويتر',
	'website'                      => 'الموقع الرسمى',
	'contact_name'                 => 'المسؤل عن المصنع',
	'lat'                          => '',
	'lng'                          => '',
	'manufacturers_icon'           => 'شعار المصنع',
	'address'                      => 'العنوان',
	'shipping'                     => 'شركات الشحن',
	'owner_id'                     => 'المالك',
	'malls'                        => 'الممجمعات التجارية',
	'mall_icon'                    => 'شعار المول',
	'colors'                       => 'الالوان',
	'color'                        => 'اللون',
	'sizes'                        => 'الاحجام',
	'size'                         => 'الحجم',
	'dep_name'                     => 'القسم',
	'is_public'                    => 'عام ؟' ,
    'all_products'                 => 'كل المنتجات' ,	
	'products'                     => 'المنتجات',
	'product'                      => 'المنتج',
	'weights'                      => 'الأوزان',
	'weight'                       => 'الوزن',
	'fills'                        => 'الحشوات',
	'fill'                         => 'الحشوة',
	'event_coordinators'           => 'منسقة الحفلات',
	'event_coordinator'            => 'منسقة الحفلات',
	'start_date'                   => 'تاريخ البداية',
	'end_date'                     => 'تاريخ النهاية',
	'commission'                   => 'العمولة',
	'phone'                        => 'الجوال',
	'office'                       => 'الجهة',
	'create_products'              => 'إضافة منتجات',
	'product_info'                 => 'معلومات المنتج',
	'product_settings'             => 'اعدادات المنتج',
	'product_media'                => 'وسائط المنتج',
	'product_size_weight'          => 'وزن المنتج وحجمه',
	'dashboard'                    => 'الرئيسية',
	'control_panel'                => 'لوحة التحكم',
	'other_data'                   => 'البيانات الاضافية',
	'save_and_continue'            => 'الحفظ والمتابعة',
	'copy_product'                 => 'نسخ المنتج',
	'create_or_edit_product'       => ' :title إضافة او تعديل المنتج',
	'title'                        => 'الاسم',
	'content'                      => 'المحتوى',
    'price'                        => 'السعر',
	'stock'                        => 'الكمية',
	'start_at'                     => 'يبدأ في ',
	'end_at'                       => 'ينتهي في',
	'price_offer'                  => 'سعر العرض',
	'start_offer_at'               => 'يبدأ العرض في',
	'end_offer_at'                 => 'ينتهي العرض في ',
	'pending'                      => 'معلق',
	'active'                       => 'فعَّال',
	'refused'                      => 'مرفوض',
	'reason'                       => 'السبب',
	'upload_message'               => 'من فضلك قم بإدراج بعض املفات للرفع',
	'main_photo'                   => 'الصورة الرئيسية',
	'mainphoto_message'            => 'من فضلك فم بادراج الصورة الرئيسية',
	'photos'                       => 'الصور',
	'size_id'                      => 'نوع الحجم',
	'weight_id'                    => 'نوع الوزن',
	'exp_date'                     => 'تاريخ الإنتهاء',
	'cal'                          => 'السعرات الحرارية',
	'sale_price'                   => 'سعر البيع',
	'numbers_unit'                 => 'عدد الوحدات',
	'number_unit'                  => 'العدد',
	'units_weight'                 => 'وزن الوحدة',
	'prd_date'                     => 'تاريخ الإنتاج',
	'trademark'                    => 'العلامة التجارية',
	'manufacturer'                 => 'المورد / المُصنع',
	'country'                      => 'الدولة',
	'select_msg'                   => 'الرجاء الاختيار',
	// New 
	'add_new_po'                   	=> 'اضافة طلب مشتريات',
	'Vendor'                      	=> 'المورد',
	'Select_Vendor'                 => 'اختر المورد',
	'select_ec'                     => 'اختر المنسقة',
	'Currency'                      => 'العملة',
	'Select_Currency'               => 'اختر العملة',
	'Number'                    	=> 'الرقم',
	'Date'                      	=> 'التاريخ',
	'Referance'                    	=> 'المرجع',
    'details'                       => 'التفاصيل',
    'print'                         => 'طباعة',
	'Item_Description'              => 'وصف العناصر',
	'Select_Item'                   => 'اختر عنصر',
	'Vendor_Referance'              => 'مرجع المورد',
	'Vendor_Price'                  => 'سعر المورد',
	'Qty'                      		=> 'العدد',

	'Total'                   	 	=> 'الاجمالي',
	'add_new_item'                  => 'اضافة منتج جديد',
	'terms_conditions'              => 'الشروط والأحكام',
	'savepo'                      	=> 'حفظ',
	'addandnew'                    	=> 'حفظ وانشاء جديد',
	'canceladd'                    	=> 'الغاء',
	'contract_date'                 => 'تاريخ بدء التعاقد',

	// Clients
	'clients'                      	=> 'العملاء',
	'cclients'                      => 'اختر عميل',

	// Sales Orders
	'add_new_sale'                  => 'اضافة طلب مبيعات',
	'show_all_sales'                => 'عرض المبيعات',
	'edit_sale'                     => 'تعديل طلب مبيعات',
	
	'currencies'                    => 'العملات',
	'create_currencies'             => 'انشاء عملات',
	'currency_name'                 => 'اسم العملة',
	'place_decimal'                 => 'عدد الفواصل العشرية للعملة',

	//sales Bills
	'branches'                       => 'الأفرع',
	'ads_status'                     => 'خاصية الاعلان',
	'payment_status'                 => 'طريقة الدفع',
	'delivery_status'                => 'خدمة التوصيل',
	'ec'               				 => 'منسقة المناسبة',
	'event_date'              	     => 'تاريخ المناسبة',
	'client_name'                    => 'اسم العميل',
	'delivery_date'                  => 'وقت التسليم',
	'insurance'                      => 'التأمين',
	'paid'                           => 'المدفوع',
	'total'                          => 'الاجمالي',
	'traies'                         => 'الصواني',
	'residual'                       => 'المتبقي',
	'cnotes'                         => 'الملاحظات الخاصة بالعميل',
	'later'                          => 'آجل',
	'add_new_sb'                     => 'فاتورة مبيعات',
	'clintes'                        => 'العملاء',
	'add_new_bo'                     => 'طلب شراء',
	'all_tries'                      => 'كل الصواني',
	'tries'                          => 'الصواني',
	'branch'                         => 'الفرع',
	'billNum'                        => ' رقم الفاتورة',
	'kiloQty'                        => 'الكمية بالكيلو',
	'productType'                    => 'الصنف \ نوع المنتح',
	'ProductStocks'                  => 'المخزون',
	'add_stock_first_qty'            => 'إضافة كميات بداية الفترة',
	'select_branch'                  => 'اختر الفرع',
	'first_quantity'                 => 'كمية بداية الفترة',
	'warehouse_quantity'             => 'كمية المستودع',
	'events_quantity'                => 'كمية المناسبات',
	'total_quantity'                 => 'الكمية الإجمالية',
	'remaining_quantity'             => 'الكمية المتبقية',
	'pStocks'                        => 'المخزون',
	'warehouse'                      => 'المستودع',
	'factory'                        => 'المشغل',
	'hail_branch'                    => 'فرع حائل',
	'alqassim_branch'                => 'فرع القصيم ',
	'almadinah_branch'               => 'فرع المدينة',
	'alriyadh_branch'                => 'فرع الرياض',



];
