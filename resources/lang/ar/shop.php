<?php

return [
	'home' => 'الصفحة الرئيسية',
	'featured' => 'متميز',
	'categories' => 'الاقسام',
	'blog' => 'مدونة',
	'about' => 'حول',
	'contact' => 'اتصل',
	'items_in_my_cart' => 'العناصر الموجودة في سلتي',
	'total' => 'مجموع',
	'view-cart' => 'عرض العربة',
	'checkout' => 'الدفع',
	'allProdcuts' => 'جميع المنتجات',
	'filter' => 'لتصفية',
	'add-to-cart' => 'أضف إلى السلة',
	'products' => 'منتجات',
	'tags' => 'وسم',
	'shipping' => 'الشحن',
	'callNow' => 'اتصل الان',
	'pro_desc' => 'كل شيء حاليا للبيع',
	'tag_desc' => 'استكشاف مجموعات المنتجات',
	'shiping_desc' => 'شحن مجاني مع كل طلب أكثر من 50 دولارا',
	'the_blog' => 'المدونة',
	'readMore' => 'قراءة المزيد',
	'aboutUs' => 'معلومات عنا',
	'contactUs' => 'اتصل بنا',
	'payment_method' => 'طريقة الدفع او السداد',
	'language' => 'لغة',
	'myAccount' => 'حسابي',
	'Order_tracking' => 'تتبع الطلب',
	'Return_Order' => 'حسابي',
	'Help_Support' => 'المساعدة والدعم',
	'FAQs' => 'أسئلة وأجوبة',
	'price' => 'السعر',
	'color' => 'اللون',
	'size' => 'بحجم',
	'more' => 'لمزيد',
	'quickView' => 'نظرة سريعة',
	'add_to_Wishlist' => 'أضف إلى قائمة الامنيات',
	'weight' => 'الوزن',
	'fill' => 'الحشوة',



	// register page / login


	'MEMBER' => 'الأعضاء',
	'remember' => 'تذكرني',
	'lost_password' => 'نسيت كلمة السر؟',
	'login' => 'تسجيل الدخول',
	'register' => 'إنشاء حساب',
	'full_name' => 'الاسم الكامل',
	'Email' => 'البريد الإلكتروني',
	'Password' => 'كلمه السر',
	'OR_REGISTER_WITH' => 'أو سجل مع',
	'facebook' => 'فيس بوك',
	'google' => 'جوجل',
	'change_to_register' => 'انقر لتبديل التسجيل / تسجيل الدخول',
	'for_register' => 'يسمح لك التسجيل في هذا الموقع بالوصول إلى حالة الطلب والمحفوظات. ما عليك سوى ملء الحقول أدناه ، وسنقوم بإنشاء حساب جديد لك في أي وقت من الأوقات. سنطلب منك فقط المعلومات اللازمة لجعل عملية الشراء أسرع وأسهل.',




/// Cart Page

	'CART' => 'عربة التسوق',
	'PRODUCT' => 'المنتج',
	'TOTAL' => 'مجموع',
	'QUANTITY' => 'كمية',
	'update_cart' => 'تحديث العربة',
	'CART_TOTALS' => 'مجموع السلة',
	'Subtotal' => 'حاصل الجمع',
	'Shipping' => 'الشحن',
	'PROCEED_TO_CHECKOUT' => 'اتمام الشراء',


];