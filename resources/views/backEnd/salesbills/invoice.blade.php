@extends('backEnd.layout')

@section('content')

@push('js')
<script type="text/javascript">
    $(document).ready(function () {
        $('#printInvoice').click(function () {
            Popup($('.invoice')[0].outerHTML);

            function Popup(data) {
                window.print();
                return true;
            }
        });
    });
</script>
@endpush
@push('css')
<style>
    #invoice {
        padding: 30px;
        margin-top: -30px;
    }

    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: auto;
        padding: 15px
    }

    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }

    .invoice .company-details {
        text-align: right;
        padding-top:20px;
        color:#07a996;
    }

    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }

    .invoice .contacts {
        margin-bottom: 15px
    }

    .invoice .invoice-to {
        text-align: right;
        direction: rtl;
    }

    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 10px;
        font-size: 18px;
        
    }

    .invoice .invoice-to .email {
        margin-top: 0;
        margin-bottom: 10px;
        font-size: 18px;
        
    }
    

    .invoice .invoice-details {
        text-align: left
    }

    .invoice .invoice-details .invoice-id {
        direction: rtl;
        margin-top: -100px;
        color: #308f84;
        font-size: 18px;
    }

    .invoice .invoice-details .date {
        direction: rtl;
        /* margin-top: -100px; */
        color: #308f84;
        font-size: 18px;
    }
    .invoice main {
        padding-bottom: 50px
    }

    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }

    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }

    .invoice main .notices .notice {
        font-size: 1.2em
    }

    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }

    .invoice table td,
    .invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }

    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }

    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }

    .invoice table .qty,
    .invoice table .total,
    .invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }

    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }

    .invoice table .unit {
        background: #ddd
    }

    .invoice table .total {
        background: #3989c6;
        color: #fff
    }

    .invoice table tbody tr:last-child td {
        border: none
    }

    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }

    .invoice table tfoot tr:first-child td {
        border-top: none
    }

    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }

    .invoice table tfoot tr td:first-child {
        border: none
    }

    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }
    .bill-num{
        text-align: center;
        align-content: center;
        align-self: center;
    }
    .client_details{
        color: #308f84;
        direction: rtl;
    }
    .dates{
	border:1px solid #ebeff2;
	border-radius:5px;
	padding:20px 0px;
	margin:10px 20px;
	font-size:16px;
	color:#5aadef;
	font-weight:600;	
	overflow:auto;
}
.dates div{
	float:right;
	width:50%;
	text-align:center;
	position:relative;
}
.dates strong,
.stats strong{
    
	display:block;
	color:#adb8c2;
	font-size:11px;
	font-weight:700;
}
.dates span{
	width:1px;
	height:40px;
	position:absolute;
	left:0;
	top:0;	
	background:#ebeff2;
}

.tile{
	width: 100%;
	background:#fff;
	border-radius:5px;
	box-shadow:0px 2px 3px -1px rgba(151, 171, 187, 0.7);
	float:right;
  	transform-style: preserve-3d;
  	margin: 30px 5px;

}
.tile1{
	width: 100%;
	background:#fff;
	border-radius:5px;
	box-shadow:0px 2px 3px -1px rgba(151, 171, 187, 0.7);
	float:right;
  	transform-style: preserve-3d;
  	margin-right:320px;

}
.header{
	border-bottom:1px solid #ebeff2;
	padding:19px 0;
	text-align:center;
	color:#59687f;
	font-size:600;
	font-size:19px;	
	position:relative;
}

.banner-img {
	padding: 5px 5px 0;
}

.banner-img img {
	width: 100%;
	border-radius: 5px;
}
.stats{
	border-top:1px solid #ebeff2;
	background:#f7f8fa;
	overflow:auto;
	padding:15px 0;
	font-size:16px;
	color:#59687f;
	font-weight:600;
	border-radius: 0 0 5px 5px;
}
.stats div{
	border-right:1px solid #ebeff2;
	width: 33.33333%;
	float:left;
	text-align:center
}
div.footer {
	text-align: right;
	position: relative;
	margin: 20px 5px;
}
.stats div:nth-of-type(3){border:none;}
    @media print {
       
        .invoice {
            font-size: 11px !important;
            overflow: hidden !important;
            margin-top:-100px;
        }

        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }

        .invoice>div:last-child {
            page-break-before: always
        }
    .tile{
	width:100%;
	background:#fff;
	border-radius:5px;
	box-shadow:0px 2px 3px -1px rgba(151, 171, 187, 0.7);
	float:right;
  	transform-style: preserve-3d;
  	margin-top: 80px;
}

.tile1{
	width: 100%;
	background:#fff;
	border-radius:5px;
	box-shadow:0px 2px 3px -1px rgba(151, 171, 187, 0.7);
	float:right;
  	transform-style: preserve-3d;
  	margin-right:30px;
}

.tile3{
margin-top: -150px;

}
.header{
	border-bottom:1px solid #ebeff2;
	padding:19px 0;
    /* margin-top: 20px;
    padding-left: 350px; */
	text-align:center;
	color:#59687f;
	/* font-size:600; */
	font-size:19px;	
	/* position:relative; */
}
    }
    #summary {
        width:18vw;   
    }
    
</style>
@endpush
<div class="padding">
    <div class="box">

        <div class="box-body">
   <div id="invoice">

                <div class="toolbar hidden-print">
                    <div class="text-left">
                        <button id="printInvoice" class="btn btn-sm btn-info"><i class="fa fa-print"></i> طباعة الفاتورة</button>
                        {{-- <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button> --}}
                    </div>

                </div>
                <div class="invoice overflow-auto">
       <div >
        <header>
            <div class="row col-xs-12">
                
                
                <a class="navbar-brand" href="{{ route('adminHome') }}">
                        <img style="" src="{{ URL::to('backEnd/assets/images/logo.png') }}" alt="Control">
                        {{-- <span class="hidden-folded inline">{{ trans('backLang.control') }}</span> --}}
                    </a>
        
                    {{-- <h4 class="bill-num">
                            فاتورة رقم
                        </h4> --}}
                <div class="col company-details">
                  
                       
                  
                    <h2 class="name">
                        <h3>
                                توزين للشوكولاتة

                        </h3>  
                    </h2>
                    {{-- <div>455 Foggy Heights, AZ 85004, US</div>
                    <div>(123) 456-789</div>
                    <div>company@example.com</div> --}}
                </div>
            </div>
        </header>
        <main>
            <div class="row contacts">
              

            <div class="clearfix"></div>
            <hr>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="tile2">
                <div class="wrapper">
                    <div class="header">{{'فاتورة رقم #'.'  '.$salesBills->number}}</div>
                    <div class="dates">
                        <div class="start">
                            <strong>أُصدرت من قبل </strong> 
                            {{$salesBills->oc['name_ar']}}
                            <span></span>
                        </div>
                        <div class="ends">
                            <strong>وقد سلمت إلى</strong>  
                            {{$salesBills->bi['name_ar']}}
                       </div>
                    </div>
                    <div class="dates">




                        <div class="stats">

                                <div>
                                    <strong> المدفوع</strong> {{$salesBills->paid}}
                                </div>
        
                                <div>
                                    <strong>التأمين</strong> {{$salesBills->insurance}}
                                </div>
        
                                <div>
                                    <strong>المتبقي</strong> {{$salesBills->residual}}
                                </div>
        
                            </div>
                    <div class="footer">
                            
                   </div>
    </div>
            <hr>
            <div class="clearfix"></div>
            <div class="notices">
                    <div>
                        <h4>
                            تفاصيل الطلب والملاحظات :    
                    </h4>
                </div>
                    <div class="notice notes">
                    
                            {!! $salesBills->notes !!}
                      
                    </div>
                </div>
                <div class="clearfix"></div>  
            <table border="0" cellspacing="0" cellpadding="0" style="margin-top:60px;">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">الكود</th>
                        <th class="text-left">الصنف</th>
                        <th class="text-left">الكمية</th>
                        <th class="text-left"> الخصم</th>
                        <th class="text-left">السعر</th>
                    </tr>
                </thead>
                <tbody>
                        
                         {{-- count the total price before add the tax --}}
                        <?php $price_before_tax = 0 ?>
                        
                       @foreach ($salesBills->orderItems as $index => $item)
                        
                       {{-- {{dd($item->product)}} --}}
                            
                    <tr>
                    <td>
                        {{$index + 1}}
                    </td>
                    {{-- <td class="no">{{$product->code}}</td> --}}
                    {{-- @endforeach --}}
                        <td class="text-left">
                            <h3>
                                {{$item->product->code}}
                            </h3>
                        </td>
                        <td class="unit text-right" >{{$item->product->title}}</td>
                        <td class="qty">{{$item->qty}} كيلو</td>
                        
                        @if($item->discount > 0)
                        <td class="discount">{{$item->discount}} %</td>
                        @else 
                        <td class="discount">0</td>
                        @endif
                        <td class="total">{{$item->product->price * $item->qty}} 
                            ريال</td>
                    </tr>

                    <?php 
                        $price_before_tax += $item->product->price * $item->qty
                    ?>
                     

                     @endforeach
                </tbody>
                
      
            </table>
            {{-- Invoice Summary --}}
            <div class="row">
            <div class="col-md-3">
            <table class="table">
                    <thead>
                      <tr>
                        <th scope="col" colspan="2" class="text-center">الإجمالي</th>
                        
                    
                      </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">السعر قبل الضريبة</th>
                            <td>{{$price_before_tax}}</td>
                        </tr>
                      <tr>
                        <th scope="row">الضريبة</th>
                        <td>5%</td>
               
                      </tr>
                      <tr>
                        <th scope="row">السعر شامل الضريبة</th>
                      <td>{{$salesBills->total}}</td>
               
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <div class="clearfix"></div>
            {{-- <div class="thanks">شكرا</div> --}}
           
        </main>
        <footer>
            Invoice was created on a tozeen website and is valid without the signature and seal.
        </footer>
    </div>
    <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
    {{-- <div></div> --}}
</div>


 @endsection