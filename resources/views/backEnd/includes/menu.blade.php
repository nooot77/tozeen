<?php
// Current Full URL
$fullPagePath = Request::url();
// Char Count of Backend folder Plus 1
$envAdminCharCount = strlen(env('BACKEND_PATH')) + 1;
// URL after Root Path EX: admin/home
$urlAfterRoot = substr($fullPagePath, strpos($fullPagePath, env('BACKEND_PATH')) + $envAdminCharCount);
?>

<style>
    .navbar-brand img, .navbar-brand svg {
        min-height: 45px;
        margin-top: 15px;
    }
    .half-rule { 
        /* margin-left: 0; */
        text-align: center;
        width: 50%;
        color: #07a996;
        background-color: #07a996;
    }
</style>

<div id="aside" class="app-aside modal fade folded md nav-expand">
    <div class="left navside dark dk" layout="column">
        <div class="navbar navbar-md no-radius">
            <!-- brand -->
            <a class="navbar-brand" href="{{ route('adminHome') }}">
                <img src="{{ URL::to('backEnd/assets/images/logo.png') }}" alt="Control">
                {{-- <span class="hidden-folded inline">{{ trans('backLang.control') }}</span> --}}
            </a>
            <!-- / brand -->
        </div>
        <div flex class="hide-scroll">
            <nav class="scroll nav-active-primary">

                <ul class="nav" ui-nav>

                    <hr class="half-rule"/>
                    <li class="nav-header hidden-folded">
                        <h5 class="text-muted" style="text-align:center; color:white;">{{Auth::user()->permissionsGroup->name}}</h5>
                    </li>
                    <hr class="half-rule"/>
                    <li>
                        <a href="{{ route('adminHome') }}" onclick="location.href='{{ route('adminHome') }}'">
                          <span class="nav-icon">
                            <i class="material-icons">&#xe3fc;</i>
                        </span>
                        <span class="nav-text">{{ trans('backLang.dashboard') }}</span>
                    </a>
                </li>

                


                @if(Helper::GeneralWebmasterSettings("analytics_status"))
                @if(@Auth::user()->permissionsGroup->analytics_status)
                <?php
                            $currentFolder = "analytics"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));

                            $currentFolder2 = "ip"; // Put folder name here
                            $PathCurrentFolder2 = substr($urlAfterRoot, 0, strlen($currentFolder2));

                            $currentFolder3 = "visitors"; // Put folder name here
                            $PathCurrentFolder3 = substr($urlAfterRoot, 0, strlen($currentFolder3));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder || $PathCurrentFolder2==$currentFolder2  || $PathCurrentFolder3==$currentFolder3) ? 'class=active' : '' }}>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                        <i class="material-icons">&#xe1b8;</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.visitorsAnalytics') }}</span>
                                </a>
                                <ul class="nav-sub">
                                    <li>
                                        <a onclick="location.href='{{ route('analytics', 'date') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsBydate') }}</span>
                                        </a>
                                    </li>

                                    <?php
                                    $currentFolder = "analytics/country"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'country') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByCountry') }}</span>
                                        </a>
                                    </li>

                                    <?php
                                    $currentFolder = "analytics/city"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'city') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByCity') }}</span>
                                        </a>
                                    </li>

                                    <?php
                                    $currentFolder = "analytics/os"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'os') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByOperatingSystem') }}</span>
                                        </a>
                                    </li>

                                    <?php
                                    $currentFolder = "analytics/browser"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'browser') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByBrowser') }}</span>
                                        </a>
                                    </li>

                                    <?php
                                    $currentFolder = "analytics/referrer"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'referrer') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByReachWay') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                    $currentFolder = "analytics/hostname"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'hostname') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByHostName') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                    $currentFolder = "analytics/org"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('analytics', 'org') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsByOrganization') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                    $currentFolder = "visitors"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a onclick="location.href='{{ route('visitors') }}'">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsVisitorsHistory') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                    $currentFolder = "ip"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('visitorsIP') }}">
                                            <span class="nav-text">{{ trans('backLang.visitorsAnalyticsIPInquiry') }}</span>
                                        </a>
                                    </li>


                                </ul>
                            </li>
                            @endif
                            @endif
                            @if(Helper::GeneralWebmasterSettings("newsletter_status"))
                            @if(@Auth::user()->permissionsGroup->newsletter_status)
                            <?php
                            $currentFolder = "contacts"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('contacts') }}">
                                    <span class="nav-icon">
                                        <i class="material-icons">&#xe7ef;</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.newsletter') }}</span>
                                </a>
                            </li>
                            @endif
                            @endif

                            @if(Helper::GeneralWebmasterSettings("inbox_status"))
                            @if(@Auth::user()->permissionsGroup->inbox_status)
                            <?php
                            $currentFolder = "webmails"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('webmails') }}">
                                  <span class="nav-icon">
                                    <i class="material-icons">&#xe156;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.siteInbox') }}
                                    @if( Helper::webmailsNewCount() >0)
                                    <badge class="label warn m-l-xs">{{ Helper::webmailsNewCount() }}</badge>
                                    @endif
                                </span>

                            </a>
                        </li>
                        @endif
                        @endif

                        @if(Helper::GeneralWebmasterSettings("calendar_status"))
                        @if(@Auth::user()->permissionsGroup->calendar_status)
                        <?php
                            $currentFolder = "calendar"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('calendar') }}" onclick="location.href='{{ route('calendar') }}'">
                                  <span class="nav-icon">
                                    <i class="material-icons">&#xe5c3;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.calendar') }}</span>
                            </a>
                        </li>
                        @endif
                        @endif





                        @if(Helper::GeneralWebmasterSettings("banners_status"))
                        @if(@Auth::user()->permissionsGroup->banners_status)
                        <?php
                            $currentFolder = "banners"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }} >
                                <a href="{{ route('Banners') }}">
                                    <span class="nav-icon">
                                        <i class="material-icons">&#xe433;</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.adsBanners') }}</span>
                                </a>
                            </li>
                            @endif
                            @endif

                            {{-- Start Clients Section --}}
                            @if(@Auth::user()->permissionsGroup->clients_status)
                            <li class="nav-header hidden-folded">
                                <small class="text-muted">{{ trans('admin.clients') }}</small>
                            </li>




                            <?php
                    $currentFolder = "clients"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('clients') }}" onclick="location.href='{{ route('clients') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">developer_board</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.clients_all') }}</span>
                        </a>
                    </li>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('clients.create') }}" onclick="location.href='{{ route('clients.create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">add_box</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.add') }}</span>
                        </a>
                    </li>
                    @endif 
                    {{-- End Departments Section --}}

                    {{-- Start Departments Section --}}
                    @if(@Auth::user()->permissionsGroup->departments_status)
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">{{ trans('admin.departments') }}</small>
                    </li>



                    <?php
                    $currentFolder = "departments"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('departments') }}" onclick="location.href='{{ route('departments') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">developer_board</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.all_departments') }}</span>
                        </a>
                    </li>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('departments.create') }}" onclick="location.href='{{ route('departments.create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">add_box</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.add') }}</span>
                        </a>
                    </li>
                    @endif 
                    {{-- End Departments Section --}}

                    {{-- Strat location & currencies  Section --}}
                    @if(@Auth::user()->permissionsGroup->lc_status)
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">{{ trans('backLang.lc') }}</small>
                    </li>
                    <?php
                    $currentFolder = "countires"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>

                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <i class="material-icons">blur_circular</i>
                            </span>
                            <span class="nav-text">              
                                {{ trans('admin.countries') }}     
                            </span>
                        </a>

                        <ul class="nav-sub">
                            <li>
                                <a onclick="location.href='{{ route('countries.index') }}'">
                                    <span class="nav-text">{{ trans('backLang.allcountries') }}</span>
                                </a>
                            </li>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('countries.create') }} " onclick="location.href='{{ route('countries.create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">add_box</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.add') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                    $currentFolder = "cities"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>

                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <i class="material-icons">gps_fixed</i>
                            </span>
                            <span class="nav-text">              
                                {{ trans('admin.cities') }}     
                            </span>
                        </a>

                        <ul class="nav-sub">
                            <li>
                                <a onclick="location.href='{{ route('cities.index') }}'">
                                    <span class="nav-text">{{ trans('backLang.allcities') }}</span>
                                </a>
                            </li>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('cities.create') }} " onclick="location.href='{{ route('cities.create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">add_box</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.add') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                $currentFolder = "currencies"; // Put folder name here
                $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                ?>

                <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                    <a>
                        <span class="nav-caret">
                            <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                            <i class="material-icons">money</i>
                        </span>
                        <span class="nav-text">              
                            {{ trans('backLang.currencies') }}     
                        </span>
                    </a>

                    <ul class="nav-sub">
                        <li>
                            <a onclick="location.href='{{ route('currencies.index') }}'">
                                <span class="nav-text">{{ trans('backLang.allcurrencies') }}</span>
                            </a>
                        </li>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('currencies.create') }} " onclick="location.href='{{ route('currencies.create') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.add') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                {{-- end location & currencies  Section --}}

                {{-- Start Products Section --}}

                   {{-- Strat Product Stocks  Section --}}
                @if(@Auth::user()->permissionsGroup->pstocks_status)
                <li class="nav-header hidden-folded">
                    <small class="text-muted">{{ trans('backLang.pstocks_manager') }}</small>
                </li>
                <?php
                    $currentFolder = "allRegProductStocks"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                    <i class="material-icons">
                                            pie_chart
                                            </i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.first_qty_stocks') }}</span>
                        </a>

                        
                        <ul class="nav-sub">
                            <li>
                                <a onclick="location.href='{{ route('allRegProductStocks') }}'">
                                    <span class="nav-text">{{ trans('backLang.allRegProductStocks') }}</span>
                                </a>
                            </li>
                            <li>
                                <a onclick="location.href='{{ route('allNonRegProductStocks') }}'">
                                    <span class="nav-text">{{ trans('backLang.allNonRegProductStocks') }}</span>
                                </a>
                            </li>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('product-stock-create') }} " onclick="location.href='{{ route('product-stock-create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">add_box</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.add') }}</span>
                                </a>
                            </li>
                        </ul>

                    </li>

                 
                    <?php
                        $currentFolder = "productStocks"; // Put folder name here
                        $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                        ?>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                        <i class="material-icons">
                                                pie_chart
                                                </i>
                                </span>
                                <span class="nav-text">{{ trans('admin.pStocks') }}</span>
                            </a>
    
                            
                            <ul class="nav-sub">
                                <li>
                                    <a onclick="location.href='{{ Helper::aurl('productStocks/7') }}'">
                                        <span class="nav-text">{{ trans('admin.warehouse') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="location.href='{{  Helper::aurl('productStocks/6') }}'">
                                        <span class="nav-text">{{ trans('admin.factory') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="location.href='{{  Helper::aurl('productStocks/5') }}'">
                                        <span class="nav-text">{{ trans('admin.almadinah_branch') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="location.href='{{  Helper::aurl('productStocks/4') }}'">
                                        <span class="nav-text">{{ trans('admin.alqassim_branch') }}</span>
                                    </a>
                                </li>
                                <li>
                                    <a onclick="location.href='{{  Helper::aurl('productStocks/3') }}'">
                                        <span class="nav-text">{{ trans('admin.hail_branch') }}</span>
                                    </a>              
                                </li>
                                <li>
                                    <a onclick="location.href='{{  Helper::aurl('productStocks/2') }}'">
                                        <span class="nav-text">{{ trans('admin.alriyadh_branch') }}</span>
                                    </a>
                                </li>
                            </ul>
    
                        </li>
           
       
                @endif

                {{-- end Product Stocks Section --}}

                {{-- Strat Product Settings Section --}}
                @if(@Auth::user()->permissionsGroup->psettings_status)
                <li class="nav-header hidden-folded">
                    <small class="text-muted">{{ trans('backLang.psettings') }}</small>
                </li>
                <?php
                    $currentFolder = "sizes"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <i class="material-icons">open_with</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.sizes') }}</span>
                        </a>

                        
                        <ul class="nav-sub">
                            <li>
                                <a onclick="location.href='{{ route('sizes') }}'">
                                    <span class="nav-text">{{ trans('backLang.allsizes') }}</span>
                                </a>
                            </li>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('sizes.create') }} " onclick="location.href='{{ route('sizes.create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">add_box</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.add') }}</span>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <?php
                    $currentFolder = "weights"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <i class="material-icons">adjust</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.weights') }}</span>
                        </a>



                        <ul class="nav-sub">
                            <li>
                                <a onclick="location.href='{{ route('weights') }}'">
                                    <span class="nav-text">{{ trans('backLang.allweights') }}</span>
                                </a>
                            </li>
                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                <a href="{{ route('weights.create') }} " onclick="location.href='{{ route('weights.create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">add_box</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.add') }}</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                $currentFolder = "colors"; // Put folder name here
                $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                ?>

                <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                    <a>
                        <span class="nav-caret">
                            <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                            <i class="material-icons">colorize</i>
                        </span>
                        <span class="nav-text">{{ trans('admin.colors') }}</span>
                    </a>

                    <ul class="nav-sub">
                        <li>
                            <a onclick="location.href='{{ route('colors') }}'">
                                <span class="nav-text">{{ trans('backLang.allcolors') }}</span>
                            </a>
                        </li>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('colors.create') }} " onclick="location.href='{{ route('colors.create') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.add') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php
                $currentFolder = "fills"; // Put folder name here
                $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                ?>
                <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                    <a>
                        <span class="nav-caret">
                            <i class="fa fa-caret-down"></i>
                        </span>
                        <span class="nav-icon">
                            <i class="material-icons">filter_center_focus</i>
                        </span>
                        <span class="nav-text">{{ trans('admin.fills') }}</span>
                    </a>

                    <ul class="nav-sub">
                        <li>
                            <a onclick="location.href='{{ route('fills') }}'">
                                <span class="nav-text">{{ trans('backLang.allfills') }}</span>
                            </a>
                        </li>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('fills.create') }} " onclick="location.href='{{ route('fills.create') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.add') }}</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endif

                {{-- end Product Settings Section --}}
                @if(@Auth::user()->permissionsGroup->products_status)
                <li class="nav-header hidden-folded">
                    <small class="text-muted">{{ trans('backLang.products') }}</small>
                </li>


                @if(Helper::GeneralWebmasterSettings("analytics_status"))
                @if(@Auth::user()->permissionsGroup->analytics_status)
                <?php
                        $currentFolder = "products"; // Put folder name here
                        $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                        ?>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="material-icons">&#xe1b8;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.products_analytics') }}</span>
                            </a>

                        </li>
                        @endif
                        @endif

                        <?php
                    $currentFolder = "products"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('products') }}" onclick="location.href='{{ route('products') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">shopping_basket</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.all_products') }}</span>
                        </a>
                    </li>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('tries') }}" onclick="location.href='{{ route('tries') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">shopping_basket</i>
                            </span>
                            <span class="nav-text">{{ trans('admin.all_tries') }}</span>
                        </a>
                    </li>

                     <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('products.img.load') }} " onclick="location.href='{{ route('products.img.load') }}'">
                            <span class="nav-icon">
                              
                                <i class="material-icons">
                                    view_carousel
                                </i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.media_gallery') }}</span>
                        </a>
                    </li>

                    
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('products.create') }} " onclick="location.href='{{ route('products.create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">add_box</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.add') }}</span>
                        </a>
                    </li>


                    @endif 
                    {{-- End Products Section --}}

                    {{-- Start manufacturers Section --}}
                    @if(@Auth::user()->permissionsGroup->manufacturer_status)
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">{{ trans('backLang.manufacturers') }}</small>
                    </li>
                    <?php 
                    $currentFolder = "manufacturers"; // Put folder name here
                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                    ?>
                    
                    @if(Helper::GeneralWebmasterSettings("analytics_status"))
                    @if(@Auth::user()->permissionsGroup->analytics_status)

                    <li {{ ($PathCurrentFolder==$currentFolder)  ? 'class=active' : '' }}>
                        <a>
                            <span class="nav-caret">
                                <i class="fa fa-caret-down"></i>
                            </span>
                            <span class="nav-icon">
                                <i class="material-icons">&#xe1b8;</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.manufacturers_analytics') }}</span>
                        </a>

                    </li>
                    @endif
                    @endif


                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('manufacturers') }}" onclick="location.href='{{ route('manufacturers') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">shopping_basket</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.all_manufacturers') }}</span>
                        </a>
                    </li>
                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                        <a href="{{ route('manufacturersCreate') }}" onclick="location.href='{{ route('manufacturersCreate') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">add_box</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.add') }}</span>
                        </a>
                    </li>

                    {{-- End Manufacturers Section --}}

                    {{-- Start Trademarks Section --}}
                    
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">{{ trans('admin.trademarks') }}</small>
                    </li>
                    <?php 
                        $currentFolder = "trademarks"; // Put folder name here
                        $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                        ?>
                        
                        @if(Helper::GeneralWebmasterSettings("analytics_status"))
                        @if(@Auth::user()->permissionsGroup->analytics_status)

                        <li {{ ($PathCurrentFolder==$currentFolder)  ? 'class=active' : '' }}>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="material-icons">&#xe1b8;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.trademarks_analytics') }}</span>
                            </a>

                        </li>
                        @endif
                        @endif


                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('trademarks.index') }}" onclick="location.href='{{ route('trademarks.index') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">work</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.all_trademarks') }}</span>
                            </a>
                        </li>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('trademarks.create') }}" onclick="location.href='{{ route('manufacturersCreate') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.add') }}</span>
                            </a>
                        </li>
                        {{-- End Trademarks Section --}}

                        @endif 


                        {{-- Start Event Cordinators Section --}}
                        @if(@Auth::user()->permissionsGroup->ec_status)
                        <li class="nav-header hidden-folded">
                            <small class="text-muted">{{ trans('admin.event_coordinators') }}</small>
                        </li>
                        <?php 
                        $currentFolder = "event_coordinators"; // Put folder name here
                        $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                        ?>
                        
                        @if(Helper::GeneralWebmasterSettings("analytics_status"))
                        @if(@Auth::user()->permissionsGroup->analytics_status)

                        <li {{ ($PathCurrentFolder==$currentFolder)  ? 'class=active' : '' }}>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="material-icons">&#xe1b8;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.ec_analytics') }}</span>
                            </a>

                        </li>
                        @endif
                        @endif
                        
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('event_coordinators.index') }}" onclick="location.href='{{ route('event_coordinators.index') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">perm_identity</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.all_event_coordinators') }}</span>
                            </a>
                        </li>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a href="{{ route('event_coordinators.create') }}" onclick="location.href='{{ route('manufacturersCreate') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.add') }}</span>
                            </a>
                        </li>
                        @endif 
                        {{-- End Event Cordinators Section --}}



                        {{-- Start purchases Section --}}
                        @if(@Auth::user()->permissionsGroup->purchases_status)
                        <li class="nav-header hidden-folded">
                            <small class="text-muted">{{ trans('backLang.purchases') }}</small>
                        </li>

                        <li>
                            <a onclick="location.href='{{ route('purchases-orders') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">shopping_basket</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.all_purchases') }}</span>
                            </a>
                        </li>
                        <li>
                            <a onclick="location.href='{{ route('po-orderlist') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">shopping_basket</i>
                                </span>
                                <span class="nav-text">{{trans('backLang.purchases_orderlist')}}</span>
                            </a>
                        </li>
                        <li>
                            <a onclick="location.href='{{ route('purchases-orders-create') }}'">
                                <span class="nav-icon">
                                    <i class="material-icons">add_box</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.addPurchase') }}</span>

                            </a>
                        </li>


                        @endif
                        {{-- end purchases Section --}}
                        {{-- Start sales Section --}}
                        @if(@Auth::user()->permissionsGroup->sales_status)
                        <li class="nav-header hidden-folded">
                         <small class="text-muted">{{ trans('backLang.sales') }}</small>
                     </li>

                     <li>
                        <a onclick="location.href='{{ route('sales-orders') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">shopping_basket</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.all_sales') }}</span>
                        </a>
                    </li>

                    <li>
                        <a onclick="location.href='{{ route('so-orderlist') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">shopping_basket</i>
                            </span>
                            <span class="nav-text">{{trans('backLang.sales_orders_list')}}</span>
                        </a>
                    </li>


                    <li>
                        <a onclick="location.href='{{ route('sales-orders-create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">add_box</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.addSalesOrder') }}</span>

                        </a>
                    </li>
                    @if(@Auth::user()->permissionsGroup->pos_status)
                    <li>
                        <a onclick="location.href='{{ route('sales-bills') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">event_note</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.all_sales_bills') }}</span>
                        </a>
                    </li>

                    <li>
                        <a onclick="location.href='{{ route('sb-orderlist') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">event_note</i>
                            </span>
                            <span class="nav-text">{{trans('backLang.sales_bills_orderlist')}}</span>
                        </a>
                    </li>

                    <li>
                        <a onclick="location.href='{{ route('sales-bills-create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">note_add</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.addSalesBill') }}</span>

                        </a>
                    </li>

                        <hr>
                            <li class="nav-header hidden-folded">
                                <small class="text-muted">{{ trans('backLang.all_daily_sales_bills') }}</small>
                            </li>

                            <li>
                                <a onclick="location.href='{{ route('daily-sales-bills') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">note_add</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.invoices_daily') }}</span>

                                </a>
                            </li>


                            <li>
                                <a onclick="location.href='{{ route('dsb-orderlist') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">note_add</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.dsb-orderlist') }}</span>

                                </a>
                            </li>

                            <li>
                                <a onclick="location.href='{{ route('daily-sales-bills-create') }}'">
                                    <span class="nav-icon">
                                        <i class="material-icons">note_add</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.adddailySalesBill') }}</span>

                                </a>
                            </li>

                        <hr>

                    @endif


                    @if(@Auth::user()->permissionsGroup->pos_status)
                    <li>
                        <a onclick="location.href='{{ route('buy-orders') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">event_note</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.all_buy_orders') }}</span>
                        </a>
                    </li>
                    <li>
                        <a onclick="location.href='{{ route('bo-orderlist') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">event_note</i>
                            </span>
                            <span class="nav-text">{{trans('backLang.buy_orders_orderlist')}}</span>
                        </a>
                    </li>
                    <li>
                        <a onclick="location.href='{{ route('buy-orders-create') }}'">
                            <span class="nav-icon">
                                <i class="material-icons">note_add</i>
                            </span>
                            <span class="nav-text">{{ trans('backLang.add_buy_orders') }}</span>

                        </a>
                    </li>
                    @endif
                    @endif
                    {{-- end sales Section --}}

                    @if(Helper::GeneralWebmasterSettings("settings_status"))
                    @if(@Auth::user()->permissionsGroup->settings_status)
                    <li class="nav-header hidden-folded">
                        <small class="text-muted">{{ trans('backLang.settings') }}</small>
                    </li>

                    <?php
                            $currentFolder = "settings"; // Put folder name here
                            $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));

                            $currentFolder2 = "menus"; // Put folder name here
                            $PathCurrentFolder2 = substr($urlAfterRoot, 0, strlen($currentFolder2));

                            $currentFolder3 = "users"; // Put folder name here
                            $PathCurrentFolder3 = substr($urlAfterRoot, 0, strlen($currentFolder2));
                            ?>
                            <li {{ ($PathCurrentFolder==$currentFolder || $PathCurrentFolder2==$currentFolder2 || $PathCurrentFolder3==$currentFolder3 ) ? 'class=active' : '' }}>
                                <a>
                                    <span class="nav-caret">
                                        <i class="fa fa-caret-down"></i>
                                    </span>
                                    <span class="nav-icon">
                                        <i class="material-icons">&#xe8b8;</i>
                                    </span>
                                    <span class="nav-text">{{ trans('backLang.generalSiteSettings') }}</span>
                                </a>
                                <ul class="nav-sub">
                                    <?php
                                    $currentFolder = "settings"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('settings') }}"
                                        onclick="location.href='{{ route('settings') }}'">
                                        <span class="nav-text">{{ trans('backLang.generalSettings') }}</span>
                                    </a>
                                </li>
                                <?php
                                    $currentFolder = "menus"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('menus') }}">
                                            <span class="nav-text">{{ trans('backLang.siteMenus') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                    $currentFolder = "users"; // Put folder name here
                                    $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                                    ?>
                                    <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('users') }}">
                                            <span class="nav-text">{{ trans('backLang.usersPermissions') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            @endif


                            @if(@Auth::user()->permissionsGroup->webmaster_status)
                            <?php
                        $currentFolder = "webmaster"; // Put folder name here
                        $PathCurrentFolder = substr($urlAfterRoot, 0, strlen($currentFolder));
                        ?>
                        <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }}>
                            <a>
                                <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="material-icons">&#xe8be;</i>
                                </span>
                                <span class="nav-text">{{ trans('backLang.webmasterTools') }}</span>
                            </a>
                            <ul class="nav-sub">
                                <?php
                                $PathCurrentSubFolder = substr($urlAfterRoot, 0, (strlen($currentFolder) + 1));
                                ?>
                                <li {{ ($PathCurrentFolder==$PathCurrentSubFolder) ? 'class=active' : '' }}>
                                    <a href="{{ route('webmasterSettings') }}"
                                    onclick="location.href='{{ route('webmasterSettings') }}'">
                                    <span class="nav-text">{{ trans('backLang.generalSettings') }}</span>
                                </a>
                            </li>
                            <?php
                                $currentSubFolder = "sections"; // Put folder name here
                                $PathCurrentSubFolder = substr($urlAfterRoot, (strlen($currentFolder) + 1),
                                    strlen($currentSubFolder));
                                    ?>
                                    <li {{ ($PathCurrentSubFolder==$currentSubFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('WebmasterSections') }}">
                                            <span class="nav-text">{{ trans('backLang.siteSectionsSettings') }}</span>
                                        </a>
                                    </li>
                                    <?php
                                $currentSubFolder = "banners"; // Put folder name here
                                $PathCurrentSubFolder = substr($urlAfterRoot, (strlen($currentFolder) + 1),
                                    strlen($currentSubFolder));
                                    ?>
                                    <li {{ ($PathCurrentSubFolder==$currentSubFolder) ? 'class=active' : '' }}>
                                        <a href="{{ route('WebmasterBanners') }}">
                                            <span class="nav-text">{{ trans('backLang.adsBannersSettings') }}</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            @endif
                            <?php
                            $data_sections_arr = explode(",", Auth::user()->permissionsGroup->data_sections);
                            ?>


                            <li class="nav-header hidden-folded">
                                <small class="text-muted">{{ trans('backLang.siteData') }}</small>
                            </li>

                            @foreach($GeneralWebmasterSections as $GeneralWebmasterSection)
                            @if(in_array($GeneralWebmasterSection->id,$data_sections_arr))


                            <?php
                            $LiIcon = "&#xe2c8;";
                            if ($GeneralWebmasterSection->type == 3) {
                                $LiIcon = "&#xe050;";
                            }
                            if ($GeneralWebmasterSection->type == 2) {
                                $LiIcon = "&#xe63a;";
                            }
                            if ($GeneralWebmasterSection->type == 1) {
                                $LiIcon = "&#xe251;";
                            }
                            if ($GeneralWebmasterSection->type == 0) {
                                $LiIcon = "&#xe2c8;";
                            }
                            if ($GeneralWebmasterSection->name == "sitePages") {
                                $LiIcon = "&#xe3e8;";
                            }
                            if ($GeneralWebmasterSection->name == "articles") {
                                $LiIcon = "&#xe02f;";
                            }
                            if ($GeneralWebmasterSection->name == "services") {
                                $LiIcon = "&#xe540;";
                            }
                            if ($GeneralWebmasterSection->name == "news") {
                                $LiIcon = "&#xe307;";
                            }
                            if ($GeneralWebmasterSection->name == "products") {
                                $LiIcon = "&#xe8f6;";
                            }

                            // get 9 char after root url to check if is "webmaster"
                            $is_webmaster = substr($urlAfterRoot, 0, 9);
                            ?>
                            @if($GeneralWebmasterSection->sections_status > 0)
                            <li {{ ($GeneralWebmasterSection->id == @$WebmasterSection->id && $is_webmaster != "webmaster") ? 'class=active' : '' }}>
                                <a>
                                  <span class="nav-caret">
                                    <i class="fa fa-caret-down"></i>
                                </span>
                                <span class="nav-icon">
                                    <i class="material-icons">{!! $LiIcon !!}</i>
                                </span>
                                <span class="nav-text">{!! str_replace("backLang.","",trans('backLang.'.$GeneralWebmasterSection->name)) !!}</span>
                            </a>
                            <ul class="nav-sub">
                                @if($GeneralWebmasterSection->sections_status > 0)

                                <?php
                                            $currentFolder = "sections"; // Put folder name here
                                            $PathCurrentFolder = substr($urlAfterRoot,
                                                (strlen($GeneralWebmasterSection->id) + 1), strlen($currentFolder));
                                                ?>
                                                <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }} >
                                                    <a href="{{ route('sections',$GeneralWebmasterSection->id) }}">
                                                        <span class="nav-text">{{ trans('backLang.sectionsOf') }} {{ str_replace("backLang.","",trans('backLang.'.$GeneralWebmasterSection->name)) }}</span>
                                                    </a>
                                                </li>
                                                @endif

                                                <?php
                                        $currentFolder = "topics"; // Put folder name here
                                        $PathCurrentFolder = substr($urlAfterRoot,
                                            (strlen($GeneralWebmasterSection->id) + 1), strlen($currentFolder));
                                            ?>
                                            <li {{ ($PathCurrentFolder==$currentFolder) ? 'class=active' : '' }} >
                                                <a href="{{ route('topics',$GeneralWebmasterSection->id) }}">
                                                    <span class="nav-text">{!! str_replace("backLang.","",trans('backLang.'.$GeneralWebmasterSection->name)) !!}</span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                    @else
                                    <li {{ ($GeneralWebmasterSection->id== @$WebmasterSection->id) ? 'class=active' : '' }}>
                                        <a href="{{ route('topics',$GeneralWebmasterSection->id) }}">
                                            <span class="nav-icon">
                                                <i class="material-icons">{!! $LiIcon !!}</i>
                                            </span>
                                            <span class="nav-text">{!! str_replace("backLang.","",trans('backLang.'.$GeneralWebmasterSection->name)) !!}</span>
                                        </a>
                                    </li>
                                    @endif
                                    @endif
                                    @endforeach
                                </ul>

                            </nav>
                        </div>
                    </div>
                </div>