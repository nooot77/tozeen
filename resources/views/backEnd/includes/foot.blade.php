<script type="text/javascript">
    var public_lang = "{{ trans('backLang.calendarLanguage') }}"; // this is a public var used in app.html.js to define path to js files
    var public_folder_path = "{{ URL::to('') }}"; // this is a public var used in app.html.js to define path to js files
</script>
<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/scripts/app.html.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/scripts/myfunctions.js') }}"></script>
{!! Helper::SaveVisitorInfo("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") !!}


<script type="text/javascript" src="{{ URL::to('backEnd/scripts/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/scripts/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/scripts/datatables.net-bs/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('/vendor/datatables/buttons.server-side.js') }}"></script>

<script type="text/javascript" src="{{ URL::to('backEnd/jstree/jstree.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/jstree/jstree.wholerow.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('backEnd/jstree/jstree.checkbox.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.ar.min.js"></script>
{{-- <script src="https://unpkg.com/vue-select@latest"></script> --}}

@yield('vueincludes')


@stack('js')
@stack('css')

