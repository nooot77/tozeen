
@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
      <div class="box-header dker">
      <h3>تعديل طلب مشتريات</h3>
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <po-edit-component 
      :syslng="{{json_encode(session('locale'))}}"
      :vendors="{{json_encode($vendors)}}" 
      :currencies="{{json_encode($currencies)}}" 
      :items="{{json_encode($items)}}"
      :id="{{$id}}"
      ></po-edit-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->


@endsection

@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>
@endsection
