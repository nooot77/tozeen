
@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
      <div class="box-header dker">
          <h3>
            {{ trans('admin.add_new_po') }}
          </h3>
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <po-create-component
      :syslng="{{json_encode(session('locale'))}}"
      :vendors="{{json_encode($vendors)}}" 
      :currencies="{{json_encode($currencies)}}" 
      :items="{{json_encode($items)}}"
      :lastnumber="{{$lastNumber}}"
      ></po-create-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->


@endsection


@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>
@endsection