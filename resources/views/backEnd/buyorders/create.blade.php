
@extends('backEnd.layout')

@section('content')


<div class="padding">
  <div class="box">
      <div class="box-header dker">
        <center>
            <h3>
                {{ trans('admin.add_new_bo') }}
              </h3>
        </center>
     
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <buy-orders-create-component
      :syslng="{{json_encode(session('locale'))}}"
      :cities="{{json_encode($cities)}}" 
      :ec="{{json_encode($ec)}}"
      :items="{{json_encode($items)}}"
      :tries="{{json_encode($tries)}}"
      :lastnumber="{{$lastNumber}}"
      v-bind:userId="{{$userId}}"
      v-bind:branch="{{$branch}}"
      ></buy-orders-create-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<script>
  window.userId = {!! json_encode($userId); !!};
</script>

@endsection


@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>

@endsection