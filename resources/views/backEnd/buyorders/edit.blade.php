
@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
      <div class="box-header dker">
      <h3>تعديل طلب مشتريات</h3>
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <buy-orders-create-component 
      :syslng="{{json_encode(session('locale'))}}"
      :branches="{{json_encode($branches)}}" 
      :cities="{{json_encode($cities)}}" 
      :ec="{{json_encode($ec)}}"
      :items="{{json_encode($items)}}"
      :tries="{{json_encode($tries)}}"
      :lastnumber="{{$lastNumber}}"
      v-bind:userId="{{$userId}}"
      ></buy-orders-create-component>
    </div>
  
  </div>
  <!-- /.box-body -->
  <script>
    window.userId = {!! json_encode($userId); !!};
  </script>
</div>
<!-- /.box -->


@endsection

@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>
@endsection
