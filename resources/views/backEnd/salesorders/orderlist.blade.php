@extends('backEnd.layout')


@include('backEnd.includes.carbon')
@section('content')

    <div class="padding">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ trans('backLang.all_sales_orders') }}</h3>
            </div>
            @push('js')
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#printInvoice').click(function () {
                            Popup($('.invoice')[0].outerHTML);

                            function Popup(data) {
                                window.print();
                                return true;
                            }
                        });
                    });
                    $(document).ready(function () {
                        // Add minus icon for collapse element which is open by default
                        $(".collapse.show").each(function () {
                            $(this).prev(".input-group").find(".fa").addClass("fa-minus").removeClass(
                                "fa-plus");
                        });

                        // Toggle plus minus icon on show hide of collapse element
                        $(".collapse").on('show.bs.collapse', function () {
                            $(this).prev(".input-group").find(".fa").removeClass("fa-plus").addClass(
                                "fa-minus");
                        }).on('hide.bs.collapse', function () {
                            $(this).prev(".input-group").find(".fa").removeClass("fa-minus").addClass(
                                "fa-plus");
                        });

                    });


                </script>


                {{-- end script  --}}
            @endpush
            @push('css')
                @if(Session::get('locale') == 'ar')

                    <link rel="stylesheet" href="{{asset('css/orderlist-ar.css')}}">

                @else
                    <link rel="stylesheet" href="{{asset('css/orderlist-en.css')}}">

            @endif
        @endpush

        <!-- /.box-header -->
            <div class="row" style="margin-bottom:3vw">
                <div class="col-md-6">
                    <a href="{{route('sales-bills-create')}}" style="margin-right:2.5vw" class="btn btn-create btn-success btn-sm"><i class="fa fa-plus"></i>
                        {{trans('backLang.addSalesOrder')}}
                    </a>
                </div>
            </div>


            <div class="time-filter row">
                <div class="col-md-2">
                    <p>{{trans('backLang.time_filter')}}</p>
                </div>

                <div class="dropdown col-md-3 show">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ...
                    </a>

                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="{{route('sb-orderlist')}}">{{trans('admin.all_record')}}</a>
                        <a class="dropdown-item" href="{{route('sb-orderlist')}}?duration=1">{{trans('backLang.hour')}}</a>
                        <a class="dropdown-item" href="{{route('sb-orderlist')}}?duration=2"> 6 {{trans('backLang.hours')}}</a>
                        <a class="dropdown-item" href="{{route('sb-orderlist')}}?duration=3"> {{trans('backLang.day')}}</a>
                        <a class="dropdown-item" href="{{route('sb-orderlist')}}?duration=4"> {{trans('backLang.week')}}</a>
                    </div>
                </div>
            </div>

            <div class="order-list ">
                @foreach($SalesOrders as $index => $SalesOrder)
                    <div class="row ">
                        <div class="container">
                            <div class="box  box-primary col-md-9">

                                <div class="table-responsive">
                                    <table class="table"> <!-- start table-->

                                        <thead style="background-color: #2E3E4E;color:white;"> <!--  start table head-->
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">{{trans('backLang.invoice_number')}}</th>
                                            <th scope="col">{{trans('backLang.manufacturer_name')}}</th>

                                            <th scope="col">{{trans('admin.Referance')}}</th>
                                            <th scope="col">{{trans('admin.created_at')}}</th>
                                            <th scope="col">{{trans('admin.print')}}</th>
                                            <th scope="col">{{trans('admin.details')}}</th>
                                        </tr>
                                        </thead> <!-- end table head-->

                                        <tbody> <!-- table body -->
                                        <tr>
                                            <td>{{$index + 1}}</td>
                                            <td>{{$SalesOrder->number}}</td>
                                            <td>{{$SalesOrder->client_name}}</td>
                                            <td>{{$SalesOrder->reference}}</td>
                                            <td>{{(new $carbon($SalesOrder->created_at))->diffForHumans()}}</td>
                                            <td><a href="{{route('sb-invoice',['id'=>$SalesOrder->id])}}" class="btn btn-info btn-sm"><i class="fa fa-print"></i></a></td>
                                            <td>

                                                <div class="col-md-2">
                                                    <a class="btn btn-primary btn-sm" type="button" data-toggle="collapse" data-target="#div_{{$index + 1}}"
                                                       aria-expanded="false" aria-controls="collapseExample">
                                                        <i class="fa fa-arrow-down"></i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody> <!-- end table body -->







                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="collapse table-responsive container " id="div_{{$index + 1}}">

                            <div class="box  col-md-8 box-primary">
                                <?php $price = 0?>



                                <h5 class="text-center">{{trans('admin.products')}}</h5>
                                <table class="table ">
                                    <thead class="thead-light">


                                    <tr>
                                        <th class="text-left">#</th>
                                        <th class="text-left">{{trans('backLang.all_code')}}</th>
                                        <th class="text-left">{{trans('admin.product')}}</th>
                                        <th class="text-left">{{trans('admin.Qty')}}</th>
                                        <th class="text-left"> {{trans('backLang.discount')}}</th>
                                        <th class="text-left">{{trans('admin.price')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($SalesOrder->orderItems as $item_index => $item)
                                        <tr>
                                            <td>{{$item_index + 1}}</td>

                                            <th scope="row">{{$item->product['code']}}</th>


                                            <td>{{$item->product['title']}}</td>
                                            <td>{{$item->qty. " " . trans('backLang.kilo')}}</td>
                                            <td>{{$item->discount}} %</td>

                                            <td>{{$item->unit_price . " "
                                         . trans('backLang.sar')}}  </td>

                                            <?php $price += ($item->unit_price * $item->qty) - ($item->unit_price* $item->discount);  ?>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                                <div class="box">
                                    <div class="box-head container">
                                        <div class="row">
                                            <h5 class="text-center">{{trans('backLang.summry')}}</h5>
                                        </div>

                                        <table class="table table-responsive"> <!-- start table-->

                                            <tr>
                                                <td scope="col" class="total">{{trans('backLang.total_b_tax')}}</td>
                                                <td scope="col "> {{$price . " " . trans('backLang.sar')}}  </td>
                                            </tr>
                                            <tr>
                                                <td class="total">{{trans('backLang.tax')}}</td>
                                                <td> 5% </td>
                                            </tr>

                                            <tr>
                                                <td class="total">{{trans('backLang.total_a_tax')}}</td>
                                                <td> {{($price += $price * (5/100)) . " " . trans('backLang.sar')}}  </td>
                                            </tr>


                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


            </div>
        </div>

@endsection










