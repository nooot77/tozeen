
@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
      <div class="box-header dker">
          <h3>
            {{ trans('admin.add_new_sale') }}
          </h3>
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <sales-create-component
      :syslng="{{json_encode(session('locale'))}}"
      :clients="{{json_encode($clients)}}" 
      :cities="{{json_encode($cities)}}" 
      :currencies="{{json_encode($currencies)}}" 
      :items="{{json_encode($items)}}"
      :lastnumber="{{$lastNumber}}"
      v-bind:userId="{{$userId}}"
      ></sales-create-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<script>
  window.userId = {!! json_encode($userId); !!};
</script>

@endsection


@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>
@endsection