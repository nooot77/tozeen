
@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
      <div class="box-header dker">
        <center>
            <h3>
                {{ trans('admin.add_new_sb') }}
              </h3>
        </center>
     
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <daily-sales-bills-create-component
      :syslng="{{json_encode(session('locale'))}}"
      :branches="{{json_encode($branches)}}" 
      v-bind:cities="{{$cities}}" 
      :ec="{{json_encode($ec)}}"
      :items="{{json_encode($items)}}"
      :lastnumber="{{$lastNumber}}"
      v-bind:userId="{{$userId}}"
      ></daily-sales-bills-create-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
<script>
  window.userId = {!! json_encode($userId); !!};
  window.cities = {!! json_encode($cities); !!};

</script>

@endsection


@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>
@endsection