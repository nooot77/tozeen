@extends('admin.index')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('reviews/'.$review->id),'method'=>'put']) !!}
    <div class="form-group">
      {!! Form::label('name',trans('admin.name')) !!}
      {!! Form::text('name',$review->name,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('phone',trans('admin.phone')) !!}
      {!! Form::text('phone',$review->phone,['class'=>'form-control']) !!}
    </div>
      <div class="form-group">
      {!! Form::label('message',trans('admin.message')) !!}
      {!! Form::textarea('message',$review->message,['class'=>'form-control']) !!}
    </div>
     <div class="clearfix"></div>
  <div class="form-group col-md-2 col-lg-2 col-sm-2 col-xs-12">
    {!! Form::label('status',trans('admin.status')) !!}
    {!!
    Form::select('status',
    ['pending'=>trans('admin.pending'),'refused'=>trans('admin.refused'),'active'=>trans('admin.active')]
    ,$review->status,['class'=>'form-control status'])
    !!}
  </div>
   <div class="clearfix"></div>
    {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
