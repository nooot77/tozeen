@include('website.layouts.header')
@include('website.layouts.navbar')


 

<!------ Include the above in your HEAD tag ---------->

                    {{-- @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('warning'))
                        <div class="alert alert-warning">
                            {{ session('warning') }}
                        </div>
                    @endif
            
            <form method="post" action="{{ route('contactus.store') }}">
			    {{ csrf_field() }}
                <h3>Contact Us</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" name="name" class="form-control" placeholder="Your Name *"  required />
				 @if ($errors->has('name'))
										<span class="help-block">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
				 @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" name="email" class="form-control" placeholder="Your Email *"  required />
							 @if ($errors->has('email'))
										<span class="help-block">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
							 @endif
                        </div>
                         <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="phone" name="phone" class="form-control" placeholder="Your phone *"  required />
							 @if ($errors->has('phone'))
										<span class="help-block">
											<strong>{{ $errors->first('phone') }}</strong>
										</span>
							 @endif
                        </div>
                        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                            <input type="text" name="title" class="form-control" placeholder="Subject *"  />
							@if ($errors->has('title'))
										<span class="help-block">
											<strong>{{ $errors->first('title') }}</strong>
										</span>
							 @endif
                        </div>
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" class="btn btn-primary btn-round btn-sm" value="Send Message" />
							
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea name="message" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;" required></textarea>
			     @if ($errors->has('message'))
				<span class="help-block">
				<strong>{{ $errors->first('message') }}</strong>
				</span>
			    @endif
                        </div>
                    </div>
                </div>
            </form> --}}


        <!-- Start Contact -->
        <div id="contact" class="contact section">
            <div class="container">
                <div class="section-title">
                    <h2 data-title="Get in touch">{{trans('website.contact_us')}}</h2>
                </div>
                <div class="contact-content section-content">
                    <form class="contact-form" action="{{ route('contactus.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="small-fields">
                            <div class="form-group">
                                <input id="name" type="text" name="name" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="name">{{trans('website.name')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="email" type="email" name="email" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="email">{{trans('website.email')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="phone" type="tel" name="phone" value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="phone">{{trans('website.phone')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="title" type="text" name="title" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="title">{{trans('website.subject')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea id="message" name="message" required data-value="" onkeyup="this.setAttribute('data-value', this.value);"></textarea>
                            <label for="message">{{trans('website.message')}}</label>
                            <span class="border-bottom"></span>
                        </div>
                        <div class="buttons-container">
                            <button class="rounded-btn main-color-two fill" type="submit">{{trans('website.send_message')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Contact -->
        
@include('website.layouts.footer')