@extends('website.index')
@section('content')
     <!-- Start About -->
        <div id="about" class="about section">
            <div class="container">
                <div class="section-title">
                    <h2 data-title="{{trans('website.who_we_are')}}">{{trans('website.about')}}</h2>
                </div>
                <div class="about-content section-content">
                    <div class="about-video">
                       @if(!empty(Helper::website()->video_url))
                    <img src="{{Helper::website()->video_url}}" alt="video-button">
                      @endif
                      @if(!empty(Helper::website()->video_thamnail))
                      <a href="{{Storage::url(Helper::website()->video_thamnail)}}" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit">
                            <img src="{{Storage::url(Helper::website()->video_thamnail)}}" alt="about-video">
                    @endif
                    @if(empty(Helper::website()->video_thamnail))
                        <a href="{{ url('/') }}" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit">
                            <img src="https://via.placeholder.com/700x500" alt="about-video">
                            @endif
                            <span class="video-icon">
                                <img src="{{ url('design/website') }}/images/play-button2.svg" alt="about-video">
                            </span>
                        </a>
                    </div>
                    <div class="about-text section-text">
                        <h3 class="section-sub-heading">{{trans('website.our_mission')}}</h3>
                      @if(Helper::direction() == 'rtl')
                         <p>
                            {{Helper::website()->about_bodyb_ar}}
                        </p>
                        @endif
                           @if(Helper::direction() == 'ltr')
                          <p>
                             {{Helper::website()->about_bodyb_en}}
                        </p>
                        @endif
                        <div class="video-play-button">
                            <a href="#">
                                <img src="{{ url('design/website') }}/images/play-button.svg" alt="video-button">
                                 {{trans('website.watch_our_story')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End About -->

         {{-- <!-- Start Services -->
        <div id="services" class="services section padding0">
            <div class="bg-shapes">
                <img src="{{ url('design/Helper::website') }}/images/wave-shapes.png" alt="">
            </div>
            <div class="div-margin">
                <div class="container">
                    <div class="section-title">
                    <h2 data-title="What we do">{{trans('Helper::website.services')}}</h2>
                    </div>
                    <div class="services-content section-content">
                        <div class="services-item">
                            <div class="services-item-icon">
                                <img src="{{ url('design/Helper::website') }}/images/services/services-1.svg" alt="services-1">
                            </div>
                            <h3 class="services-title">Graphic Design</h3>
                            <p>
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                            </p>
                            <div class="buttons-container">
                                <a class="rounded-btn main-color-one fill" href="#">Read More</a>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-item-icon">
                                <img src="{{ url('design/Helper::website') }}/images/services/services-2.svg" alt="services-2">
                            </div>
                            <h3 class="services-title">Photography</h3>
                            <p>
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                            </p>
                            <div class="buttons-container">
                                <a class="rounded-btn main-color-one fill" href="#">Read More</a>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-item-icon">
                                <img src="{{ url('design/Helper::website') }}/images/services/services-3.svg" alt="services-3">
                            </div>
                            <h3 class="services-title">Marketing</h3>
                            <p>
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
                            </p>
                            <div class="buttons-container">
                                <a class="rounded-btn main-color-one fill" href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="bg-shapes">
                <img src="{{ url('design/Helper::website') }}/images/wave-shapes.png" alt="">
            </div>
        </div>
        <!-- End Services --> --}}


           <!-- Start Testimonials -->
        <div id="testimonials" class="testimonials section padding0">
            <div class="bg-shapes">
                <img src="{{ url('design/website') }}/images/wave-shapes.png" alt="">
            </div>
            <div class="div-margin">
                <div class="container">
                    <div class="section-title">
                        <h2 data-title="What people say">{{trans('website.testimonials')}}</h2>
                    </div>
                </div>
                <div class="testimonials-content section-content">
                	<div class="slider owl-carousel owl-theme">
                         @foreach($reviews as $key => $rev)
                        <div class="item">
                        <div class="client-details">
                            <img src="https://via.placeholder.com/140x140" alt="testimonials-1">
                            <div class="name">
                                <h3>{{$rev->name}}</h3>
                                <h5 style="text-align:right">{{date('d-m-Y', strtotime($rev->created_at))}}</h5>
                            </div>
                        </div>
                        <p>
                          {{$rev->message}}
                        </p>
                        <div class="quotes">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                        </div> 
                    </div>
                    @endforeach
          
                    </div>
                </div>
            </div>
			<div class="bg-shapes">
                <img src="{{ url('design/website') }}/images/wave-shapes.png" alt="">
            </div>
        </div>
        <!-- End Testimonials -->

              <!-- Start Portfolio -->
        <div id="portfolio" class="portfolio section">
            <div class="container">
                <div class="section-title">
                    <h2 data-title="our best work">{{trans('website.portfolio')}}</h2>
                </div>
                <div class="portfolio-content section-content">
                    {{-- <ul class="work-cat">
                        <li class="active">All</li>
                        <li data-value="graphic">Graphic Design</li>
                        <li data-value="web">Web Design</li>
                        <li data-value="photography">Photography</li>
                        <li><a href="portfolio.html">More</a></li>
                    </ul> --}}
                    <div class="portfolio-items">
                         {{-- @foreach($images as $key => $image) --}}
                        {{-- <a class="item animated zoomIn photography" href="{{ $image }}"> --}}
                            {{-- <img src="{{ $image }}" alt="portfolio-1"> --}}
                     <div class="overlay">
                                <h3 class="title animated fadeInUp">tozeen</h3>
                                <h4 class="category animated fadeInUp"></h4>
                            </div>
                        </a>
                       
                        {{-- @endforeach --}}
                    </div>
                </div>
            </div>
        </div>
        <!-- End Portfolio -->
@endsection