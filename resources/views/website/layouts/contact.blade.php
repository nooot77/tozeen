<div id="contact" class="contact section">
            <div class="container">
                <div class="section-title">
                    <h2 data-title="Get in touch">{{trans('website.contact_us')}}</h2>
                </div>
                <div class="contact-content section-content">
                    <form class="contact-form" action="{{ route('contactus.store') }}" method="post">
                        {{ csrf_field() }}
                        <div class="small-fields">
                            <div class="form-group">
                                <input id="name" type="text" name="name" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="name">{{trans('website.name')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="email" type="email" name="email" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="email">{{trans('website.email')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="phone" type="tel" name="phone" value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="phone">{{trans('website.phone')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                            <div class="form-group">
                                <input id="title" type="text" name="title" required value="" onkeyup="this.setAttribute('value', this.value);">
                                <label for="title">{{trans('website.subject')}}</label>
                                <span class="border-bottom"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea id="message" name="message" required data-value="" onkeyup="this.setAttribute('data-value', this.value);"></textarea>
                            <label for="message">{{trans('website.message')}}</label>
                            <span class="border-bottom"></span>
                        </div>
                        <div class="buttons-container">
                            <button class="rounded-btn main-color-two fill" type="submit">{{trans('website.send_message')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>