
<!DOCTYPE html>
@if(Helper::direction() == 'rtl')
<html lang="en" dir="rtl">
@endif 
<html lang="en" dir="ltr">

    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Website Title in The Browser Toolbar -->
        <title>Tozeen</title>
        <!-- Website Favicon Icon -->
        <link rel="icon" href="{{ url('design/website') }}/images/sewak-icon.png">
        <!-- Google Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,400,500%7CPlayball">
        <!-- Normalize Css File -->
        <link rel="stylesheet" href="{{ url('design/website') }}/css/normalize.css">
        <!-- Font Awesome Css File -->
        <link rel="stylesheet" href="{{ url('design/website') }}/css/font-awesome.min.css">
        <!-- Animate.css File -->
        <link rel="stylesheet" href="{{ url('design/website') }}/css/animate.min.css">
        <!-- Magnific Popup Plugin -->
        <link rel="stylesheet" href="{{ url('design/website') }}/css/magnific-popup.css">
        <!-- Owl Carousel Css Files -->
        <link rel="stylesheet" href="{{ url('design/website') }}/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{ url('design/website') }}/css/owl.theme.default.min.css">
        <link rel="stylesheet" href="{{ url('adminlte') }}/bower_components/font-awesome-5/css/all.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

          {{-- <link rel="stylesheet" href="{{ url('/') }}/design/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css"> --}}

        <!-- Website Style File -->
        @if(Helper::direction() == 'rtl')
        <link rel="stylesheet" href="https://www.fontstatic.com/f=flat-jooza,flat-jooza,yassin">

        <link rel="stylesheet" href="{{ url('design/website') }}/css/style-rtl.css">
        @endif 
        @if(Helper::direction() == 'ltr')

        <link rel="stylesheet" href="{{ url('design/website') }}/css/style-rtl.css">
        <link rel="stylesheet" href="{{ url('design/website') }}/css/style-ltr.css">
     @endif 
    </head>
     @if(Helper::direction() == 'rtl')
       <style>
            body {
            font-family: 'flat-jooza', sans-serif;
            font-size: 15px;
            line-height: 1.3;
           }
           .container{
               text-align: right;
           }
           header .overlay nav .navigation .nav-menu-container {
            /* left:-250px;   */
            right:inherit;
           }
           header .overlay nav .navigation .direct-links li{
              margin-right: 10px;
              margin-left: 10px; 
           }
           .slider-item{
            margin-top: 100px;
           }
           /* For Mobile */
            @media screen and (max-width: 540px) {
                .websiteHeaderpic {
                    visibility: hidden;
                }
            }

            /* For Tablets */
            @media screen and (min-width: 540px) and (max-width: 780px) {
                .websiteHeaderpic {
                    visibility: hidden;
                }
            }
        </style>
         @endif
    <body class="waves-style">
       
      <!-- Start Preloader -->
        <svg style="display: none;">
            <defs>
                <filter >
                    {{-- <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"/> --}}
                    <feColorMatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="filter"/>
                    {{-- <feComposite in="SourceGraphic" in2="filter" operator="atop"/> --}}
                </filter>
            </defs>
        </svg>
        
        <div class="preloader">
            <div class="content">
                <div class="item"></div>
                <div class="item"></div>
                <div class="item"></div>
                <div class="item"></div>
                <div class="item"></div>
            </div>
        </div>
        <!-- End Preloader -->