      <!-- Start Header -->
        <header id="home" class="home section padding0 has-shapes not-img-bg">
            <div class="overlay">
                <div class="bg-shapes">
                    <img src="{{ url('design/website') }}/images/wave-shapes.png" alt="">
                </div>
                <nav>
                    <div class="container">
                        <h1 class="logo">

                            {{-- 
                                           @if(Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')) !="")
                        <img alt=""
                             src="{{ URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode'))) }}">
                    @else
                        <img alt="" src="{{ URL::to('uploads/settings/nologo.png') }}">
                    @endif
                                --}}
                                  @if(Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')) !="")
                       <a  href="{{url('/')}}">
                                  <img src="{{  URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')))  }}"   alt="website image" style="width:auto;height: 40px;"/>

                       </a>
                               
                                    @else
                                    <img alt="" src="{{ URL::to('uploads/settings/nologo.png') }}">
                                @endif
                        </h1>
                        <div class="navigation">
                            <ul class="direct-links">
                                <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fas fa-language"></i>
                                <span class="hidden-xs"> </span>
                                </a>
                                <ul class="dropdown-menu">
                                <li><a href="{{ Helper::aurl('lang/ar') }}">العربية</a></li>
                                <li><a href="{{ Helper::aurl('lang/en') }}">English</a></li>
                                </ul>
                            </li>
                                <li><a href="{{url('/gallery')}}">{{trans('website.our_products')}}</a></li>
                                <li><a href="{{url('/reviews')}}">{{trans('website.reviews')}}</a></li>
                                <li><a href="{{url('/about')}}">{{trans('website.about')}}</a></li>
                                <li><a href="{{url('/contact')}}">{{trans('website.contact_us')}}</a></li>

                       
                            </ul>
                        <a class="rounded-btn main-color-one fill reverse shadow" href="{{url('/')}}">{{trans('website.home')}}</a>
                            <div class="nav-menu-button hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <div class="nav-menu-container">
                                <h2 class="logo">
                                    @if(Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')) !="")
                                    <a  href="{{url('/')}}">
                                       <img src="{{  URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')))  }}"   alt="website image" style="width:auto;height: 40px;"/>
             
                                    </a>
                                            
                                     @else
                                       <img alt="" src="{{ URL::to('uploads/settings/nologo.png') }}">
                                     @endif
                                </h2>
                                <ul class="nav-menu">
                                    <li class="menu-item"><a class="active" href="#" data-value="#home">{{trans('website.home')}}</a></li>
                                    <li class="menu-item"><a href="#" data-value="#about">{{trans('website.about')}}</a></li>
                                    <li class="menu-item"><a href="#" data-value="#services">{{trans('website.services')}}</a></li>
                                    <li class="menu-item"><a href="#" data-value="#portfolio">{{trans('website.portfolio')}}</a></li>
                                    {{-- <li class="menu-item"><a href="#" data-value="#team">Our Team</a></li> --}}
                                    <li class="menu-item"><a href="#" data-value="#testimonials">{{trans('website.testimonials')}}</a></li>
                                    {{-- <li class="menu-item"><a href="#" data-value="#clients">Clients</a></li>
                                    <li class="menu-item"><a href="#" data-value="#pricing">Pricing</a></li> --}}
                                    <li class="menu-item"><a href="#" data-value="#contact">{{trans('website.contact_us')}}</a></li>
                                    <li class="menu-item"><a href="{{url('/gallery')}}" >{{trans('website.our_products')}}</a></li>
                    

                                </ul>
                                <div class="signIn">
                                    <ul class="nav">
                                        <li class="menu"   style="align:center; padding-top:50px;">
                                            <a href="{{URL::to('/admin')}}" style=" color:aqua;text-decoration:none;padding:60px; font-size:14px;">
                                                {{trans('website.singIN')}}
                                                <i class="fas fa-sign-in-alt" aria-hidden="true"></i>

                                            </a>
                                           

                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </nav>
                
                <!-- Header Slider -->
                <div id="header-slider" class="slider">
                    <div class="container">
                        <div class="slider-content">
                            <div class="slider-item">
                                <h4 class="sub-head">
                                   {{trans('website.about_head')}}

                                </h4>
                                 @if(Helper::direction() == 'rtl')
                         <h2>
                            {{Helper::website()->about_title_ar}}
                         </h2>
                         <p>
                             {{Helper::website()->about_bodys_ar}}
                       </p>  
                        @endif
                           @if(Helper::direction() == 'ltr')
                          <h2>
                             {{Helper::website()->about_title_en}}
                          </h2>
                          <p>
                             {{Helper::website()->about_bodys_en}}
                       </p> 
                        @endif
                              
                             
                                <div class="buttons-container">
                                <a class="rounded-btn main-color-one fill reverse shadow" href="{{url('/about')}}"> {{trans('website.contact_us')}} </a>
                                <a class="rounded-btn white-one fill reverse shadow" href="{{url('/reviews')}}"> {{trans('website.write_reveiw')}} </a>
                                </div>
                            </div>

                            <div class="websiteHeaderpic" style="
                            float: left;
                            margin-top: -650px;
                            margin-right: 700px;
                        ">
                                <img src="{{ url('design/website') }}/images/tozeenWebsitePic.png" alt="" style="
                                width: 400px;
                                margin-top: 100px;
                               
                            ">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Header Slider -->
            </div>
        </header>
        <!-- End Header -->