
        <!-- Start Footer -->
        <footer>
            <div class="bg-shapes">
                <img src="{{ url('design/website') }}/images/wave-shapes.png" alt="">
            </div>
            <div class="div-margin">
                <div class="container">
                    <div class="footer-top">
                        <div class="widget links">
                            <h3 class="widget-title">{{trans('website.tozeen')}}</h3>
                            <div class="widget-content">
                                <ul>
                                    <li><a href="#">{{trans('website.company')}}</a></li>
                                    {{-- <li><a href="#">Community</a></li> --}}
                                    <li><a href="#">{{trans('website.careers')}}</a></li>
                                    <li><a href="#">{{trans('website.term_of_use')}}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget links">
                            <h3 class="widget-title">{{trans('website.locations')}}</h3>
                            <div class="widget-content">
                                <ul>
                                      
                                <li class="address"><a href="https://goo.gl/maps/6FN4Sofkxtk">  {{Helper::website()->location1}}</a></li>
                                    <li class="address"><a href="https://goo.gl/maps/NR3qHHa4BVr"> {{Helper::website()->location2}}</a></li>
                                    <li class="address"><a href="https://goo.gl/maps/kwGGpUifo952"> {{Helper::website()->location3}}</a></li>
                                    {{-- <li><a href="#">Video</a></li> --}}
                                </ul>
                            </div>
                        </div>
                        <div class="widget contact-widget">
                            <h3 class="widget-title">{{trans('website.contact_us')}}</h3>
                            <div class="widget-content">
                                <ul>
                                    <li class="email">
                                       {{Helper::website()->email1}}
                                    </li>
                                    <li class="email">
                                      {{Helper::website()->email2}}
                                    </li>
                                    <li class="phone">
                                      {{Helper::website()->number1}}
                                    </li>
                                     <li class="phone">
                                        {{Helper::website()->number2}}
                                    </li>
                                      <li class="phone">
                                        {{Helper::website()->number3}} 
                                    </li>
                                      <li class="phone">
                                     {{Helper::website()->number4}} <br>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget instagram-widget">
                            <h3 class="widget-title">{{trans('website.instagram')}}</h3>
                            <div class="widget-content">
                                <ul>
                         {{-- @foreach($images as $key => $image)

                                    <li>
                                        <a href="{{$image}}">
                                        <img src="{{$image}}" alt="tozeen">
                                        </a>
                                    </li>
                    
                                    @endforeach --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer-bottom">
                        <div>
                             @if(Helper::direction() == 'rtl')
                            {{Helper::website()->footer_ar}}
                            @endif
                             @if(Helper::direction() == 'ltr')
                            {{Helper::website()->footer_en}}
                            @endif
                        </div>
                        <div>
                                @if(Helper::direction() == 'rtl')
                             توزين للشوكلاته مبرمجة بفخر من قبل محمد العبيد &copy; 2019
                            
                            @endif
                             @if(Helper::direction() == 'ltr')
                            &copy; 2019 Tozeen chocolate is Proudly Powered by <a href="{{url('/')}}">Mohamad Alobaid</a>
                            
                            @endif
                        </div>
        <iframe src="https://maroof.sa/Business/GetStamp?bid=84780" style=" width: auto; height: 250px;  "  frameborder="0" seamless='seamless' scrollable="no"></iframe>

                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
        
        <!-- Scroll To Top Button -->
        <div id="Scroll-To-Top" class="Scroll-To-Top" title="Back To Top">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </div>
        <!-- Scroll To Top Button -->
        
        
        <!-- jQuery Library -->
        <script src="{{ url('design/website') }}/js/jquery-3.3.1.min.js"></script>
        <!-- Ribbons Plugin -->
        {{-- <script src="{{ url('design/website') }}/js/ribbons.js"></script> --}}
        <!-- Magnific Popup Plugin -->
        <script src="{{ url('design/website') }}/js/jquery.magnific-popup.min.js"></script>
        <!-- Tilt Plugin -->
        <script src="{{ url('design/website') }}/js/tilt.jquery.min.js"></script>
        <!-- Direction Aware Hover Plugin -->
        <script src="{{ url('design/website') }}/js/direction.aware.hover.min.js"></script>
        <!-- Owl Carousel Slider Plugin -->
        <script src="{{ url('design/website') }}/js/owl.carousel.min.js"></script>
        <!-- Nice Scroll Plugin -->
        <script src="{{ url('design/website') }}/js/jquery.nicescroll.min.js"></script>
        <!-- Website Script File -->
        <script src="{{ url('design/website') }}/js/script.js"></script>
        {{-- <script src="{{ url('/design/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script> --}}
        {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
    
</html>