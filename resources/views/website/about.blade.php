@include('website.layouts.header')
@include('website.layouts.navbar')

     <!-- Start About -->
        <div id="about" class="about section">
            <div class="container">
                <div class="section-title">
                    <h2 data-title="{{trans('website.who_we_are')}}">{{trans('website.about')}}</h2>
                </div>
                <div class="about-content section-content">
                    <div class="about-video">
                                     
                    @if(!empty(Helper::website()->video_url))
                    <img src="{{Helper::website()->video_url}}" alt="video-button">
                      @endif
                      @if(!empty(Helper::website()->video_thamnail))
                      <a href="{{Storage::url(Helper::website()->video_thamnail)}}" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit">
                            <img src="{{Storage::url(Helper::website()->video_thamnail)}}" alt="about-video">
                    @endif
                    @if(empty(Helper::website()->video_thamnail))
                        <a href="{{ url('/') }}" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit">
                            <img src="https://via.placeholder.com/700x500" alt="about-video">
                            @endif
                            <span class="video-icon">
                                <img src="{{ url('design/website') }}/images/play-button2.svg" alt="about-video">
                            </span>
                        </a>
                    </div>
                    <div class="about-text section-text">
                        <h3 class="section-sub-heading">{{trans('website.our_mission')}}</h3>
                        @if(Helper::direction() == 'rtl')
                         <p>
                            {{Helper::website()->about_bodyb_ar}}
                        </p>
                        @endif
                           @if(Helper::direction() == 'ltr')
                          <p>
                             {{Helper::website()->about_bodyb_en}}
                        </p>
                        @endif
                     
                        <div class="video-play-button">
                            <a href="#">
                   
                                <img src="{{ url('design/website') }}/images/play-button.svg" alt="video-button">
                                {{trans('website.watch_our_story')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End About -->
@include('website.layouts.footer')