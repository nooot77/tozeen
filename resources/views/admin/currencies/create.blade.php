@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('currencies')]) !!}
     <div class="form-group">
        {!! Form::label('name',trans('admin.currency_name')) !!}
        {!! Form::text('name',old('name'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('code',trans('admin.code')) !!}
        {!! Form::text('code',old('code'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('decimal_place',trans('admin.place_decimal')) !!}
        {!! Form::number('decimal_place',old('decimal_place'),['class'=>'form-control']) !!}
     </div>


     {!! Form::submit(trans('admin.add'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection