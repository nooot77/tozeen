@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('currencies/'.$currency->id),'method'=>'put','files'=>true ]) !!}
   <div class="form-group">
        {!! Form::label('name',trans('admin.currency_name')) !!}
        {!! Form::text('name',$currency->name,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('code',trans('admin.code')) !!}
        {!! Form::text('code',$currency->code,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('decimal_place',trans('admin.place_decimal')) !!}
        {!! Form::number('decimal_place',$currency->decimal_place,['class'=>'form-control']) !!}
     </div>

     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection