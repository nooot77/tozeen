@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('event_coordinators')]) !!}

    <!-- 'name',
    'office',
    'phone',
    'commission',
    'city_id',
    'start_date',
    'end_date', -->
     <div class="form-group">
        {!! Form::label('name',trans('admin.name')) !!}
        {!! Form::text('name',old('name'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('office',trans('admin.office')) !!}
        {!! Form::text('office',old('office'),['class'=>'form-control']) !!}
     </div>



     <div class="form-group">
        {!! Form::label('phone',trans('admin.phone')) !!}
        {!! Form::text('phone',old('phone'),['class'=>'form-control']) !!}
     </div>



     <div class="form-group">
        {!! Form::label('commission',trans('admin.commission')) !!}
        {!! Form::text('commission',old('commission'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('city_id',trans('admin.city_id')) !!}
        {!! Form::select('city_id',App\Model\City::pluck('city_name_'.session('locale'),'id'),old('city_id'),['class'=>'form-control']) !!}
     </div>

  <div class="form-group">
        {!! Form::label('b_city_id',trans('admin.b_city_id')) !!}
        {!! Form::select('b_city_id',App\Model\City::pluck('city_name_'.session('locale'),'id'),old('b_city_id'),['class'=>'form-control']) !!}
     </div>


     <div class="form-group">
        {!! Form::label('start_date',trans('admin.start_date')) !!}
        {!! Form::date('start_date',\Carbon\Carbon::now()) !!}
     </div>


     <div class="form-group">
        {!! Form::label('end_date',trans('admin.end_date')) !!}
        {!! Form::date('end_date',\Carbon\Carbon::now()) !!}
     </div>



     {!! Form::submit(trans('admin.add'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
