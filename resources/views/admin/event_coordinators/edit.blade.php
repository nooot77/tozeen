@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->


  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('event_coordinators/'.$event_coordinators->id),'method'=>'put' ]) !!}

   <div class="form-group">
        {!! Form::label('name',trans('admin.name')) !!}
        {!! Form::text('name',$event_coordinators->name,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('office',trans('admin.office')) !!}
        {!! Form::text('office',$event_coordinators->office,['class'=>'form-control']) !!}
     </div>



     <div class="form-group">
        {!! Form::label('phone',trans('admin.phone')) !!}
        {!! Form::text('phone',$event_coordinators->phone,['class'=>'form-control']) !!}
     </div>



     <div class="form-group">
        {!! Form::label('commission',trans('admin.commission')) !!}
        {!! Form::text('commission',$event_coordinators->commission,['class'=>'form-control']) !!}
     </div>


     <div class="form-group">
       {!! Form::label('city_id',trans('admin.city_id')) !!}
       {!! Form::select('city_id',App\Model\City::pluck('city_name_'.session('locale'),'id'),['class'=>'form-control']) !!}
     </div>
  <div class="form-group">
        {!! Form::label('b_city_id',trans('admin.b_city_id')) !!}
        {!! Form::select('b_city_id',App\Model\City::pluck('city_name_'.session('locale'),'id'),old('b_city_id'),['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('start_date',trans('admin.start_date')) !!}
        {!! Form::date('start_date',$event_coordinators->start_date )!!}
     </div>


     <div class="form-group">
        {!! Form::label('end_date',trans('admin.end_date')) !!}
        {!! Form::date('end_date',$event_coordinators->end_date  )!!}
     </div>

     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
