@extends('backEnd.layout')
@include('backEnd.includes.errors')
@if(Session::has('success'))

<div class="alert alert-success">

    {{ Session::get('success') }}

    @php

    Session::forget('success');

    @endphp

</div>

@endif
@section('content')
<div class="padding">
  <div class="box">
 @push('js')
 <script type="text/javascript" src='https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyBwxuW2cdXbL38w9dcPOXfGLmi1J7AVVB8'></script>
 <script type="text/javascript" src='{{  URL::to('backEnd/libs/js/locationpicker.jquery.js') }}'></script>
<?php
$lat = !empty(old('lat'))?old('lat'):'24.713552';
$lng = !empty(old('lng'))?old('lng'):'46.675297';

?>
 <script>
      $('.datepicker').datepicker({
    rtl: '{{session('locale') == 'ar'?true:false}}',
    language: '{{session('locale')}}',
    format: 'yyyy/mm/dd',
    autoclose: false,
    todayBtn: true,
    clearBtn: true,
  });

  $('#us1').locationpicker({
      location: {
          latitude: {{ $lat }},
          longitude:{{ $lng }}
      },
      radius: 300,
      markerIcon: '{{  URL::to('backEnd/assets/images/marker_3.png') }}',
      inputBinding: {
        latitudeInput: $('#lat'),
        longitudeInput: $('#lng'),
       // radiusInput: $('#us2-radius'),
        locationNameInput: $('#address')
      }

  });
 </script>
  <script type="text/javascript">
   $(document).ready(function(){
     @if(old('country_id'))
     $.ajax({
         url:'{{ url('admin/cities/create') }}',
         type:'get',
         dataType:'html',
         data:{country_id:'{{ old('country_id') }}',select:'{{ old('city_id') }}'},
         success: function(data)
         {
           $('.city').html(data);
         }
       });
     @endif
    $(document).on('change','.country_id',function(){
     var country = $('.country_id option:selected').val();
     if(country > 0)
     {
       $.ajax({
         url:'{{ url('admin/cities/create') }}',
         type:'get',
         dataType:'html',
         data:{country_id:country,select:''},
         success: function(data)
         {
           $('.city').html(data);
         }
       });
     }else{
           $('.city').html('');
     }
    });
   });
   </script>
 @endpush

  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('manufacturers'),'files'=>true]) !!}
    <input type="hidden" value="{{ $lat }}" id="lat" name="lat">
    <input type="hidden" value="{{ $lng }}" id="lng" name="lng">
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('name_ar',trans('admin.name_ar')) !!}
        {!! Form::text('name_ar',old('name_ar'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('name_en',trans('admin.name_en')) !!}
        {!! Form::text('name_en',old('name_en'),['class'=>'form-control']) !!}
     </div>



     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('contact_name',trans('admin.contact_name')) !!}
        {!! Form::text('contact_name',old('contact_name'),['class'=>'form-control']) !!}
     </div>


     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('mobile',trans('admin.mobile')) !!}
        {!! Form::text('mobile',old('mobile'),['class'=>'form-control']) !!}
     </div>

     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
         {!! Form::label('email',trans('admin.email')) !!}
         {!! Form::email('email',old('email'),['class'=>'form-control']) !!}
      </div>
 
 
     
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
         {!! Form::label('contract_date',trans('admin.contract_date')) !!}
         {!! Form::text('contract_date',old('contract_date'),['class'=>'form-control datepicker']) !!}
       </div>
       <div class="clearfix"></div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('address',trans('admin.address')) !!}
        {!! Form::text('address',old('address'),['class'=>'form-control address']) !!}
     </div>
     
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
         {!! Form::label('country_id',trans('admin.country_id')) !!}
         {!! Form::select('country_id',App\Model\Country::pluck('title_'.session('locale'),'id'),old('country_id'),['class'=>'form-control country_id','placeholder'=>'.............']) !!}
      </div>
      
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
         {!! Form::label('city_id',trans('admin.city_id')) !!}
         {{-- {!! Form::select('city_id',App\Model\City::pluck('city_name_'.session('locale'),'id'),old('city_id'),['class'=>'form-control','placeholder'=>'.............']) !!} --}}
         <span class="city"></span>
      </div>
     <div class="clearfix"></div>
     <div class="form-group">
       <div id="us1" style="width: 100%; height: 400px;"></div>
     </div>

     <div class="clearfix"></div>


     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12 ">
         {!! Form::label('icon',trans('admin.manufacturers_icon')) !!}
         {!! Form::file('icon',['class'=>'form-control']) !!}
      </div>

     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('facebook',trans('admin.facebook')) !!}
        {!! Form::text('facebook',old('facebook'),['class'=>'form-control']) !!}
     </div>


     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('twitter',trans('admin.twitter')) !!}
        {!! Form::text('twitter',old('twitter'),['class'=>'form-control']) !!}
     </div>



     {!! Form::submit(trans('admin.add'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
