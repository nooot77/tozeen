@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('admin'),'files'=>true]) !!}
     <div class="form-group">
        {!! Form::label('fname',trans('admin.fname')) !!}
        {!! Form::text('fname',old('fname'),['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('lname',trans('admin.lname')) !!}
        {!! Form::text('lname',old('lname'),['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('email',trans('admin.email')) !!}
        {!! Form::email('email',old('email'),['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('password',trans('admin.password')) !!}
        {!! Form::password('password',['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
       {!! Form::label('pic',trans('admin.pic')) !!}
       {!! Form::file('pic',['class'=>'form-control']) !!}
     </div>



     {!! Form::submit(trans('admin.create_admin'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
