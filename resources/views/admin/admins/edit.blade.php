@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('admin/'.$admin->id),'method'=>'put' ,'files'=>true]) !!}
    <div class="form-group">
       {!! Form::label('fname',trans('admin.fname')) !!}
       {!! Form::text('fname',$admin->fname,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
       {!! Form::label('lname',trans('admin.lname')) !!}
       {!! Form::text('lname',$admin->lname,['class'=>'form-control']) !!}
    </div>
     <div class="form-group">
        {!! Form::label('email',trans('admin.email')) !!}
        {!! Form::email('email',$admin->email,['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('password',trans('admin.password')) !!}
        {!! Form::password('password',['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
       <img src="{{ asset('storage/' .admin()->user()->pic)  }}" style="width:60px;height: 60px; " />
     </div>
     <div class="form-group">
       {!! Form::label('pic',trans('admin.pic')) !!}
       {!! Form::file('pic',['class'=>'form-control']) !!}
     </div>

     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection
