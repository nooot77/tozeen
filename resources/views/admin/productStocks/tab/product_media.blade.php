@push('js')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
 
  <script type="text/javascript">
 
 Dropzone.autoDiscover = false;
 $(document).ready(function(){

var myDropzone = new Dropzone("div#dropzonefileupload", { 
  url: "{{ Helper::aurl('upload/image/'.$product->id) }}",
      method:'POST',
      paramName:'file',
      uploadMultiple:true,
      maxFiles:99,
      maxFilessize:99,
      addRemoveLinks:true,
      autoProcessQueue:true,
      // parallelUploads:10,
      acceptedFiles:'image/*',
      dictDefaultMessage:'{{ trans('admin.upload_message')}}',
      dictRemoveFile:'{{ trans('admin.delete') }}',
      thumbnailWidth:80,
      thumbnailHeight:80,
      addRemoveLinks:true,
      removedfile:function(file){
        $.ajax({
          dataType:'json',
          type:'post',
          url:'{{ Helper::aurl('delete/image/')}}',
          data: { _token:'{{ csrf_token() }}',id:file.file_id},
        });
      var fmock;
      return (fmock =file.previewElement) != null ? fmock.parentNode.removeChild(file.previewElement):void 0 ;
      },
      params:{
       _token:'{{ csrf_token() }}'
      },
      init:function(){

       @foreach($product->files()->get() as $file)
       var mock = {file_id:'{{$file->id}}',name:'{{$file->name}}',size:'{{$file->size}}',type:'{{$file->mime_type}}'}
       this.emit('addedfile',mock);
       this.options.thumbnail.call(this,mock,'{{url('storage/'.$file->full_file)}}');
       $('.dz-progress').remove();
       @endforeach
       this.on('sending',function(file,xhr,formData){
           formData.append('file_id','');
           file.file_id = '';
       });
       this.on('success',function(file,response){
         file.file_id = response.id;
       });

       }      
  });
  
var myDropzone = new Dropzone("div#mainphoto", { 
  url: "{{ Helper::aurl('update/image/'.$product->id) }}",
      method:'POST',
      paramName:'file',
      uploadMultiple:false,
      maxFiles:1,
      maxFilessize:3,
      addRemoveLinks:true,
      autoProcessQueue:true,
      // parallelUploads:10,
      acceptedFiles:'image/*',
      dictDefaultMessage:'{{ trans('admin.mainphoto_message')}}',
      dictRemoveFile:'{{ trans('admin.delete') }}',
      thumbnailWidth:80,
      thumbnailHeight:80,
      addRemoveLinks:true,
      removedfile:function(file){
        $.ajax({
          dataType:'json',
          type:'post',
          url:'{{ Helper::aurl('delete/product/image/'.$product->id)}}',
          data: { _token:'{{ csrf_token() }}'},
        });
      var fmock;
      return (fmock =file.previewElement) != null ? fmock.parentNode.removeChild(file.previewElement):void 0 ;
      },
      params:{
       _token:'{{ csrf_token() }}'
      },
      init:function(){
          
         @if(!empty($product->photo))
       var mock = {name:'{{$product->title}}',size:'',type:''}
       this.emit('addedfile',mock);
       this.options.thumbnail.call(this,mock,'{{url('storage/'.$product->photo)}}');
       $('.dz-progress').remove();
         @endif

       this.on('sending',function(file,xhr,formData){
           formData.append('file_id','');
           file.file_id = '';
       });
       this.on('success',function(file,response){
         file.file_id = response.id;
       });

         }      
  });
});
  </script>  
<style type="text/css">
.dz-image img{
  width:100px;
  height:100px;
}
</style>
@endpush

    <div id="product_media" class="tab-pane" role="tabpanel">
      <h3>{{trans('admin.product_media')}}</h3>
      <center>
      <h5>
      {{trans('admin.main_photo')}}
      </h5>
      </center>
      <hr />
    <div class="dropzone" id="mainphoto">  </div>
      <hr />
       <center>
      <h5>
      {{trans('admin.photos')}}
      </h5>
      </center>
    <div class="dropzone" id="dropzonefileupload"> 

      </div>

    </div>