
@extends('backEnd.layout')

@section('content')


<div class="padding">
  <div class="box">
      <div class="box-header dker">
        <center>
            <h3>
                {{ trans('admin.add_stock_first_qty') }}
              </h3>
        </center>
     
      </div>

  <div class="box-header">
 
  </div>
  <!-- /.box-header -->
  <div class="box-body" >
    <div id="app">
      <!-- Vue Component-->
      <product-stocks-component
      :syslng="{{json_encode(session('locale'))}}"
      :branches="{{json_encode($branches)}}"  
      :items="{{json_encode($items)}}"
      ></product-stocks-component>
    </div>
  
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->

@endsection


@section('vueincludes')
<script type="text/javascript" src="{{ asset('js/vue/app.js') }}"></script>

@endsection