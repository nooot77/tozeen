@push('js')
<script src="{{ url('/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>

<!-- <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script> -->
<script>
  CKEDITOR.replace('reason_ckeditor');
</script>

<script>
  $('.datepicker').datepicker({
    rtl: '{{session('locale') == 'ar'?true:false}}',
    localeuage: '{{session('locale')}}',
    format: 'yyyy/mm/dd',
    autoclose: false,
    todayBtn: true,
    clearBtn: true,
  });

  $(document).on('change','.status',function(){
       var status =$('.status option:selected').val();
       if(status == 'refused')
       {
         $('.reason').removeClass('invisible');
       }else{
         $('.reason').addClass('invisible');
       }
  });


</script>
@endpush

<div id="product_settings" class="tab-pane" role="tabpanel">
  <h3 style="margin:30px;">{{trans('admin.product_info')}}</h3>


  
  <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('title',trans('admin.title')) !!}
      {!! Form::text('title',$product->title,['class'=>'form-control']) !!}
    </div>
   <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('code',trans('admin.pcode')) !!}
      {!! Form::number('code',$product->code,['class'=>'form-control']) !!}
    </div>
  
       <div class="form-group  col-md-4 col-lg-4 col-sm-4 col-xs-12">
          {!! Form::label('fill_id',trans('admin.fill')) !!}
          {!! Form::select('fill_id',App\Model\Fill::pluck('name_'.session('locale'),'id'),$product->fill_id, ['class'=>'form-control select2','placeholder' => trans('admin.select_msg')]) !!}
       </div>



       <div class="form-group col-md-3 col-lg-3 col-sm-3 col-xs-12">
          {!! Form::label('price',trans('admin.sale_price')) !!}
          {!! Form::text('price',$product->price,['class'=>'form-control']) !!}
        </div>

  <div class="form-group col-md-3 col-lg-3 col-sm-3 col-xs-12">
    {!! Form::label('stock',trans('admin.stock')) !!}
    {!! Form::text('stock',$product->stock,['class'=>'form-control']) !!}
  </div>

  {{-- <div class="form-group col-md-3 col-lg-3 col-sm-3 col-xs-12">
    {!! Form::label('start_at',trans('admin.start_at')) !!}
    {!! Form::text('start_at',$product->start_at,['class'=>'form-control datepicker']) !!}
  </div>
  <div class="form-group col-md-3 col-lg-3 col-sm-3 col-xs-12">
    {!! Form::label('end_at',trans('admin.end_at')) !!}
    {!! Form::text('end_at',$product->end_at,['class'=>'form-control datepicker']) !!}
  </div> --}}

  <div class="clearfix"></div>

  <div class="form-group col-md-3 col-lg-3 col-sm-3 col-xs-12">
      {!! Form::label('cal',trans('admin.cal')) !!}
      {!! Form::text('cal',$product->cal,['class'=>'form-control']) !!}
    </div>
    <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
        {!! Form::label('prd_date',trans('admin.prd_date')) !!}
        {!! Form::text('prd_date',$product->prd_date,['class'=>'form-control datepicker','autocomplete'=>'off']) !!}
      </div>
  <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
    {!! Form::label('exp_date',trans('admin.exp_date')) !!}
    {!! Form::text('exp_date',$product->exp_date,['class'=>'form-control datepicker','autocomplete'=>'off']) !!}
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('trade_id',trans('admin.trademark')) !!}
      {!! Form::select('trade_id',App\Model\TradeMark::pluck('name_'.session('locale'),'id'),$product->trade_id,['class'=>'form-control select2','placeholder' => trans('admin.select_msg')]) !!}
   </div>
   <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('manu_id',trans('admin.manufacturer')) !!}
      {!! Form::select('manu_id',App\Model\Manufacturers::pluck('name_'.session('locale'),'id'),$product->manu_id,['class'=>'form-control select2','placeholder' => trans('admin.select_msg')]) !!}
   </div>
   <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('country_id',trans('admin.country')) !!}
      {!! Form::select('country_id',App\Model\Country::pluck('title_'.session('locale'),'id'),$product->country_id,['class'=>'form-control select2','placeholder' => trans('admin.select_msg')]) !!}
   </div>
  <div class="clearfix"></div>
 
    <h4 style="margin:30px;"> القسم الخاص بالمتجر</h4>
 
  
  <div class="clearfix"></div>
  <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('price_offer',trans('admin.price_offer')) !!}
      {!! Form::text('price_offer',$product->price_offer,['class'=>'form-control']) !!}
    </div>
  
    <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('start_offer_at',trans('admin.start_offer_at')) !!}
      {!! Form::text('start_offer_at',$product->start_offer_at,['class'=>'form-control datepicker','autocomplete'=>'off']) !!}
    </div>
    <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('end_offer_at',trans('admin.end_offer_at')) !!}
      {!! Form::text('end_offer_at',$product->end_offer_at,['class'=>'form-control datepicker','autocomplete'=>'off']) !!}
    </div>
  
    <div class="clearfix"></div>
  
  <div class="form-group">
    {!! Form::label('content',trans('admin.content')) !!}
    {!! Form::textarea('content',$product->content,['class'=>'form-control','id'=>'content_ckeditor']) !!}
  </div>
  <div class="clearfix"></div>
  <div class="form-group col-md-2 col-lg-2 col-sm-2 col-xs-12">
    {!! Form::label('status',trans('admin.status')) !!}
    {!!
    Form::select('status',
    ['pending'=>trans('admin.pending'),'refused'=>trans('admin.refused'),'active'=>trans('admin.active')]
    ,$product->status,['class'=>'form-control status'])
    !!}
  </div>
  <div class="clearfix"></div>

  <div class="form-group  reason {{$product->status != 'refused'?'invisible':''}}">
    {!! Form::label('reason',trans('admin.reason')) !!}
    {!! Form::textarea('reason',$product->reason,['class'=>'form-control','id'=>'reason_ckeditor']) !!}
  </div>
  
</div>

