@extends('backEnd.layout')
@section('content')
@push('js')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"></script>

<script>
  $(window).ready(function () {

$('#jstree').jstree({
      "core": {
        'data': {!!Helper::load_dep() !!},
        "themes": {
          "variant": "large"
        }
      },
      "checkbox": {
        "keep_selected_style": true
      },
      "plugins": ["wholerow"] //checkbox
    });

  

  

  $('#jstree').on("changed.jstree", function (e, data) {

      $.ajax({  
        url: "{{Helper::aurl('search/image')}}",
        type: 'GET',
        dataType: 'json',
        data: {imgSearch: '', depId: data.selected[0]},

        success: function(data) {
       
        $('.putResult').html(data.result);


        
      },error(response){

        console.log(response);
      }
      



    });
  console.log(data.selected[0]);
});




    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });



    $('.search-input').on('keyup', function() {



      $.ajax({
        url: "{{Helper::aurl('search/image')}}",
        type: 'GET',
        dataType: 'json',
        data: {imgSearch: $(this).val()},

        success: function(data) {
       
        $('.putResult').html(data.result);


        
      },error(response){

       // console.log(response);
      }
      



    });
    });



  });
</script>

@endpush



<div class="box">
  <div class="box-header">
    <h3 class="box-title">section</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
   
    <div id="jstree"></div>

  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->





<div class="padding">
  <div class="box">
    <div class="box-header">
      <h2 class="box-title text-center m-b"> @lang('backLang.media_gallery')</h2>
      <input class="form-control search-input" type="search" name="get_product">
    </div>
    <!-- /.box-header -->
    <div class="box-body">


     {{---------  image search --------}}
     <div id="delete-img" class="container-img-delete my-5">


      <div class="row putResult">




        @foreach($products as $product)
        <div class='col-md-2 m-a'>
          <div class='img-conetn-manage mt-3'>
            <h6 class="text-dark">{{$product->title}}</h6>
            <p>{{$product->code}}</p>


            @if($product->photo)
            <img width='100%' height='100%' src='{{Storage::url($product->photo)}}'>  
            @else
            <img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'> 

            @endif


            <div class='content-icon'>



              <a href='#' type="button"  data-toggle="modal" data-target="#exampleModalCenter{{$product->id}}"> <i class='far fa-trash-alt'></i> Zoom in</a>



            </div>
          </div>

        </div>

        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{$product->title}}  || {{$product->code}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">

               @if($product->photo)
               <img width='100%' height='100%' src='{{Storage::url($product->photo)}}'>  
               @else
               <img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'> 

               @endif

             </div>
             <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

            </div>
          </div>
        </div>
      </div>
      @endforeach









      
    </div>
  </div>



  {{--------- end image delete --------}}


</div>
<!-- /.box-body -->
</div>
<!-- /.box -->
{{ $products->links() }}


@endsection
