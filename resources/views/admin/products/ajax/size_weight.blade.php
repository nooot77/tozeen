@push('js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"></script>
<script>
$(document).ready(function() {
    $('.select2').select2();
});
</script>
@endpush

<div>
<div class="clearfix"></div>

<div class="col-md-6">

    <div class="form-group">
        <label for="sizes" class="col-md-3">{{trans('admin.size_id')}}</label>
        <div class="col-md-9">
          {{-- {!! Form::label('size_id',trans('admin.size_id')) !!} --}}

            {!! Form::select('size_id',$sizes,$product->size,['class'=>'form-control','placeholder'=>trans('admin.size_id')]) !!}

        </div>
    </div>

    <div class="form-group">
        <label for="sizes" class="col-md-3">{{trans('admin.numbers_unit')}}</label>
        <div class="col-md-9">
            {!! Form::text('size_id',$product->size_id,['class'=>'form-control','placeholder'=>trans('admin.number_unit')]) !!}
        </div>
    </div>

</div>
<div class="col-md-6">


    <div class="form-group">
        <label for="weights" class="col-md-3">{{trans('admin.weight_id')}}</label>
        <div class="col-md-9">
            {!! Form::select('weight_id',$weights,$product->weight,['class'=>'form-control select','placeholder'=>trans('admin.weight_id')])
            !!}
        </div>
    </div>
    <div class="form-group">
	
    <label for="weights" class="col-md-3">{{trans('admin.units_weight')}}</label>
        <div class="col-md-9">
            {!! Form::text('weight_id',$product->weight_id,['class'=>'form-control','placeholder'=>trans('admin.weight')])
            !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
</div>
