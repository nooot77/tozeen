@extends('backEnd.layout')
@section('content')
@push('js')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/javascript.util/0.12.12/javascript.util.min.js"></script>
<script>
$(document).ready(function() {
    $('.select2').select2();
});
</script>

<script>
$('#department a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
</script>

<script>
  $(document).ready(function() {
    $(document).on('click','.save_and_continue',function(){
    var form_data = $('#product_form').serialize();
    $.ajax({
      url:'{{ Helper::aurl('products/'.$product->id)}}',
      dataType:'json',
      type:'post',
      data:form_data, 
      beforeSend: function(){
        $('.loading_save_and_c').removeClass('invisible');
        $('.validate_message').html('').addClass('invisible');
        $('.success_message').html('').addClass('invisible');

      },success: function(data){
        if(data.status == true) { 
        $('.loading_save_and_c').addClass('invisible');
        $('.success_message').html('<h2>'+data.message+'</h2>').removeClass('invisible');
            }
      },error(response){
        $('.loading_save_and_c').addClass('invisible');
        var error_li = '';
        $.each(response.responseJSON.errors,function(index,value){
             error_li +='<li>'+value+'</li>';
        });
        $('.error_message').removeClass('invisible');
        $('.validate_message').html(error_li);

      }

        });
        return false;
      });
});

</script>
@endpush

<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['files'=>true,'id'=>'product_form','url'=>Helper::aurl('products'),'method'=>'put','enctype' => 'multipart/form-data',])!!}


      <div class="container-fluid" style="margin-bottom: 30px; margin-top: 30px; ">

  <a href="#" class="btn btn-sm btn-primary navbar-btn save">
    {{trans('admin.save')}}
    <i class="fa fa-floppy-o" aria-hidden="true"></i>
    </a>

  <a href="#" class="btn btn-sm btn-success navbar-btn save_and_continue">
        {{trans('admin.save_and_continue')}}
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        <i class="invisible fa fa-spin fa-spinner loading_save_and_c hidden " aria-hidden="true" ></i>
  </a>

  <a href="#" class="btn btn-sm btn-info navbar-btn copy_product">
        {{trans('admin.copy_product')}}
    <i class="fa fa-clone" aria-hidden="true"></i>
   </a>

  <a href="#" class="btn btn-sm btn-danger navbar-btn delete">
        {{trans('admin.delete')}}
  <i class="fa fa-trash"></i>

   </a>
       <hr/>
         <div class="alert alert-danger error_message invisible">
             <ul class="validate_message">

             </ul>
         </div>
         <div class="alert alert-success success_message invisible">
         
      </div>
       <hr/>     

      </div>
  
      <div class="clearfix"></div>

    <ul class="nav nav-tabs nav-fill nav-justified" role="tablist">
     
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#department">{{trans('admin.department')}}        </a>
      </li> 

      <li class="nav-item">
        <a class="nav-link"  data-toggle="pill" href="#product_settings">{{trans('admin.product_settings')}}</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="pill"  href="#product_media">{{trans('admin.product_media')}}        </a>
      </li>
     
      <li class="nav-item">
        <a class="nav-link"  data-toggle="pill" href="#product_size_weight">{{trans('admin.product_size_weight')}}</a>
      </li>

    </ul>
    <div class="clearfix"></div>

    
    <div class="tab-content">
        <div class="clearfix"></div>

       @include('admin.products.tab.department')
       <div class="clearfix"></div>

       @include('admin.products.tab.product_settings')
       <div class="clearfix"></div>

       @include('admin.products.tab.product_media')
       <div class="clearfix"></div>

       @include('admin.products.tab.product_size_weight')
    
    </div>

      <div class="container-fluid" style="margin-bottom: 30px; margin-top: 30px; ">
 {{-- {!! Form::submit(trans('admin.save'),['class'=>'btn btn-sm btn-primary navbar-btn save']) !!} --}}
    <a href="#" class="btn btn-primary btn-sm navbar-btn save">
    {{trans('admin.save')}} 
     <i class="fa fa-floppy-o" aria-hidden="true"></i>
    </a> 

    <a href="#" class="btn btn-sm btn-success navbar-btn save_and_continue">
        {{trans('admin.save_and_continue')}}
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
    </a>

    <a href="#" class="btn btn-sm btn-info navbar-btn copy_product">
        {{trans('admin.copy_product')}}
    <i class="fa fa-clone" aria-hidden="true"></i>
   </a>

   <a href="#" class="btn btn-sm btn-danger navbar-btn delete">
        {{trans('admin.delete')}}
   <i class="fa fa-trash"></i>
   </a>       

   
      </div>
{!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
