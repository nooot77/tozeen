@extends('admin.index')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  {{-- 
		
		'instagram',
		'youtube',
		'snapchat',
		'twitter',

		'extra', --}}
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('websites'),'files'=>true]) !!}
    <div class="form-group">
      {!! Form::label('about_title_ar',trans('website.about_title_ar')) !!}
      {!! Form::text('about_title_ar',website()->about_title_ar,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('about_title_en',trans('website.about_title_en')) !!}
      {!! Form::text('about_title_en',website()->about_title_en,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('about_bodys_ar',trans('website.about_bodys_ar')) !!}
      {!! Form::textarea('about_bodys_ar',website()->about_bodys_ar,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('about_bodys_en',trans('website.about_bodys_en')) !!}
      {!! Form::textarea('about_bodys_en',website()->about_bodys_en,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('about_bodyb_ar',trans('website.about_bodyb_ar')) !!}
      {!! Form::textarea('about_bodyb_ar',website()->about_bodyb_ar,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
      {!! Form::label('about_bodyb_en',trans('website.about_bodyb_en')) !!}
      {!! Form::textarea('about_bodyb_en',website()->about_bodyb_en,['class'=>'form-control']) !!}
    </div>
   
        <div class="form-group">
      {!! Form::label('video_url',trans('website.video_url')) !!}
      {!! Form::text('video_url',website()->video_url,['class'=>'form-control']) !!}
    </div>
 
    <div class="form-group">
      {!! Form::label('video_thamnail',trans('website.video_thamnail')) !!}
      {!! Form::file('video_thamnail',['class'=>'form-control']) !!}

         @if(!empty(website()->video_thamnail))
       <img src="{{ Storage::url(website()->video_thamnail) }}" style="width:60px;height: 60px;" />
      @endif

    </div>

     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('number1',trans('website.number1')) !!}
      {!! Form::text('number1',website()->number1,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('number2',trans('website.number2')) !!}
      {!! Form::text('number2',website()->number2,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('number3',trans('website.number3')) !!}
      {!! Form::text('number3',website()->number3,['class'=>'form-control']) !!}
    </div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('number4',trans('website.number4')) !!}
      {!! Form::text('number4',website()->number4,['class'=>'form-control']) !!}
    </div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('number5',trans('website.number5')) !!}
      {!! Form::text('number5',website()->number5,['class'=>'form-control']) !!}
    </div>
  
   <div class="clearfix"></div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('email1',trans('website.email1')) !!}
      {!! Form::email('email1',website()->email1,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('email2',trans('website.email2')) !!}
      {!! Form::email('email2',website()->email2,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('email3',trans('website.email3')) !!}
      {!! Form::email('email3',website()->email3,['class'=>'form-control']) !!}
    </div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('email4',trans('website.email4')) !!}
      {!! Form::email('email4',website()->email4,['class'=>'form-control']) !!}
    </div>
      
   <div class="clearfix"></div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('instagram',trans('website.instagram')) !!}
      {!! Form::text('instagram',website()->instagram,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('twitter',trans('website.twitter')) !!}
      {!! Form::text('twitter',website()->twitter,['class'=>'form-control']) !!}
    </div>
     <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('snapchat',trans('website.snapchat')) !!}
      {!! Form::text('snapchat',website()->snapchat,['class'=>'form-control']) !!}
    </div>
         <div class="form-group col-md-4 col-lg-4 col-sm-4 col-xs-12">
      {!! Form::label('youtube',trans('website.youtube')) !!}
      {!! Form::text('youtube',website()->youtube,['class'=>'form-control']) !!}
    </div>
  
   <div class="clearfix"></div>
         <div class="form-group">
      {!! Form::label('location1',trans('website.location1')) !!}
      {!! Form::text('location1',website()->location1,['class'=>'form-control']) !!}
    </div>
     <div class="form-group">
      {!! Form::label('location2',trans('website.location2')) !!}
      {!! Form::text('location2',website()->location2,['class'=>'form-control']) !!}
    </div>
     <div class="form-group">
      {!! Form::label('location3',trans('website.location3')) !!}
      {!! Form::text('location3',website()->location3,['class'=>'form-control']) !!}
    </div>
         <div class="form-group">
      {!! Form::label('location4',trans('website.location4')) !!}
      {!! Form::text('location4',website()->location4,['class'=>'form-control']) !!}
    </div>
         <div class="form-group">
      {!! Form::label('location5',trans('website.location5')) !!}
      {!! Form::text('location5',website()->location5,['class'=>'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('footer_ar',trans('website.footer_ar')) !!}
      {!! Form::text('footer_ar',website()->footer_ar,['class'=>'form-control']) !!}
    </div>
       <div class="form-group">
      {!! Form::label('footer_en',trans('website.footer_en')) !!}
      {!! Form::text('footer_en',website()->footer_en,['class'=>'form-control']) !!}
    </div>
   

    {!! Form::submit(trans('website.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->
@endsection
