@extends('backEnd.layout')
@section('content')
<div class="padding">
  <div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>Helper::aurl('countries/'.$country->id),'method'=>'put','files'=>true ]) !!}
   <div class="form-group">
        {!! Form::label('title_ar',trans('admin.country_name_ar')) !!}
        {!! Form::text('title_ar',$country->title_ar,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('title_en',trans('admin.country_name_en')) !!}
        {!! Form::text('title_en',$country->title_en,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('code',trans('admin.code')) !!}
        {!! Form::text('code',$country->code,['class'=>'form-control']) !!}
     </div>

     <div class="form-group">
        {!! Form::label('tel',trans('admin.mob')) !!}
        {!! Form::text('tel',$country->tel,['class'=>'form-control']) !!}
     </div>

  



     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection