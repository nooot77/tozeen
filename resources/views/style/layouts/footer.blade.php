<div class="footer-top-area">
    <div class="zigzag-bottom"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="footer-about-us">
                    <h2>T<span>ozeen</span></h2>
                    <p>
                        المتجر الاول للشوكلاتة الفاخرة 
                    </p>
                    <div class="footer-social">
                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">لوحة المستخدم </h2>
                    <ul>
                        <li><a href="#">حسابي</a></li>
                        <li><a href="#">طلباتي السابقة</a></li>
                        {{-- <li><a href="#">Wishlist</a></li> --}}
                        <li><a href="#">الدعم الفني </a></li>
                        {{-- <li><a href="#">Front page</a></li> --}}
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer-menu">
                    <h2 class="footer-wid-title">الاقاسم</h2>
                    <ul>
                        <li><a href="#">الشوكلاتة المغلفة</a></li>
                        <li><a href="#">الشوكلاتة المكشوفة</a></li>
                        <li><a href="#">الفطائر</a></li>
                        <li><a href="#">المقليات</a></li>
                        <li><a href="#">ألصواني </a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="footer-newsletter">
                    <h2 class="footer-wid-title">العروض الحصرية والخصومات</h2>
                    <p>
                        ضع ايميلك ليصلك جديدنا وعروضنا الحضرية
                    </p>
                    <div class="newsletter-form">
                        <form action="#">
                            <input type="email" placeholder="Type your email">
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <iframe src="https://maroof.sa/Business/GetStamp?bid=84780" style=" width: auto; height: 250px;  "  frameborder="0" seamless='seamless' scrollable="no"></iframe>

    </div>
    </div> <!-- End footer top area -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="copyright">

                        <p>&copy; 2019 tozeen. All Rights Reserved. <a href="http://www.freshdesignweb.com" target="_blank">tozeen.com</a></p>
                    </div>
                    
                </div>
                <div class="col-md-4">
                    <div class="footer-card-icon">
                        <i class="fa fa-cc-discover"></i>
                        <i class="fa fa-cc-mastercard"></i>
                        <i class="fa fa-cc-paypal"></i>
                        <i class="fa fa-cc-visa"></i>
                    </div>
                </div>
            </div>
        </div>
        </div> <!-- End footer bottom area -->
        <!-- Latest jQuery form server -->
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <!-- Bootstrap JS form CDN -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- jQuery sticky menu -->
        <script src="{{ url('design/style') }}/js/owl.carousel.min.js"></script>
        <script src="{{ url('design/style') }}/js/jquery.sticky.js"></script>
        <!-- jQuery easing -->
        <script src="{{ url('design/style') }}/js/jquery.easing.1.3.min.js"></script>
        <!-- Main Script -->
        <script src="{{ url('design/style') }}/js/main.js"></script>
        <!-- Slider -->
        <script type="text/javascript" src="{{ url('design/style') }}/js/bxslider.min.js"></script>
        <script type="text/javascript" src="{{ url('design/style') }}/js/script.slider.js"></script>
    </body>
</html>