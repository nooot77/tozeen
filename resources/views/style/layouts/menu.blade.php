<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.html">الرئيسية</a></li>
                    <li><a href="shop.html">المتجر</a></li>
                    {{-- <li><a href="single-product.html">Single product</a></li> --}}
                    <li><a href="cart.html">سلة المشتريات</a></li>
                    <li><a href="checkout.html">الدفع</a></li>
                    <li><a href="#">الاقسام</a></li>
                    <li><a href="#">عن الشركة</a></li>
                    <li><a href="#">تواصل معنا</a></li>
                </ul>
            </div>
        </div>
    </div>
    </div> <!-- End mainmenu area -->