<div class="header-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="user-menu">
                    <ul>
                        <li><a href="#"><i class="fa fa-user"></i> حسابي</a></li>
                        <li><a href="#"><i class="fa fa-heart"></i> Wishlist</a></li>
                        <li><a href="cart.html"><i class="fa fa-user"></i>سلة المشتريات</a></li>
                        <li><a href="checkout.html"><i class="fa fa-user"></i> الدفع</a></li>
                        <li><a href="#"><i class="fa fa-user"></i> تسجيل الدخول</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="header-right">
                    <ul class="list-unstyled list-inline">
                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">العملة :</span><span class="value">ريال سعودي </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">ريال سعودي</a></li>
                                <li><a href="#">درهم امارتي</a></li>
                                {{-- <li><a href="#">GBP</a></li> --}}
                            </ul>
                        </li>
                        <li class="dropdown dropdown-small">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" href="#"><span class="key">اللغة :</span><span class="value">العربية </span><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"></a>اللغة العربية</a></li>
                                <li><a href="#">اللغة الانجليزية</a></li>
                                {{-- <li><a href="#">German</a></li> --}}
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div> <!-- End header area -->

    <div class="site-branding-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="logo">
                        <h1>
                            <a href="{{ url('/') }}">
                                توزين
                            {{-- <img src="{{ Storage::url(setting()->icon) }}"> --}}
                        </a>
                    </h1>
                    </div>
                </div>

                {{-- <div class="col-sm-6">
                    <div class="shopping-item">
                        <a href="cart.html">Cart - <span class="cart-amunt">$100</span> <i class="fa fa-shopping-cart"></i> <span class="product-count">5</span></a>
                    </div>
                </div> --}}
            </div>
        </div>
        </div> <!-- End site branding area -->