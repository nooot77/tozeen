@extends('shop.include.layout')


@section('content')


	

<div class="wrap">
	
	<div id="content">
		<div class="content content-about">
			<div class="banner-blog product-thumb">
				<div class="box-img">
					<a href="#" class="zoom-thumb"><img src="images/blog/blog_41_1.jpg" alt="" /></a>
				</div>
			</div>
			<div class="container">
				<div class="main-content-page">
					<div class="content-about">
						<h2 class="title30">about us</h2>
						<p class="desc">GoodShop has been a family-owned, designer furniture institution in Western Australia since 1997. Having established a solid reputation built on honesty and integrity, designFARM offer their customers a genuine experience. We believe in good, authentic design, in order to do what we do best – create meaningful spaces. You won’t find any replica’s here – we only source original furniture, which is made under license. Why? Because we want your furniture to last a lifetime, as well as its story.</p>
						<p class="blockquote">
							Lorem ipsum dolor sit amet conse ctetur adipisicing elit do eiusmod tempor. Dolor sit amet conse ctetur adipisicing elit do eiusmod tempor. Lorem ipsum dolor sit amet conse ctetur adipisicing elit do eiusmod tempor.
						</p>
						<div class="about-why-choise">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<h2 class="title18">Why choose us</h2>
									<div class="about-accordion toggle-tab">
										<div class="item-toggle-tab active">
											<div class="toggle-tab-title"><span class="bg-color"><i class="fa fa-gift"></i></span><h2>Deals & Promotions</h2></div>
											<p class="desc toggle-tab-content">Western Australia since 1997. Having established a solid reputation built on honesty and integrity, designFARM offer their customers a genuine experience. We believe in good, authentic design, in order to do what we do best – create meaningful spaces. You won’t find any replica’s here</p>
										</div>
										<div class="item-toggle-tab">
											<div class="toggle-tab-title"><span class="bg-color"><i class="fa fa-diamond"></i></span><h2>Transaction Service Agreement</h2></div>
											<p class="desc toggle-tab-content">Western Australia since 1997. Having established a solid reputation built on honesty and integrity, designFARM offer their customers a genuine experience. We believe in good, authentic design, in order to do what we do best – create meaningful spaces. You won’t find any replica’s here</p>
										</div>
										<div class="item-toggle-tab">
											<div class="toggle-tab-title"><span class="bg-color"><i class="fa fa-get-pocket"></i></span><h2>Organization & Technical Support</h2></div>
											<p class="desc toggle-tab-content">Western Australia since 1997. Having established a solid reputation built on honesty and integrity, designFARM offer their customers a genuine experience. We believe in good, authentic design, in order to do what we do best – create meaningful spaces. You won’t find any replica’s here</p>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="banner-adv overlay-image zoom-image">
										<a href="#" class="adv-thumb-link"><img src="images/pages/img-about.jpg" alt="" /></a>
									</div>
								</div>
							</div>
						</div>
						<!-- End Choise -->
						<div class="about-client">
							<h2 class="title18">What people are saying</h2>
							<div class="about-client-slider">
								<div class="wrap-item" data-autoplay="true" data-pagination="false" data-itemscustom="[[0,1],[560,2],[990,3]]">
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av1.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Vincent Vanilla</a></h3>
											<span class="silver">happy customer</span>
										</div>
									</div>
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av2.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Gregor Red</a></h3>
											<span class="silver">loyal customer</span>
										</div>
									</div>
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av3.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Alexander Green</a></h3>
											<span class="silver">happy customer</span>
										</div>
									</div>
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av4.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Vincent Vanilla</a></h3>
											<span class="silver">happy customer</span>
										</div>
									</div>
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av5.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Gregor Red</a></h3>
											<span class="silver">loyal customer</span>
										</div>
									</div>
									<div class="item-about-client">
										<div class="client-thumb"><a href="#"><img src="images/pages/av6.jpg" alt="" /></a></div>
										<div class="client-info">
											<p class="desc">Lorem enim et luctus hendrelibero mole stie ante, ut fringilla purus eros quisent ipsum. Aliquam bidum. Thank you!</p>
											<h3 class="title14"><a href="#" class="color">Alexander Green</a></h3>
											<span class="silver">happy customer</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- End Client -->
						<div class="about-service">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="item-about-service item-about-service1 text-center white">
										<a href="#" class="wobble-horizontal"><i class="fa fa-gift"></i></a>
										<h2 class="title30">Gift Card</h2>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="item-about-service item-about-service2 text-center white">
										<a href="#" class="wobble-horizontal"><i class="fa fa-home"></i></a>
										<h2 class="title30">Store</h2>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="item-about-service item-about-service3 text-center white">
										<a href="#" class="wobble-horizontal"><i class="fa fa-life-bouy"></i></a>
										<h2 class="title30">Full Support</h2>
									</div>
								</div>
							</div>
						</div>
						<div class="banner-form banner-form-about clearfix">
							<div class="logo">
								<img src="images/logo-2.png" alt="" />
							</div>
							<div class="box-text">
								<p>Enter our weekly drawing for a $100 gift card and exclusive access to releases, events & more!</p>
							</div>
							<div class="box-letter">
								<form action="mailto:support@kutetheme.com" method="post" enctype="text/plain">
									<div class="txt-mail" >
										<input type="text" name="mail">
									</div>
									<div class="btn-submit">
										<input type="submit" value="">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content -->
	
</div>



@endsection

