@extends('shop.include.layout')


@section('content')


	
<div class="preload">
<div class="wrap">
	
	<div id="content">
		<div class="content-page woocommerce">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="title-shop-page dark font-bold dosis-font">My Profile</h2>
						<div class="my-profile-box register-content-box">
							<div class="form-my-account">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<p>
											<label>First name <span class="required">*</span></label>
											<input type="text" name="firstname" />
										</p>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<p>
											<label>Last name <span class="required">*</span></label>
											<input type="text" name="lastname" />
										</p>
									</div>
								</div>
								<p>
									<label>Email address <span class="required">*</span></label>
									<input type="text" name="email" />
								</p>
								<h3 class="title18 title-change-pasword">Password change</h3>
								<p>
									<label>Current password (leave blank to leave unchanged)</label>
									<input type="text" name="current-password" />
								</p>
								<p>
									<label>New password (leave blank to leave unchanged)</label>
									<input type="text" name="new-password" />
								</p>
								<p>
									<label>Confirm new password</label>
									<input type="text" name="confirm-password" />
								</p>
								<p>
									<input type="submit" class="register-button save-change" name="register" value="Save Changes">
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content Page -->
	</div>
	<!-- End Content -->
	
	<a href="#" class="scroll-top dark"><i class="fa fa-angle-double-up"></i></a>
</div>
</div>




@endsection

