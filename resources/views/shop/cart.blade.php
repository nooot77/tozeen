@extends('shop.include.layout')

@push('js')

<script>
	
	// console.log($(".qty-val").text());
</script>

<script>
	$(window).ready(function() {

		$('.cart_update').click(function() {

			window.location.href = "{{route('shop.cart.index')}}";
			return false;
		});


$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });



$('.getVal').click(function() {
	var $cartId = $(this).siblings('span').data('id');
	var $cartQty = $(this).siblings('span').text();
console.log($cartId);
console.log($cartQty);

	$.ajax({  
        url: "{{route('shop.cart.update')}}",
        type: 'GET',
        dataType: 'json',
        data: {cart_id: $cartId, cart_qty: $cartQty},

        success: function(data) {

       console.log('success');
       


        
      },error(response){

        console.log(response);
      }
	
		});


});


});
</script>
@endpush


@section('content')



	<div class="preload">
		

<div class="wrap">

	<div id="content">
		<div class="content-page woocommerce"> 
			<div class="container">
				<div class="content-about cart-content-page">
					<h2 class="title30 text-uppercase font-bold dark">@lang('shop.CART')</h2>
					<form method="post">
						<div class="table-responsive">
							<table class="shop_table cart table">
								<thead>
									<tr>
										<th class="product-remove">&nbsp;</th>
										<th class="product-thumbnail">&nbsp;</th>
										<th class="product-name">@lang('shop.PRODUCT')</th>
										<th class="product-price">@lang('shop.price')</th>
										<th class="product-quantity">@lang('shop.QUANTITY')</th>
										<th class="product-subtotal">@lang('shop.TOTAL')</th>
									</tr>
								</thead>
								<tbody>
									

								{{------ End Feach cart Data ------}}

									 @if(\Auth::user()->cart->count() > 0) 
									 {{-- {{dd(\Auth::user()->cart[1]->product)}} --}}

									@foreach(\Auth::user()->cart as $cart)

											
									
										<tr class="cart_item">

									

										<td class="product-remove">
											<a class="remove" href="{{route('shop.cart.delete' , ['id' => $cart->id])}}"><i class="fa fa-trash"></i></a>
										</td>
										<td class="product-thumbnail">

											@if($cart->product->files->count() > 0)
									<a href="#"><img width='100%' height='100%' src='{{Storage::url($product->files->first()->full_file)}}'>  </a>
									@else
									<a href="#"><img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'> </a>

									@endif





															
										</td>
										<td class="product-name" data-title="Product">
											<a href="{{route('shop.product.show', ['id' => $cart->product->id])}}">{{$cart->product->title}}</a>					
										</td>
										<td class="product-price" data-title="Price">
											<span class="amount">${{$cart->product->price}}</span>					
										</td>
										<td class="product-quantity" data-title="Quantity">
											
											<div class="detail-qty-cart border">
												<a href="#" class="getVal qty-down silver"><i class="fa fa-minus-square"></i></a>
												<span data-id='{{$cart->id}}' class="qty-val">{{$cart->quantity}}</span>
												<a href="#" class="getVal qty-up silver"><i class="fa fa-plus-square"></i></a>
											</div>
										</td>
										<td class="product-subtotal" data-title="Total">
											<span class="amount">${{$cart->product->price * $cart->quantity}}</span>					
										</td>
									</tr>
									
									



										@endforeach

									@else
										<h3 class="text-center bg-success text-dark"> you don't have any thing in this cart</h3>

										@endif

										{{------ End Feach cart Data ------}}






									
									<tr>
										<td class="actions" colspan="6">
											<div class="coupon">
												<label for="coupon_code">Coupon:</label> 
												<input type="text" placeholder="Coupon code" value="" id="coupon_code" class="input-text" name="coupon_code"> 
												<input type="submit" value="Apply Coupon" name="apply_coupon" class="button bg-color">
											</div>
											

												
											<input type="submit" value="@lang('shop.update_cart')" name="update_cart" class="cart_update pull-right button bg-color">			
											
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
					<div class="cart-collaterals">
						<div class="cart_totals ">
							<h2>@lang('shop.CART_TOTALS')</h2>
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr class="cart-subtotal">
											<th>@lang('shop.Subtotal')</th>
											<td><strong class="amount">
												@if(\Auth::user()->cart->count() > 0)

														 @php
														$newCount = 0;
														foreach(\Auth::user()->cart as $cart){
															$newCount += $cart->product->price * $cart->quantity;

														}
														@endphp
													$ {{ $newCount}}
													

												@else

												$0

												@endif
													
 
											</strong></td>
										</tr>
										<tr class="shipping">
											<th>@lang('shop.Shipping')</th>
											<td>
												<ul class="list-none" id="shipping_method">
													<li>
														<input type="radio" class="shipping_method" checked="checked" value="free_shipping" id="shipping_method_0_free_shipping" data-index="0" name="shipping_method[0]">
														<label for="shipping_method_0_free_shipping">Free Shipping</label>
													</li>
													<li>
														<input type="radio" class="shipping_method" value="local_delivery" id="shipping_method_0_local_delivery" data-index="0" name="shipping_method[0]">
														<label for="shipping_method_0_local_delivery">Local Delivery (Free)</label>
													</li>
													<li>
														<input type="radio" class="shipping_method" value="local_pickup" id="shipping_method_0_local_pickup" data-index="0" name="shipping_method[0]">
														<label for="shipping_method_0_local_pickup">Local Pickup (Free)</label>
													</li>
												</ul>
											</td>
										</tr>
										<tr class="order-total">
											<th>@lang('shop.TOTAL')</th>
											<td><strong><span class="amount">$106.00</span></strong> </td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="wc-proceed-to-checkout">
								<a class="checkout-button button alt wc-forward bg-color" href="#">@lang('shop.PROCEED_TO_CHECKOUT')</a>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- End Content Pages -->
	</div>
	<!-- End Content -->
	
	<a href="#" class="scroll-top dark"><i class="fa fa-angle-double-up"></i></a>
</div>
	</div>

	



@endsection

