@extends('shop.include.layout')


@section('content')


<div class="wrap">
	<div id="content">
		<div class="container">
			<div class="product-compare-lightbox">
				<div class="table-responsive ">
				<p></p>
					<h2 class="title18 text-uppercase text-center font-bold compare-title">Compare products</h2>
					<h3></h3>
					<table class="table table-bordered compare-product-table">
						<tr class="remove-compare text-center">
							<th></th>
							<td><a href="#" class="title18 color"><i class="fa fa-trash"></i></a></td>
							<td><a href="#" class="title18 color"><i class="fa fa-trash"></i></a></td>
							<td><a href="#" class="title18 color"><i class="fa fa-trash"></i></a></td>
						</tr>
						<tr class="compare-img">
							<th>Image</th>
							<td><a href="#"><img src="images/products/tech/tech-03.jpg" alt=""></a></td>
							<td><a href="#"><img src="images/products/tech/tech-08.jpg" alt=""></a></td>
							<td><a href="#"><img src="images/products/tech/tech-06.jpg" alt=""></a></td>
						</tr>
						<tr class="compare-product-title">
							<th>Title</th>
							<td><a href="#">Smartbuy Product 01</a></td>
							<td><a href="#">Smartbuy Product 02</a></td>
							<td><a href="#">Smartbuy Product 03</a></td>
						</tr>
						<tr class="compare-price">
							<th>Price</th>
							<td>
								<div class="product-price">
									<del class="silver">$718.00</del>
									<ins class="color title18 font-bold">$359.00</ins>
								</div>
							</td>
							<td>
								<div class="product-price">
									<del class="silver">$718.00</del>
									<ins class="color title18 font-bold">$359.00</ins>
								</div>
							</td>
							<td>
								<div class="product-price">
									<del class="silver">$718.00</del>
									<ins class="color title18 font-bold">$359.00</ins>
								</div>
							</td>
						</tr>
						<tr class="compare-color">
							<th>Color</th>
							<td>Red, Black, Blue</td>
							<td>Red, Black, Blue</td>
							<td>Red, Black, Blue</td>
						</tr>
						<tr class="compare-size">
							<th>Size</th>
							<td>M</td>
							<td>M</td>
							<td>M</td>
						</tr>
						<tr class="compare-des">
							<th>Description</th>
							<td>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla paria tur.</td>
							<td>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla paria tur.</td>
							<td>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla paria tur.</td>
						</tr>
						<tr class="compare-atc text-center">
							<th></th>
							<td><a href="#" class="shop-button bg-color">Add to cart</a></td>
							<td><a href="#" class="shop-button bg-color">Add to cart</a></td>
							<td><a href="#" class="shop-button bg-color">Add to cart</a></td>
						</tr>
					</table>
				</div>
			</div>	
		</div>	
	</div>
	<!-- End Content -->
</div>

@endsection
