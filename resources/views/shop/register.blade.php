@extends('shop.include.layout')


@section('content')


	
<div class="preload">
<div class="wrap">

	<div id="content">
		
		<div class="content-page woocommerce">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="title-shop-page dark font-bold dosis-font">@lang('shop.MEMBER')</h2>
						<div class="register-content-box">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-ms-12">
									<div class="check-billing">
										<div class="form-my-account">
											<form class="block-login" method="POST" action="{{route('shop.user.register')}}">
												@csrf

												<h2 class="title24 title-form-account">@lang('shop.register')</h2>


												<p>
													<label>@lang('shop.full_name')<span class="required">*</span></label>
													<input type="text" name="name" class=" {{ $errors->has('name') ? ' is-invalid' : '' }} "/>

													 @if ($errors->has('name'))
												<span class="invalid-feedback" role="alert">

													<strong>{{ $errors->first('name') }}</strong>
												</span>
												@endif 


												</p>





												<p>

												<label>@lang('shop.Email')
													<span class="required">*</span></label>
													<input type="text" name="email" class=" {{ $errors->has('email') ? ' is-invalid' : '' }} "/>

													 @if ($errors->has('email'))
												<span class="invalid-feedback" role="alert">

													<strong>{{ $errors->first('email') }}</strong>
												</span>
												@endif 
												</p>




												<p>

												<label> @lang('shop.Password') 

													<span class="required">*</span>

												</label>
												<input type="text" name="password" class=" {{ $errors->has('password') ? ' is-invalid' : '' }} " />

												 @if ($errors->has('password'))
												<span class="invalid-feedback" role="alert">

													<strong>{{ $errors->first('password') }}</strong>
												</span>
												@endif 
												</p>
												<p>
													<input type="submit" class="register-button" name="register" value="@lang('shop.register')">
												</p>
												
												<h2 class="title18 social-login-title">@lang('shop.OR_REGISTER_WITH')</h2>
												<div class="social-login-block table-custom text-center">
													<div class="social-login-btn">
														<a href="#" class="login-fb-link">@lang('shop.facebook')</a>
													</div>
													<div class="social-login-btn">
														<a href="#" class="login-goo-link">@lang('shop.google')</a>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-ms-12">
									<div class="check-address">
										<div class="form-my-account check-register text-center">
											<h2 class="title24 title-form-account">@lang('shop.register')</h2>
											<p class="desc">@lang('shop.for_register')</p>
											<a href="#" class="shop-button bg-color login-to-register" data-login="Login" data-register="Register">@lang('shop.register')</a>
											<p class="desc title12 silver"><i>@lang('shop.change_to_register')</i></p>
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content Page -->
	</div>
	<!-- End Content -->
	
	<a href="#" class="scroll-top dark"><i class="fa fa-angle-double-up"></i></a>
</div>

</div>




@endsection

