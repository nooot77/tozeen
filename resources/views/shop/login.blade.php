@extends('shop.include.layout')


@section('content')



<div class="preload">
<div class="wrap">

<div id="content">
	<div class="content-page woocommerce">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="title-shop-page dark font-bold dosis-font">@lang('shop.MEMBER')</h2>
					<div class="register-content-box">
						<div class="row">
							<div class="col-md-6 col-sm-6 col-ms-12">
								<div class="check-billing">
									<div class="form-my-account">
										<form method="POST" action="{{route('shop.user.login')}}" class="block-login">
											@csrf
											<h2 class="title24 title-form-account">@lang('shop.login')</h2>
											<p>
												<label>@lang('shop.Email') <span class="required">*</span></label>
												<input type="text" name="email" />
											</p>
											<p>
												<label>@lang('shop.Password') <span class="required">*</span></label>
												<input type="text" name="password" />
											</p>
											<p>
												<input type="submit" class="register-button" name="login" value="Login">
											</p>
											<div class="table-custom create-account">
												<div class="text-left">
													<p>
														<input name="remember" type="checkbox"  id="remember" /> <label for="remember">@lang('shop.remember')</label>
													</p>
												</div>
												<div class="text-right">
													<a href="#" class="color">@lang('shop.lost_password')</a>
												</div>
											</div>
											<h2 class="title18 social-login-title">@lang('shop.OR_REGISTER_WITH')</h2>
											<div class="social-login-block table-custom text-center">
												<div class="social-login-btn">
													<a href="{{route('social.oauth', ['driver' => 'facebook'])}}" class="login-fb-link">@lang('shop.facebook')</a>
												</div>
												<div class="social-login-btn">
													<a href="#" class="login-goo-link">@lang('shop.google')</a>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6 col-ms-12">
								<div class="check-address">
									<div class="form-my-account check-register text-center">
										<h2 class="title24 title-form-account">Register</h2>
										<p class="desc">Registering for this site allows you to access your order status and history. Just fill in the fields below, and we’ll get a new account set up for you in no time. We will only ask you for information necessary to make the purchase process faster and easier.</p>
										<a href="{{route('shop.user.register')}}" class="shop-button bg-color login-to-register" data-login="Login" data-register="Register">@lang('shop.register')</a>
										<p class="desc title12 silver"><i>Click to switch Register/Login</i></p>
									</div>
								</div>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content Page -->
</div>
<!-- End Content -->

<a href="#" class="scroll-top dark"><i class="fa fa-angle-double-up"></i></a>
</div>

</div>




@endsection

