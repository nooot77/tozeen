@extends('shop.include.layout')


@section('content')

@push('js')
<script>
	$(window).ready(function() {


$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });


$('.cart').click(function() {
	var $productId = $(this).data('product');


	$.ajax({  
        url: "{{route('shop.cart.store')}}",
        type: 'GET',
        dataType: 'json',
        data: {product_id: $productId},

        success: function(data) {

       console.log('success');
       console.log(data.count);
       $('.mini-cart-sum').text(data.count);
       $('.mini-cart-content h2').text('('+data.count+') {{ trans('shop.items_in_my_cart')}} ');
        //$('.putResult').html(data.result);


        
      },error(response){

        console.log(response);
      }
	
		});


});


});
</script>

@endpush

<div class="wrap">  
	<div id="content"> 
		<div class="content">
	@if(\Auth::check())
	<h1>
		{{\Auth::user()->name}}</h1>	
		@endif


	 <!-- <div class="box-slider"> <div class="wrap-item"
	data-navigation="true" data-pagination="true" data-itemscustom="[[0,1]]"
	data-autoplay="false" data-transition="fade"> <div class="wrap-slide"> <div
	class="box-img"> <a href="#"><img src="images/slide/slide-1.jpg" alt=""
	/></a> </div> <div class="box-info-slide"> <div class="container"> <div
	class="adv-info-slide"> <h2 class="animated"
	data-animated="fadeInLeftBig">good shop</h2> <h3 class="animated"
	data-animated="flipInX">up to 50% off</h3> <a href="#" class="animated
	has-bottom" data-animated="bounceInLeft">shop now</a> </div> </div> </div>
	</div> <div class="wrap-slide"> <div class="box-img box-img-2"> <a
	href="#"><img src="images/slide/slide-2.jpg" alt="" /></a> </div> <div
	class="box-info-slide"> <div class="container"> <div class="adv-info-slide">
	<h2 class="animated" data-animated="fadeInLeftBig">supper shop</h2> <h3
	class="animated" data-animated="flipInX">out top trend</h3> <a href="#"
	class="animated has-bottom" data-animated="bounceInLeft">shop now</a> </div>
	</div> </div> </div> <div class="wrap-slide"> <div class="box-img box-img-3">
	<a href="#"><img src="images/slide/slide-3.jpg" alt="" /></a> </div> <div
	class="box-info-slide"> <div class="container"> <div class="adv-info-slide">
	<h2 class="animated" data-animated="fadeInLeftBig">alo shop</h2> <h3
	class="animated" data-animated="flipInX">out top seller</h3> <a href="#"
	class="animated has-bottom" data-animated="bounceInLeft">shop now</a> </div>
	</div> </div> </div> </div> </div>  -->


	<div class="container"> <div
	class="box-product box-has-filter"> <div class="title"> <div class="row">
	<div class="col-lg-6 col-md-6 col-sm-8 col-xs-9"> <div class="box-tabs"> <ul
	class="nav nav-tabs list-inline" role="tablist"> 

	<li role="presentation"
	class="active">

	<h3>
		<a data-toggle="tab" href="#tab1">

			@lang('shop.allProdcuts')
		</a>
	</h3>

</li> 

									 </ul> </div>
	</div> <div class="col-lg-6 col-md-6 col-sm-4 col-xs-3"> <a href="#"
	class="filter"><i class="fa fa-bars" aria-hidden="true"></i> @lang('shop.filter')</a>
	</div> </div> <div class="dropdown-box"> <div class="row"> <div
	class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <div class="drop-list
	widget-cate"> <div class="title"> <h3>


	@lang('shop.categories')

</h3> </div> <div
	class="widget-content"> <div class="list-product"> <ul class="list-inline">
	<li> <div class="title clearfix"> <div class="box-img"> <img
	src="images/products/garden/garden-01-2.jpg" alt="" /> </div> <div
	class="text"><h3>Garden</h3></div> <i class="fa fa-angle-down"
	aria-hidden="true"></i> <i class="fa fa-angle-up" aria-hidden="true"></i>
	</div> <ul class="list-inline"> <li><a href="#">Patio, Lawn & Garden</a></li>
	<li><a href="#">Fine Art</a></li> <li><a href="#">Arts, Crafts &
	Sewing</a></li> <li><a href="#">Birdhouse, Birdbaths </a></li> <li><a
	href="#">Garden Accents</a></li> <li><a href="#">Garden Books</a></li> <li><a
	href="#">Heirloom Seeds</a></li> <li><a href="#">Lanterns</a></li> <li><a
	href="#">Planters, Hangers and Stands</a></li> </ul> </li> <li> <div
	class="title clearfix"> <div class="box-img"> <img
	src="images/products/home/home-11-2.jpg" alt="" /> </div> <div
	class="text"><h3>Home</h3></div> <i class="fa fa-angle-down"
	aria-hidden="true"></i> <i class="fa fa-angle-up" aria-hidden="true"></i>
	</div> <ul class="list-inline"> <li><a href="#">Chair</a></li> <li><a
	href="#">Lamp</a></li> <li><a href="#">Bottle</a></li> </ul> </li> <li> <div
	class="title clearfix"> <div class="box-img"> <img
	src="images/products/tech/tech-09-1.jpg" alt="" /> </div> <div
	class="text"><h3>Tech</h3></div> <i class="fa fa-angle-down"
	aria-hidden="true"></i> <i class="fa fa-angle-up" aria-hidden="true"></i>
	</div> <ul class="list-inline"> <li><a href="#">Phone</a></li> <li><a
	href="#">Superheater</a></li> <li><a href="#">Headphone</a></li> </ul> </li>
	</ul> </div> </div> </div> </div>



		{{------------ Price ------------}}


	 <div class="col-lg-3 col-md-3 col-sm-3
	col-xs-12"> <div class="drop-list widget-price"> <div class="title">
	<h3>@lang('shop.price')</h3> </div> <div class="box-range list-table clearfix"> <div
	id="amount"></div> <div id="slider-range"></div> <ul class="clearfix
	list-inline"> 

	<li><span>${{App\model\Product::min('price')}}</span></li> 

	<li><span>${{App\model\Product::max('price')}}</span></li> 
	
	</ul>
	</div> </div> </div> 

		{{------------ End Price ------------}}



		{{------------ weight ------------}}


	 <div class="col-lg-3 col-md-3 col-sm-3
	col-xs-12"> <div class="drop-list widget-price"> <div class="title">
	<h3>@lang('shop.weight')</h3> </div> <div class="box-range list-table clearfix"> <div
	id="amount"></div> <div id="slider-range"></div> <ul class="clearfix
	list-inline"> 

	<li><span>${{App\model\Product::min('weight')}}</span></li> 

	<li><span>${{App\model\Product::max('weight')}}</span></li> 
	
	</ul>
	</div> </div> </div> 
		{{------------ End weight ------------}}






	

	{{------------ fill ------------}}

	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> <div
	class="drop-list widget-size"> <div class="title"> <h3>@lang('shop.fill')</h3> </div> <ul
	class="list-inline">
	@foreach($fills as $fill)

	 <li><a href="#">{{Helper::get_lang($fill->name_en,$fill->name_ar)}}</a></li>
	@endforeach
	 </ul> </div> </div>

	{{------------ end fill ------------}}
	</div> </div>  </div> 



	<div class="tab-content">
	 <div id="tab1"
	role="tabpanel" class="tab-pane fade in active">

	 <div class="wrap-product">

	<div class="row"> 




{{------------ All Products ------------}}
	@foreach($products as $product)
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
		 <div
	class="item"> 

	<div class="product-extra-link"> 

		<a href="quickview.html"
	class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye"
	aria-hidden="true"></i><span>@lang('shop.quickView')</span></a>

	 <a href="#"
	class="box-hidden wishlist">
	<i class="fa fa-heart"
	aria-hidden="true"></i>
	<span>@lang('shop.add_to_Wishlist')</span>
</a> </div> 


<div class="thumb-product">

							@if($product->files->count() > 0)
									<a href="{{route('shop.product.show', ['id' => $product->id])}}"><img width='100%' height='100%' src='{{Storage::url($product->files->first()->full_file)}}'>  </a>
									@else
									<a href="{{route('shop.product.show', ['id' => $product->id])}}"><img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'> </a>

									@endif
						</div>








	 <div
	class="name-product"> <h3><a href="{{route('shop.product.show', ['id' => $product->id])}}">{{$product->title}}</a></h3> </div>
	<div class="box-cart"> <a href="#" onclikc="return false" data-product='{{$product->id}}' class="cart">@lang('shop.add-to-cart') <i class="fa
	fa-opencart" aria-hidden="true"></i></a>
	@php
	$productPrice = explode('.',$product->price, 2);
	@endphp

	 <ins
	class="price"><sup>$</sup>{{$productPrice[0]}}<sup>.{{$productPrice[1]}}</sup></ins> </div> <a
	href="customize.html" class="customize various"
	data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i>
	customize</a> </div> </div> 

	@endforeach
{{------------ End All Products ------------}}

	 </div> </div> </div> </div>




	





	<div class="more"> <a
	href="#"><i class="fa fa-long-arrow-down" aria-hidden="true"></i>@lang('shop.more')</a>
	</div> </div> </div>





















	 <div class="box-services"> <div class="box-service">
	<div class="container"> <div class="row"> 



		<div class="col-lg-3 col-md-6
	col-sm-6 col-xs-12"> <div class="services product clearfix"> <div
	class="wrap-box" data-color="rgba(255,99,99,0.98)"> <div
	class="icon-services"><i class="fa fa-bullhorn" aria-hidden="true"></i></div>
	<div class="text"> <h3><a href="#">@lang('shop.products') <i class="fa fa-long-arrow-right"
	aria-hidden="true"></i></a></h3> <p>@lang('shop.pro_desc')</p> </div>
	</div> </div> </div> 




	<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> <div
	class="services tag clearfix"> <div class="wrap-box"
	data-color="rgba(99,176,255,0.98)"> <div class="icon-services"><i class="fa
	fa-tags" aria-hidden="true"></i></div> <div class="text"> <h3><a
	href="#">#@lang('shop.tags') <i class="fa fa-long-arrow-right"
	aria-hidden="true"></i></a></h3> <p>@lang('shop.tag_desc')</p> </div>
	</div> </div> </div> <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"> <div
	class="services shipping clearfix"> <div class="wrap-box"
	data-color="rgba(73,213,96,0.98)"> <div class="icon-services"><i class="fa
	fa-bullhorn" aria-hidden="true"></i></div> <div class="text"> <h3><a
	href="#">@lang('shop.shipping') <i class="fa fa-long-arrow-right"
	aria-hidden="true"></i></a></h3> <p>@lang('shop.shiping_desc')</p>
	</div> </div> </div> </div> <div class="col-lg-3 col-md-6 col-sm-6
	col-xs-12"> <div class="services call clearfix"> <div class="wrap-box"
	data-color="rgba(255,203,102,0.98)"> <div class="icon-services"><i class="fa
	fa-volume-control-phone" aria-hidden="true"></i></div> <div class="text">
	<h3><a href="#">@lang('shop.callNow') <i class="fa fa-long-arrow-right"
	aria-hidden="true"></i></a></h3> <p>01678 311 160 <span>Call 24/7</span></p>
	</div> </div> </div> </div> </div> </div> </div> </div> <div
	class="box-blog"> <div class="container"> <div class="row"> <div
	class="title"> <h2>@lang('shop.the_blog')</h2> </div> <div class="wrap-item"
	data-navigation="true" data-pagination="false"
	data-itemscustom="[[0,1],[320,1],[480,1],[768,2],[992,2],[1200,2]]"
	data-autoplay="false"> <div class="wrap-blog"> <div class="box-img"> <a
	href="#"><img src="images/blog/blog_01.jpg" alt="" /></a> </div> <div
	class="info-box"> <h3><a href="#">9 Home Appliances to make your life a lot
	easier</a></h3> <a href="#" class="has-bottom">@lang('shop.readMore')</a> <ul
	class="list-inline"> <li>Post by: <a href="#">Admin Fanbong</a></li> <li><a
	href="#">5 Comments</a></li> </ul> </div> </div> <div class="wrap-blog"> <div
	class="box-img"> <a href="#"><img src="images/blog/blog_05.jpg" alt="" /></a>
	</div> <div class="info-box"> <h3><a href="#">Scandinavian Design Interior
	Products for Your Home</a></h3> <a href="#" class="has-bottom">read more</a>
	<ul class="list-inline"> <li>Post by: <a href="#">Admin Fanbong</a></li>
	<li><a href="#">5 Comments</a></li> </ul> </div> </div> <div
	class="wrap-blog"> <div class="box-img"> <a href="#"><img
	src="images/blog/blog_09.jpg" alt="" /></a> </div> <div class="info-box">
	<h3><a href="#">9 Home Appliances to make your life a lot easier</a></h3> <a
	href="#" class="has-bottom">@lang('shop.readMore')</a> <ul class="list-inline"> <li>Post
	by: <a href="#">Admin Fanbong</a></li> <li><a href="#">5 Comments</a></li>
	</ul> </div> </div> </div> </div> </div> </div> </div> </div> <!-- End
	Content --> </div>
	




@endsection

