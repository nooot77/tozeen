@extends('shop.include.layout')


@section('content')



<div class="wrap">
	
	
	<div id="content">
		<div class="content">
			<div class="container">
				<div class="main-content-gridview">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-3">
							<div class="box-left">
								<div class="list-product">
									<ul class="list-inline">
										<li class="active">
											<div class="title clearfix">
												<div class="box-img">
													<img src="images/products/garden/garden-01-2.jpg" alt="" />
												</div>
												<div class="text"><h3>Garden</h3></div>
												<i class="fa fa-angle-down" aria-hidden="true"></i>
												<i class="fa fa-angle-up" aria-hidden="true"></i>
											</div>
											<ul class="list-inline">
												<li><a href="#">Patio, Lawn & Garden</a></li>
												<li><a href="#">Fine Art</a></li>
												<li><a href="#">Arts, Crafts & Sewing</a></li>
												<li><a href="#">Birdhouse, Birdbaths </a></li>
												<li><a href="#">Garden Accents</a></li>
												<li><a href="#">Garden Books</a></li>
												<li><a href="#">Heirloom Seeds</a></li>
												<li><a href="#">Lanterns</a></li>
												<li><a href="#">Planters, Hangers and Stands</a></li>
											</ul>
										</li>
										<li>
											<div class="title clearfix">
												<div class="box-img">
													<img src="images/products/home/home-11-2.jpg" alt="" />
												</div>
												<div class="text"><h3>Home</h3></div>
												<i class="fa fa-angle-down" aria-hidden="true"></i>
												<i class="fa fa-angle-up" aria-hidden="true"></i>
											</div>
											<ul class="list-inline">
												<li><a href="#">Chair</a></li>
												<li><a href="#">Lamp</a></li>
												<li><a href="#">Bottle</a></li>
											</ul>
										</li>
										<li>
											<div class="title clearfix">
												<div class="box-img">
													<img src="images/products/tech/tech-09-1.jpg" alt="" />
												</div>
												<div class="text"><h3>Tech</h3></div>
												<i class="fa fa-angle-down" aria-hidden="true"></i>
												<i class="fa fa-angle-up" aria-hidden="true"></i>
											</div>
											<ul class="list-inline">
												<li><a href="#">Phone</a></li>
												<li><a href="#">Superheater</a></li>
												<li><a href="#">Headphone</a></li>
											</ul>
										</li>
									</ul>
								</div>
								<div class="box-range list-table clearfix">
									<div class="title">
										<h3>Price</h3>
									</div>
									<div id="amount"></div>
									<div id="slider-range"></div>
									<ul class="clearfix list-inline">
										<li><span>$5.00</span></li>
										<li><span>$500.00</span></li>
									</ul>
									<a href="#" class="btn-filter">Filter</a>
								</div>
								<div class="box-color list-table">
									<div class="title">
										<h3>Color</h3>
									</div>
									<ul class="list-inline">
										<li><a href="#" class="cyan"><i class="fa fa-check" aria-hidden="true"></i>Cyan</a></li>
										<li><a href="#" class="blue"><i class="fa fa-check" aria-hidden="true"></i>Blue</a></li>
										<li><a href="#" class="pink"><i class="fa fa-check" aria-hidden="true"></i>Pink</a></li>
										<li><a href="#" class="blush"><i class="fa fa-check" aria-hidden="true"></i>Blush</a></li>
										<li><a href="#" class="eggplant"><i class="fa fa-check" aria-hidden="true"></i>Eggplant</a></li>
										<li><a href="#" class="grey"><i class="fa fa-check" aria-hidden="true"></i>Grey</a></li>
										<li><a href="#" class="olive"><i class="fa fa-check" aria-hidden="true"></i>Olive</a></li>
										<li><a href="#" class="White"><i class="fa fa-check" aria-hidden="true"></i>White</a></li>
										<li><a href="#" class="organ"><i class="fa fa-check" aria-hidden="true"></i>Organ</a></li>
										<li><a href="#" class="black"><i class="fa fa-check" aria-hidden="true"></i>Black</a></li>
									</ul>
								</div>
								<div class="box-review list-table">
									<div class="title">
										<h3>Customer Review</h3>
									</div>
									<div class="rating-box">
										<ul class="list-inline">
											<li class="rate-4">
												<div class="product-rating">													
													<div class="inner-rating"></div>
												</div>
												<span>& Up</span>
											</li>
											<li class="rate-3">
												<div class="product-rating">													
													<div class="inner-rating"></div>
												</div>
												<span>& Up</span>
											</li>
											<li class="rate-2">
												<div class="product-rating">													
													<div class="inner-rating"></div>
												</div>
												<span>& Up</span>
											</li>
											<li class="rate-1">
												<div class="product-rating">													
													<div class="inner-rating"></div>
												</div>
												<span>& Up</span>
											</li>
										</ul>
									</div>
								</div>
								<div class="box-discount list-table">
									<div class="title">
										<h3>Discount</h3>
									</div>
									<ul class="list-inline">
										<li><a href="#">10% Off or More</a></li>
										<li><a href="#">25% Off or More</a></li>
										<li><a href="#">50% Off or More</a></li>
										<li><a href="#">70% Off or More</a></li>
									</ul>
								</div>
								<div class="banner-sidebar gray-image">
									<div class="box-img">
										<a href="#" class="adv-thumb-link"><img src="images/blog/blog_42.jpg" alt="" /></a>
									</div>
									<div class="box-info">
										<span class="upto">up to</span>
										<h3>40% off</h3>
										<span>Safavieh California</span>
										<span>Shag Collection</span>
										<a href="#" class="has-bottom">shop now</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-md-9 col-sm-9">
							<div class="box-right">
								<div class="banner-blog line-scale">
									<div class="box-img">
										<a href="#" class="adv-thumb-link"><img src="images/blog/blog_41_1.jpg" alt="" /></a>
									</div>
									<div class="box-info">
										<h3>Cactus Toothpick Holder</h3>
										<div class="price"><span>$40.90</span></div>
										<a href="#">shop now</a>
									</div>
								</div>
								<div class="wrap-gridview">
									<div class="title clearfix">
										<h3>Appliances</h3>
										<div class="box-view">
											<div class="sortby">
												<span>Sort by:</span>
												<select>
													<option value="">Position</option>
													<option value="">Name</option>
													<option value="">Name</option>
												</select>
											</div>
											<div class="view-way">
												<a href="gridview.html" class="active"><i class="fa fa-th" aria-hidden="true"></i></a>
												<a href="listview.html"><i class="fa fa-th-list" aria-hidden="true"></i></a>
											</div>
										</div>
									</div>
									<div class="wrap-product">
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/garden/garden-01.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-01.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-20.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-22.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-06.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/garden/garden-13.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-04.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-08.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-05.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-11.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/garden/garden-17.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-02.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/home/home-10.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
												<div class="item">
													<div class="product-extra-link">
														<a href="quickview.html" class="quick-view various" data-fancybox-type="iframe"><i class="fa fa-eye" aria-hidden="true"></i><span>Quick View</span></a>
														<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>Add To Wishlist</span></a>
													</div>
													<div class="thumb-product">
														<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
													</div>
													<div class="name-product">
														<h3><a href="#">Soma Glass Water Bottle</a></h3>
													</div>
													<div class="box-cart">
														<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
														<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
													</div>
													<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
												</div>
											</div>
										</div>
									</div>
									<div class="number-page">
										<ul class="list-inline">
											<li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
											<li><a href="#" class="active">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
	<!-- End Content -->
	
</div>



	



@endsection

