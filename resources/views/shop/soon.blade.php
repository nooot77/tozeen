

<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>Home Main 2</title>
	
	@include('shop.include.head')


</head>

<body>



	

<div class="preload">
<div class="wrap">
	<div id="content">
		<div class="page-coming-soon banner-background" data-image="images/pages/coming.jpg">
			<div class="content-coming-soon white text-center">
				<h2 class="title60 rale-font- font-bold">coming-soon!</h2>
				<p class="desc white title18">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibon euismod tincidunt ut. Nam vehicula commodo pulvinar.</p>
				<div class="coming-countdown time-countdown" data-date="12/12/2019" data-text='["Day","Hou","Min","Sec"]' data-color="#ffcb66" data-bg="#e5e5e5" data-width="0.02"></div>
				<form class="form-coming">
					<input type="text" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Enter You E-mail">
					<input type="submit" value="Notify Me" />
				</form>
				<div class="coming-social">
					<a href="#" class="inline-block smoke"><i class="fa fa-facebook"></i></a>
					<a href="#" class="inline-block smoke"><i class="fa fa-twitter"></i></a>
					<a href="#" class="inline-block smoke"><i class="fa fa-linkedin"></i></a>
					<a href="#" class="inline-block smoke"><i class="fa fa-pinterest-p"></i></a>
					<a href="#" class="inline-block smoke"><i class="fa fa-google-plus"></i></a>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content -->
	<a href="#" class="scroll-top dark"><i class="icon ion-android-arrow-up"></i></a>
</div>
</div>



@include('shop.include.foot')
</body>
</html>

