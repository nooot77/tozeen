@extends('shop.include.layout')


@section('content')


	

<div class="bg-gray">
<div class="wrap">
	
	<div id="content">
		<div class="content">
			<div class="container">
				<div class="main-content-page">
					<div class="content-contact-page">
						<div class="box-map">
							<h2 class="title30">contact us</h2>
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193747.6573521406!2d-74.0850833747319!3d40.64515936128871!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24416947c2109%3A0x82765c7404007886!2zQnJvb2tseW4sIFRp4buDdSBiYW5nIE5ldyBZb3JrLCBIb2EgS-G7sw!5e0!3m2!1svi!2sin!4v1499159900961" allowfullscreen></iframe>
						</div>
						<!-- End Google Map -->
						<div class="contact-page-info blockquote">
							<div class="row">
								<div class="col-md-5 col-sm-12 col-xs-12">
									<div class="contact-box">
										<span class="color"><i class="fa fa-institution"></i></span>
										<label class="title16 color">ADDRESS:</label>
										<p class="desc">The Company Name Inc. 4320 St Vincent Place,Glasgow, DC 28</p>
									</div>
								</div>
								<div class="col-md-4 col-sm-7 col-xs-12">
									<div class="contact-box">
										<span class="color"><i class="fa fa-phone"></i></span>
										<ul class="list-inline-block">
											<li>
												<label class="title16 color">PHONES:</label>
											</li>
											<li>
												<span>800-6688-999;</span>
												<span>800-8866-404</span>
											</li>
										</ul>
									</div>
									<div class="contact-box">
										<span class="color"><i class="fa fa-fax"></i></span>
										<ul class="list-inline-block">
											<li>
												<label class="title16 color">Fax:</label>
											</li>
											<li>
												<span>800-6969-0044;</span>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-md-3 col-sm-5 col-xs-12">
									<div class="contact-box">
										<span class="color"><i class="fa fa-envelope-open"></i></span>
										<label class="title16 color">e-mail:</label>
										<p class="desc"><a href="#">smartbuy@gmail.com</a></p>
									</div>
								</div>
							</div>
						</div>
						<!-- End Contact Info -->
						<div class="contact-form-faq">
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="contact-form">
										<h2 class="title18">Contact Form</h2>
										<form>
											<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Your name *" type="text">
											<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Your e-mail address *" type="text">
											<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Subject *" type="text">
											<textarea onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" rows="7">Message *</textarea>
											<input type="submit" value="send" class="shop-button" />
											<input type="reset" value="Clear" class="shop-button style2" />
										</form>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="contact-faq">
										<h2 class="title18">FAQs</h2>
										<div class="contact-accordion toggle-tab">
											<div class="item-toggle-tab active">
												<h2 class="toggle-tab-title">At vero eos et accusamus et iusto</h2>
												<p class="desc toggle-tab-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi archi tecto aspernatur assumenda cum inventore labore magnam </p>
											</div>
											<div class="item-toggle-tab">
												<h2 class="toggle-tab-title">Dignissimos ducimus qui blanditiis</h2>
												<p class="desc toggle-tab-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi archi tecto aspernatur assumenda cum inventore labore magnam </p>
											</div>
											<div class="item-toggle-tab">
												<h2 class="toggle-tab-title">Raesentium voluptatum deleniti</h2>
												<p class="desc toggle-tab-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi archi tecto aspernatur assumenda cum inventore labore magnam </p>
											</div>
											<div class="item-toggle-tab">
												<h2 class="toggle-tab-title">At vero eos et accusamus et iusto</h2>
												<p class="desc toggle-tab-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi archi tecto aspernatur assumenda cum inventore labore magnam </p>
											</div>
											<div class="item-toggle-tab">
												<h2 class="toggle-tab-title">Dignissimos ducimus qui blanditiis</h2>
												<p class="desc toggle-tab-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid animi archi tecto aspernatur assumenda cum inventore labore magnam </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="banner-form banner-form-about clearfix">
						<div class="logo">
							<img src="images/logo-2.png" alt="" />
						</div>
						<div class="box-text">
							<p>Enter our weekly drawing for a $100 gift card and exclusive access to releases, events & more!</p>
						</div>
						<div class="box-letter">
							<form action="mailto:support@kutetheme.com" method="post" enctype="text/plain">
								<div class="txt-mail" >
									<input type="text" name="mail">
								</div>
								<div class="btn-submit">
									<input type="submit" value="">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End Content -->
	
</div>

</div>



@endsection

