@extends('shop.include.layout')


@section('content')




<div class="wrap">

<div id="content">
<div class="content">
<div class="container">
		<div class="main-detail">
			<div class="row">
				<div class="col-lg-5 col-md-4 col-sm-4">
					<div class="detail-product">
						<div class="custom-container widget-detail">
							<div class="thumb-carousel">
								<div class="carousel-detail">
									<ul class="list-inline">
										@if($product->files->count() > 0)


										@foreach($product->files as $photo)
										<li class="active"><a href="#"><img src="{{$photo->full_file}}" alt=""></a></li>
										
										@endforeach
										@else
										<li class="active"><a href="#"><img src='{{asset('uploads/settings/15506911256068.jpg')}}' alt=""></a></li>
										@endif
									</ul>
								</div>  
							</div>
							<div class="mid">
								<a href="#"><img src="images/products/home/home-22.jpg" alt="1"></a>
								<p class="desc">Roll over on the image to zoom</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4">
					<div class="detail-info">
						<div class="name-pro">
							{{-- <span class="text-grey">Ojia</span> --}}
							<p class="desc">{{$product->title}}</p>
							<div class="rating-box">
								<div class="product-rating">													
									<div class="inner-rating"></div>
									<div class="feedback-rating">
										<ul class="list-inline">
											<li>
												<div class="number-star">
													<span>5 Stars (2)</span>
												</div>
												<div class="progress-star">
													<div class="average-progress">
														<div class="inner-average-progress" ></div>
													</div>
												</div>
											</li>
											<li>
												<div class="number-star">
													<span>4 Stars (0)</span>
												</div>
												<div class="progress-star">
													<div class="average-progress">
														<div class="inner-average-progress" ></div>
													</div>
												</div>
											</li>
											<li>
												<div class="number-star">
													<span>3 Stars (0)</span>
												</div>
												<div class="progress-star">
													<div class="average-progress">
														<div class="inner-average-progress" ></div>
													</div>
												</div>
											</li>
											<li>
												<div class="number-star">
													<span>2 Stars (0)</span>
												</div>
												<div class="progress-star">
													<div class="average-progress">
														<div class="inner-average-progress" ></div>
													</div>
												</div>
											</li>
											<li>
												<div class="number-star">
													<span>1 Stars (0)</span>
												</div>
												<div class="progress-star">
													<div class="average-progress">
														<div class="inner-average-progress" ></div>
													</div>
												</div>
											</li>
										</ul>
										<a href="#">See all 340 reviews <i class="fa fa-caret-right" aria-hidden="true"></i></a>
									</div>
								</div>
								<i class="fa fa-caret-down" aria-hidden="true"></i>
								<p class="text-grey">340 customer reviews</p>
							</div>
						</div>

						<div class="price-pro">
							<p class="desc">@lang('shop.price'): <span class="sp-1">${{$product->price}}</span></p>
							<p class="text-red">Sale: <span class="sp-2">$29.99</span></p>
							<p class="desc">You Save: <span class="text-red sp-3">$5.00 (14%)</span></p>
							<span class="text-green">In Stock</span>
						</div>
						<div class="size-select">
							<span>Size:</span>
							<select>
								<option>12x20 Inch</option>
								<option>Select</option>
								<option>18x18 Inch</option>
								<option>20x20 Inch</option>
								<option>24x24 Inch</option>
								<option>28x28 Inch</option>
							</select>
						</div>
						<div class="img-pro img-pro-service">
							<p class="desc">Color: <span class="text-color">Grey</span></p>
							<ul class="list-inline">
								<li>
									<img src="images/products/home/home-24.jpg" alt="" />
								</li>
								<li>
									<img src="images/products/home/home-25.jpg" alt="" />
								</li>
								<li>
									<img src="images/products/home/home-22.jpg" alt="" />
								</li>
							</ul>
						</div>
						<div class="text-box">
							<ul class="list-inline">
								<li>
									<p class="desc"><i class="fa fa-circle" aria-hidden="true"></i> 100% handmade and made of environment-friendly material: Silk and polyester fabric of two sides .Very healthy and eco-friendly!</p>
								</li>
								<li>
									<p class="desc"><i class="fa fa-circle" aria-hidden="true"></i> Have a promote health and great decorate with OJIA luxury super smooth and soft silky satin marble pillow case</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-4">
					<div class="detai-cart">
						<div class="qty-box">
							<span>Qty:</span>
							<input name="qty" value="1" id="qty" type="text">
						</div>
						<div class="txt-delivery">
							<span>Scheduled delivery</span>
							<p class="desc">Delivery will be scheduled during checkout. Signature required.</p>
						</div>
						<div class="check-box">
							<a href="#"><span>Include</span> 5-Year Accident Protection for $112.05</a>
							<a href="#"><span>Include</span> 3-Year Accident Protection for $67.86</a>
						</div>
						<div class="product-extra-link">
							<a href="#" class="cart">add to cart <i class="fa fa-opencart" aria-hidden="true"></i></a>
							<a href="#" class="wishlist">add to wishlist <i class="fa fa-heart" aria-hidden="true"></i></a>
						</div>
						<div class="info-cart">
							<p class="desc">Categories: <span>Appliances</span></p>
							<p class="desc">ID: <span>SB-0900glasse</span></p>
							<p class="desc">Tags: <span>glasses, water bottle</span></p>
						</div>
						<div class="socical-box">
							<span>
								Share 
								<i class="fa fa-envelope-o" aria-hidden="true"></i>
								<i class="fa fa-facebook-official" aria-hidden="true"></i>
								<i class="fa fa-twitter" aria-hidden="true"></i>
								<i class="fa fa-pinterest" aria-hidden="true"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-tab-detail">
			<ul class="nav nav-tabs list-inline" role="tablist">
				<li role="presentation" class="active"><a data-toggle="tab" href="#tab-detail1"><h2>Product Description </h2></a></li>
				<li role="presentation"><a data-toggle="tab" href="#tab-detail2"><h2>Product Detail</h2></a></li>
				<li role="presentation"><a data-toggle="tab" href="#tab-detail3"><h2>Customer reviews</h2></a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade in active" id="tab-detail1">
					<div class="info-box">
						<div class="info-item clearfix">
							{{-- <div class="title"><h3>Expertly Designed To Keep You Happy, Healthy And Well Hydrated</h3></div> --}}
							{{-- <div class="img-box">
								<a href="#"><img src="images/products/home/home-03-2.jpg" alt="" /></a>
							</div> --}}
							<div class="txt-box">	
								<p class="desc">{{$product->content}}</p>
							</div>
						</div>
						{{-- <div class="info-item clearfix">
							<div class="title"><h3>Help Bring Clean Drinking Water To 663 Million People</h3></div>
							<div class="img-box">	
								<a href="#"><img src="images/blog/blog_50.jpg" alt="" /></a>
							</div>
							<div class="txt-box">	
								<p class="desc">We believe that everyone deserves clean drinking water,Every time you buy a Soma water bottle, we make a donation that helps charity: water build sustainable, community-owned water projects in developing countries.</p>
								<p class="desc">We're incredibly proud to say that thousands of people now have access to clean drinking water because of people like you who have bought Soma products. But we're just getting started.With your help, we want to bring clean drinking water to millions more.</p>
							</div>
						</div> --}}
					</div>
				</div>


				<div role="tabpane2" class="tab-pane fade in" id="tab-detail2">
					<div class="detail-addition">
						<table class="table table-bordered table-striped">
							<tr>
								<td><p class="desc">Frame Material: Wood</p></td>
								<td><p class="desc">Seat Material: Wood</p></td>
							</tr>
							<tr>
								<td><p class="desc">Adjustable Height: No</p></td>
								<td><p class="desc">Seat Style: Saddle</p></td>
							</tr>
							<tr>
								<td><p class="desc">Distressed: No</p></td>
								<td><p class="desc">Custom Made: No</p></td>
							</tr>
							<tr>
								<td><p class="desc">Number of Items Included: 1</p></td>
								<td><p class="desc">Folding: No</p></td>
							</tr>
							<tr>
								<td><p class="desc">Stackable: No</p></td>
								<td><p class="desc">Cushions Included: No</p></td>
							</tr>
							<tr>
								<td><p class="desc">Arms Included: No</p></td>
								<td>
									<div class="product-more-info">
										<p class="desc">Legs Included: Yes</p>
										<ul class="list-none">
											<li><a href="#">Leg Material: Wood</a></li>
											<li><a href="#">Number of Legs: 4</a></li>
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<td><p class="desc">Footrest Included: Yes</p>	</td>
								<td><p class="desc">Casters Included: No</p></td>
							</tr>
							<tr>
								<td><p class="desc">Nailhead Trim: No</p></td>
								<td><p class="desc">Weight Capacity: 225 Kilogramm</td>
							</tr>
							<tr>
								<td><p class="desc">Commercial Use: No</p></td>
								<td><p class="desc">Country of Manufacture: Vietnam</p></td>
							</tr>
						</table>
					</div>
				</div>
				<div role="tabpane3" class="tab-pane fade in" id="tab-detail3">
					<div class="content-tags-detail">
						<h3 class="title14">2 Review for bakery macaron</h3>
						<ul class="list-none list-tags-review">
							<li>
								<div class="review-author">
									<a href="#"><img src="images/pages/av1.jpg" alt=""></a>
								</div>
								<div class="review-info">
									<p class="review-header"><a href="#"><strong>7up-theme</strong></a> &ndash; March 30, 2017:</p>
									<div class="rating-box">
										<div class="product-rating">													
											<div class="inner-rating"></div>
										</div>
									</div>
									<p class="desc">Really a nice stool. It was better than I expected in quality. The color is a rich, honey brown and looks a little lighter than pictured but still a great stool for the money.</p>
								</div>
							</li>
							<li>
								<div class="review-author">
									<a href="#"><img src="images/pages/av2.jpg" alt=""></a>
								</div>
								<div class="review-info">
									<p class="review-header"><a href="#"><strong>7up-theme</strong></a> &ndash; March 30, 2017:</p>
									<div class="rating-box">
										<div class="product-rating">													
											<div class="inner-rating"></div>
										</div>
									</div>
									<p class="desc">Really a nice stool. It was better than I expected in quality. The color is a rich, honey brown and looks a little lighter than pictured but still a great stool for the money.</p>
								</div>
							</li>
						</ul>
						<div class="add-review-form">
							<h3 class="title14">Add a Review</h3>
							<p>Your email address will not be published. Required fields are marked *</p>
							<form class="review-form">
								<div>
									<label>Name *</label>
									<input name="name" id="name" type="text">
								</div>
								<div>
									<label>Email *</label>
									<input name="email" id="email" type="text">
								</div>
								<div>
									<label>Your Rating</label>
									<div class="rating-box">
										<div class="product-rating">													
											<div class="inner-rating"></div>
										</div>
									</div>
								</div>
								<div>
									<label>Your Review *</label>
									<textarea name="messasge" id="message" cols="30" rows="10"></textarea>
								</div>
								<div>
									<input class="btn-rect radius6" value="Submit" type="submit">
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="lastest-pro-detail">
			<div class="title">
				<h3>Lastest Product</h3>
			</div>
			<div class="wrap-pro">
				<div class="wrap-item" data-navigation="true" data-pagination="false" data-itemscustom="[[0,1],[480,1],[768,4],[992,4]]" data-autoplay="false">
					@foreach($products as $product)
					<div class="item">
						<div class="product-extra-link">
							<a href="#" class="quick-view"><i class="fa fa-eye" aria-hidden="true"></i><span>

							@lang('shop.quickView')

							</span></a>
							<a href="#" class="box-hidden wishlist"><i class="fa fa-heart" aria-hidden="true"></i><span>@lang('shop.add_to_Wishlist')</span></a>
						</div>

						<div class="thumb-product">

							@if($product->files->count() > 0)
									<a href="#"><img width='100%' height='100%' src='{{Storage::url($product->files->first()->full_file)}}'>  </a>
									@else
									<a href="#"><img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'> </a>

									@endif
						</div>
						<div class="name-product">
							<h3><a href="#">{{$product->title}}</a></h3>
						</div>
						@php
						$productPrice = explode('.',$product->price, 2);
						@endphp
						<div class="box-cart">
							<a href="#" class="cart">@lang('shop.add-to-cart') <i class="fa fa-opencart" aria-hidden="true"></i></a>
							<ins class="price"><sup>$</sup>{{$productPrice[0]}}<sup>.{{$productPrice[1]}}</sup></ins>
						</div>
						<a href="#" class="customize"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!-- End Content -->

</div>



@endsection

