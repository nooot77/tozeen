<footer>
<div id="footer">
<div class="footer">
<div class="top-footer top-footer-3 top-footer-4">
<div class="container">
<div class="row">
	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
		<div class="logo">
			<img src="images/logo-2.png" alt="" />
		</div>
	</div>
	<div class="col-lg-5 col-md-5 col-sm-7 col-xs-12">
		<div class="footer-menu">
			<ul class="list-inline">
				<li><a href="#">@lang('shop.aboutUs')</a></li>
				<li><a href="#">@lang('shop.blog')</a></li>
				<li><a href="#">@lang('shop.FAQs')</a></li>
				<li><a href="#">@lang('shop.Order_tracking')</a></li>
				<li><a href="contact.html">@lang('shop.contact')</a></li>
			</ul>
		</div>
	</div>
	<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
		<div class="box-icon-sn">
			<ul class="list-inline">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>
</div>
</div>
<div class="info-footer info-footer-3">
<div class="container">
<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-4">
		<div class="info-box">
			<h3>@lang('shop.contactUs')</h3>
			<ul class="text address list-inline">
				<li><i class="fa fa-home" aria-hidden="true"></i>Our business address is 1063 Freelon Street San Francisco, CA 95108</li>
				<li><i class="fa fa-volume-control-phone" aria-hidden="true"></i>84.24 01678 311 160</li>
				<li><a href="#"><i class="fa fa-envelope-open-o" aria-hidden="true"></i>contact.7uptheme@gmail.com</a></li>
			</ul>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4">
		<div class="info-box">
			<h3>Smart buy Guide</h3>
			<ul class="text list-inline">
				<li><a href="#">@lang('shop.myAccount')</a></li>
				<li><a href="#">@lang('shop.checkout')</a></li>
				<li><a href="#">@lang('shop.Order_tracking')</a></li>
				<li><a href="#">@lang('shop.Return_Order')</a></li>
				<li><a href="#">@lang('shop.Help_Support')</a></li>
			</ul>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4">
		<div class="box-cart">
			<h3>@lang('shop.payment_method')</h3>
			<ul class="list-inline">
				<li><a href="#">

					<img src="{{asset("shop/images/cart/cart-1.png")}}"  alt="" /></a></li>

				<li><a href="#">

					<img src="{{asset("shop/images/cart/cart-2.png")}}" alt="" /></a></li>

				<li><a href="#">

					<img src="{{asset("shop/images/cart/cart-3.png")}}" alt="" /></a></li>

				<li><a href="#">

					<img src="{{asset("shop/images/cart/cart-4.png")}} "alt="" /></a></li>

				<li><a href="#">

					<img src="{{asset("shop/images/cart/cart-5.png")}}" 
					alt="" /></a></li>
			</ul>
		</div>
		<div class="box-language">

			<h3>@lang('shop.language')</h3>

			<ul class="list-inline">

				<li>
					<a href="{{route('selectLang', 'en')}}">
						<img src="{{asset("shop/images/language/lg-1.png")}}" alt="" />
					</a>
				</li>

				<li>
					<a href="{{route('selectLang', 'ar')}}">
						<img src="{{asset("shop/images/language/saudi-arabia-flag-icon-16.png")}}" alt="" />
					</a>
				</li>
				
			</ul>
		</div>
	</div>
</div>
<div class="box-madeby">
	<p>&copy; 2017 Powered by <a href="http://7uptheme.com/">7uptheme.com<sup>TM</sup></a>. All Rights Reserved</p>
</div>
</div>
<a href="#" id="back-to-top" class="bg-color"><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
</div>
</div>
</div>
<!-- End Footer -->

</footer>

