<!DOCTYPE html>
<html lang="en">
<head>
	 <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>Home Main 2</title>
	
	@include('shop.include.head')


</head>
<body>
	
	@include('shop.include.header')
	



	@yield('content')

@include('shop.include.footer')
@include('shop.include.foot')
</body>
</html>



