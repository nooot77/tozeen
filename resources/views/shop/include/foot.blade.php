<script src="{{asset('shop/js/libs/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('shop/js/libs/bootstrap.min.js')}}"></script>
	<script src="{{asset('shop/js/libs/jquery.fancybox.js')}}"></script>
	<script src="{{asset('shop/js/libs/TimeCircles.js')}}"></script>
	<script src="{{asset('shop/js/libs/jquery-ui.js')}}"></script>
	<script src="{{asset('shop/js/libs/owl.carousel.js')}}"></script>
	<script src="{{asset('shop/js/theme.js')}}">
		
	</script>

	@stack('js')