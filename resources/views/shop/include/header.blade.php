<div id="header">
<div class="header">
<div class="top-header top-header-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
				<div class="logo">
					<a href="home-01.html"><img src="images/logo-2.png" alt="" /></a>
				</div>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
				<nav class="main-nav">
					<a href="#" class="toggle-mobile-menu"><span></span></a>
					<ul class="list-inline">
						<li class="menu-item-has-children">
							<a href="{{route('shop.home')}}">@lang('shop.home')</a>
						
						</li>
						<li class="menu-item-has-children has-mega-menu">
							<a href="#">@lang('shop.featured')</a>
							{{-- <div class="mega-menu mega-menu-v1">
								<div class="sidebar-mega-menu">
									<ul class="nav nav-tabs list-inline" role="tablist">
										<li role="presentation" class="active"><h3><a href="#garden-pro" data-toggle="tab">Garden</a></h3></li>
										<li role="presentation"><h3><a href="#home-pro" data-toggle="tab">Home</a></h3></li>
										<li role="presentation"><h3><a href="#tech-pro" data-toggle="tab">tech</a></h3></li>
									</ul>
								</div>
								<div class="content-mega-menu">
									<div class="tab-content">
										<div id="garden-pro" class="tab-pane fade in active" role="tabpanel">
											<div class="list-product-mega-menu">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/garden/garden-01.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/garden/garden-02.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/garden/garden-03.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Tab -->
										<div id="home-pro" class="tab-pane fade in" role="tabpanel">
											<div class="list-product-mega-menu">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/home/home-01.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/home/home-02.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/home/home-03.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Tab -->
										<div id="tech-pro" class="tab-pane fade in" role="tabpanel">
											<div class="list-product-mega-menu">
												<div class="row">
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/tech/tech-01.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/tech/tech-02.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
													<div class="col-md-4 col-sm-4 col-xs-12">
														<div class="item">
															<div class="thumb-product">
																<a href="#"><img src="images/products/tech/tech-03.jpg" alt="" /></a>
															</div>
															<div class="name-product">
																<h3><a href="#">Soma Glass Water Bottle</a></h3>
															</div>
															<div class="box-cart">
																<ins class="price"><sup>$</sup>85<sup>.99</sup></ins>
															</div>
															<a href="customize.html" class="customize various" data-fancybox-type="iframe"><i class="fa fa-wrench" aria-hidden="true"></i> customize</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> --}}
						</li>
						<li class="menu-item-has-children has-mega-menu">
							<a href="#">@lang('shop.categories')</a>
							<div class="mega-menu mega-menu-v2">
								<div class="content-mega-menu">
									<div class="row">
										<div class="col-md-3 col-sm-4 col-xs-12">
											<div class="mega-list-cat">
												<h2 class="title18 font-bold text-uppercase">CATEGORY PAGE</h2>
												<ul class="list-none">
													<li><a href="#">Off-Canvas Filtering</a></li>
													<li><a href="#">Full Width Layout</a></li>
													<li><a href="#">List layout</a></li>
													<li><a href="#">Masonry layout</a></li>
													<li><a href="#">Top Content</a></li>
													<li><a href="#">Transparent Header</a></li>
													<li><a href="#">Dark Style</a></li>
												</ul>
											</div>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-12">
											<div class="mega-list-cat">
												<h2 class="title18 font-bold text-uppercase">product PAGE</h2>
												<ul class="list-none">
													<li><a href="#">Left Column</a></li>
													<li><a href="#">Right Column</a></li>
													<li><a href="#">Full Width Gallery</a></li>
													<li><a href="#">Vertical Gallery</a></li>
													<li><a href="#">Variations</a></li>
													<li><a href="#">Transparent Header</a></li>
													<li><a href="#">Affiliat</a></li>
												</ul>
											</div>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-12">
											<div class="mega-list-cat">
												<h2 class="title18 font-bold text-uppercase">more...</h2>
												<ul class="list-none">
													<li><a href="#">Accordion Style</a></li>
													<li><a href="#">Section Style</a></li>
													<li><a href="#">Vertical Tabs</a></li>
													<li><a href="#">Normal Tabs</a></li>
													<li><a href="#">Dark Style</a></li>
													<li><a href="#">Image Zoom</a></li>
													<li><a href="#">Product Examples</a></li>
												</ul>
											</div>
										</div>
										<div class="col-md-3 hidden-sm col-xs-12">
											<div class="mega-menu-thumb banner-adv zoom-image line-scale"><a href="#" class="adv-thumb-link"><img src="images/blog/blog_03_1.jpg" alt=""></a></div>
										</div>
									</div>
								</div>
							</div>
						</li>
						
						<li class="menu-item-has-children">
							<a href="#">@lang('shop.blog')</a>
							<ul class="sub-menu list-inline">
								<li class="menu-item-preview">
									<a href="blog_featured.html">Blog Featured</a>
								</li>
								<li class="menu-item-preview">
									<a href="blog_list.html">Blog List</a>
								</li>
								<li class="menu-item-preview">
									<a href="blog_grid.html">Blog Grid</a>
								</li>
							</ul>
						</li>
						<li><a href="about.html">@lang('shop.about')</a></li>
						<li><a href="contact.html">@lang('shop.contact')</a></li>
						<li class="menu-item-has-children">
							<a href="#">shop</a>
							<ul class="sub-menu list-inline">
								<li class="menu-item-preview">
									<a href="gridview.html">Gridview</a>
								</li>
								<li class="menu-item-preview">
									<a href="gridview_full.html">Gridviewfull</a>
								</li>
								<li class="menu-item-preview">
									<a href="listview.html">Listview</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_buy.html">product show buy</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_color_price.html">product show color price</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_color_service.html">product show color service</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_color_size.html">product show color size</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_color_size_select.html">product show color size select</a>
								</li>
								<li class="menu-item-preview">
									<a href="product_show_video.html">product show video</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-has-children">
							<a href="#">page</a>
							<ul class="sub-menu list-inline">
								<li class="menu-item-preview">
									<a href="404.html">404 page</a>
								</li>
								<li class="menu-item-preview">
									<a href="cart.html">cart</a>
								</li>
								<li class="menu-item-preview">
									<a href="checkout.html">checkout</a>
								</li>
								<li class="menu-item-preview">
									<a href="coming-soon.html">coming-soon</a>
								</li>
								<li class="menu-item-preview">
									<a href="compare-product.html">compare product</a>
								</li>
								<li class="menu-item-preview">
									<a href="member-login.html">member login</a>
								</li>
								<li class="menu-item-preview">
									<a href="my-profile.html">my profile</a>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-3  col-xs-3">
				<div class="box-right">
					<ul class="list-inline">
						<li><a href="#" class="account"><i class="fa fa-user-o" aria-hidden="true"></i></a></li>


						<li class="mini-cart-box">
							<a href="#"><i class="fa fa-opencart" aria-hidden="true"></i><sup class="mini-cart-sum">

								@auth
								{{\Auth::user()->cart->sum('quantity')}}
								@endauth

								@guest
								0
								@endguest

								


							</sup></a>




							{{-- nav cart menu --}}
							@auth
							<div class="mini-cart-content">
								<h2>(

									
									{{\Auth::user()->cart->sum('quantity')}}
									

								)


								 @lang('shop.items_in_my_cart')
								</h2>



								<div class="mini-cart-item">


									@if(\Auth::user()->cart->count() > 0)

										@foreach(\Auth::user()->cart->take(2) as $cart)
									<div class="product-mini-cart clearfix">
										<div class="product-thumb">


								@if($cart->product->files->count() > 0)


								<a href="{{route('shop.product.show', ['id' => $cart->product->id])}}">
								<img width='100%' height='100%' src='{{Storage::url($product->files->first()->full_file)}}'> 
								 </a>

								@else

									<a href="{{route('shop.product.show', ['id' => $cart->product->id])}}">
										<img width='100%' height='100%' src='{{asset('uploads/settings/15506911256068.jpg')}}'>
									 </a>

								@endif

											

										</div>
										<div class="product-info">
											<h3 class="product-title">
												<a href="{{route('shop.product.show', ['id' => $cart->product->id])}}">
													{{$cart->product->title}}

												</a></h3>
											<div class="price">
												<span class="old-price">${{$cart->product->price}}</span>
												<span class="new-price">${{$cart->product->price}}</span>
											</div>
											<div class="rating-box">
												<div class="product-rating">													
													<div class="inner-rating"></div>
												</div>
											</div>
										</div>
									</div>


									@endforeach


									@else

									<h2>you don't have itme </h2>

									@endif



									
								</div>
								<div class="mini-cart-total  clearfix">
									<strong class="pull-left">@lang('shop.total')</strong>
									<span class="pull-right">
										
										@if(\Auth::user()->cart->count() > 0)

											@php

											$newCount = 0;

											foreach(\Auth::user()->cart as $cart){

											$newCount += $cart->product->price * $cart->quantity;

											}

											@endphp

										$ {{ $newCount}}
													

										@else

										$0

										@endif



									</span>
								</div>
								<div class="mini-cart-button">
									<a href="{{route('shop.cart.index')}}">@lang('shop.view-cart') </a>
									<a href="#">@lang('shop.checkout')</a>
								</div>
							</div>



							@endauth
						{{-- if user not signed --}}
							@guest
									
		
						<div class="mini-cart-content">
							
						<h2>you cant see cart now please register before</h2>

						</div>
						
									@endguest



						</li>



					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="cate-header">
	<div class="container">
		<ul class="list-inline">
			<li><a href="#">Home</a></li>
			<li><a href="#">Kitchen & Dining </a></li>
			<li><a href="#">Furniture</a></li>
			<li><a href="#">Bedding & Bath</a></li>
			<li><a href="#">Appliances</a></li>
			<li><a href="#">Home Improvement</a></li>
			<li><a href="#">Power & Hand Tools</a></li>
			<li><a href="#">Kitchen & Bath Fixtures</a></li>
		</ul>
	</div>
</div>
<div class="box-breadcrumb">
	<div class="container">
		<ul class="list-inline">
			<li><a href="#">Home <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			<li><a href="#">Appliances</a></li>
		</ul>
	</div>
</div>
</div>
</div>
<!-- End Header -->