
	
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/font-awesome.min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/bootstrap.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/bootstrap-theme.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/jquery.fancybox.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/jquery-ui.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/owl.carousel.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/owl.transitions.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/owl.theme.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/hover-min.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/libs/animate.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/color.css')}}" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/theme.css')}}" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('shop/css/responsive.css')}}" media="all"/>
	@if(\App::getLocale() == 'ar')
	 <link rel="stylesheet" type="text/css" href="{{asset('shop/css/rtl.css')}}" media="all"/> 
	@endif