function check_all()
{

  $("#checkAll").click(function () {
      $('input:checkbox').not(this).prop('checked', this.checked);
  });
}

function delete_all()
{
  $(document).on('click','.del_all',function(){
     $('#form_data').submit();
  });
  $(document).on('click','.delBtn',function(){

    var item_checked = $(':checkbox.checkItem').prop('checked');
    var numberOfChecked = $(':checkbox.checkItem:checked').length;
    if (numberOfChecked > 0 ) {
      $('.record_count').text(numberOfChecked);
      $('.not_empty_record').removeClass('hidden');
      $('.empty_record').addClass('hidden');

    }else{
      $('.record_count').text('');
      $('.not_empty_record').addClass('hidden');
      $('.empty_record').removeClass('hidden');
    }
    $('#mutlipleDelete').modal('show');
  });

}
