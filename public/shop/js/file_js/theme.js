(function($){
"use strict"; // Start of use strict
//after_action
	function after_action(){
		var current =  this.owl.currentItem;
		current = Number(current)+1;
		var totalcurrentItems = current*6;
		this.$elem.parents('.clothing').find(".current-total-items").html(totalcurrentItems);
	};
	//menu reponsive
	function menu_responsive(){
		$('body').on('click',function(event){
			if($(window).width()<768){
				$('.nav-box>ul').slideUp('slow');
			}
		});
		$('.toggle-mobile-menu').on('click',function(event){
			if($(window).width()<768){
				event.preventDefault();
				event.stopPropagation();
				$('.nav-box>ul').slideToggle('slow');
			}
		});
		$('.nav-box li.has-children>a').on('click',function(event){
			if($(window).width()<768 && !$(this).parent().hasClass('has-mega-menu')){
				event.preventDefault();
				event.stopPropagation();
				$(this).next().stop(true,true).slideToggle('slow');
			}
		});
	};
//Document Ready
function fixed_header(){
	var ht = $('.header').height();
	var st = $(window).scrollTop();
	//console.log(st);
	if(st>ht){
		$('.header').addClass('fixed-header');
	}else{
		$('.header').removeClass('fixed-header');
	}
}
function get_html(){
	if($('.main-slider').find('.owl-item.active').prev().length>0){
		$('.wrap-item-prev').html($('.main-slider').find('.owl-item.active').prev().find('.slide').html());
	}else{
		$('.wrap-item-prev').html($('.main-slider').find('.owl-item').last().find('.slide').html());
	}
	if($('.main-slider').find('.owl-item.active').next().length>0){
		$('.wrap-item-next').html($('.main-slider').find('.owl-item.active').next().find('.slide').html());
	}else{
		$('.wrap-item-next').html($('.main-slider').find('.owl-item').first().find('.slide').html());
	}
}
jQuery(document).ready(function(){
	menu_responsive();
});

$( window ).resize(function() {
  if($(window).width()>=768) $('.nav-box>ul').slideDown('slow');
});

//Window Load
jQuery(window).load(function(){ 
	//Product Slider 7
	if($('.banner-slide').length>0){
		$('.banner-slide').each(function(){
			$(this).find('.wrap-item').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
				afterAction: get_html,
			});
			if($(window).width()>1024){
				get_html();
			}
			$(".banner-slide .owl-prev").on('mouseover',function(){
				$('.wrap-item-prev').addClass('active');
			});
			$(".banner-slide .owl-prev").on('mouseout',function(){
				$('.wrap-item-prev').removeClass('active');
			});
			$(".banner-slide .owl-next").on('mouseover',function(){
				$('.wrap-item-next').addClass('active');
			});
			$(".banner-slide .owl-next").on('mouseout',function(){
				$('.wrap-item-next').removeClass('active');
			});
		});
	}
	//slide seller
	if($('.seller').length>0){
		$('.seller').each(function(){
			$(this).find('.product-content').owlCarousel({
				items: 4,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[520, 2],
				[768, 3], 
				[992, 4], 
				[1200, 4] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	// slide-designer
	if($('.wrap-designer').length>0){
		$('.wrap-designer').each(function(){
			$(this).find('.box-content-design').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//slide collection
	if($('.collection').length>0){
		$('.collection').each(function(){
			$(this).find('.main-content').owlCarousel({
				items: 5,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[560, 2],
				[768, 3], 
				[992, 4], 
				[1200, 5] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//slide upto
	if($('.upto').length>0){
		$('.upto').each(function(){
			$(this).find('.product-box').owlCarousel({
				items: 3,
				itemsCustom: [ 
				[0, 1], 
				[480, 1],
				[640, 2],
				[768, 2], 
				[992, 3], 
				[1200, 3] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//Deal Count Down Special
	if($('.content-deal-countdown').length>0){
		$('.content-deal-countdown').TimeCircles({
			fg_width: 0.03,
			bg_width: 1.2,
			text_size: 0.07,
			circle_bg_color: "#000000",
			time: {
				Days: {
					show: true,
					text: "d",
					color: "#000000"
				},
				Hours: {
					show: true,
					text: "h",
					color: "#000000"
				},
				Minutes: {
					show: true,
					text: "m",
					color: "#000000"
				},
				Seconds: {
					show: true,
					text: "s",
					color: "#000000"
				}
			}
		}); 
	}
	if ($('#back-to-top').length>0) {
		$('#back-to-top').on('click', function (e) {
			e.preventDefault();
			$('html,body').animate({
				scrollTop: 0
			}, 700);
		});
		$(window).scroll(function(){
			if ($(this).scrollTop() > 300) {
				$('#back-to-top').fadeIn();
			} 
			else {
			$('#back-to-top').fadeOut();
			}
		});
	}
	//fixed-header
	if($(window).width()>1024){		
		fixed_header();
		$(window).scroll(function(){
			fixed_header();
		});
	}
	// slide of blog
	if($('.page-item').length>0){
		$('.page-item').each(function(){
			$(this).find('.wrap-item').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//slide-blogdetail
	if($('.page-detail').length>0){
		$('.page-detail').each(function(){
			$(this).find('.wrap-comment').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: true,
				navigation: true,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
				afterAction: function(e){
					if (this.$owlItems.length > this.options.items) {
						$('#blog-detail .navslider').show();
					} else {
						$('#blog-detail .navslider').hide();
					}
				}
			});
			$('#blog-detail .navslider .previous').on('click', function(e) {
				e.preventDefault();
				$('#blog-detail div.wrap-comment').trigger('owl.prev');
			});
			$('#blog-detail .navslider .next').on('click', function(e) {
				e.preventDefault();
				$('#blog-detail div.wrap-comment').trigger('owl.next');
			});
		});
	}
	//shop-by of LISTVIEW
	$('.main-box>ul li>a').on('click',function(event){
		event.preventDefault();
		$(this).toggleClass('active');
		$(this).next().slideToggle('slow');
	});
	//shop-by of LISTVIEW 
	$('ul.list-show li>a').on('click',function(event){
		event.preventDefault();
		if($(this).parent().hasClass('active')){
			$(this).parent().removeClass('active');
		}else{
			$(this).parent().addClass('active');
		}
	});
	$('.detail-product .color>a').on('click',function(event){
		event.preventDefault();
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		}else{
			$(this).addClass('active');
		}
	});
	// price of listview
	if($("#slider-range").length>0){
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$( "#amount" ).html( "<span class='first-price'><span>"+"$" + ui.values[ 0 ]+"</span></span>" + "<span class='last-price'><span>"+"$" + ui.values[ 1 ] +"</span></span>");
				$('#slider-range').find('.ui-slider-handle').first().html($('.first-price').html());
				$('#slider-range').find('.ui-slider-handle').last().html($('.last-price').html());
			}
		});
		$( "#amount" ).html( "<span class='first-price'><span>"+"$" + $( "#slider-range" ).slider( "values", 0 )+"</span></span>" + "<span class='last-price'><span>"+"$" + $( "#slider-range" ).slider( "values", 1 )+"</span></span>" );	
		$('#slider-range').find('.ui-slider-handle').first().html($('.first-price').html());
		$('#slider-range').find('.ui-slider-handle').last().html($('.last-price').html());
	}
	//list-service of About-page
	$('.content-service.active').find('.text').slideDown('slow');
	$('.content-service > h3').on('click',function(event){
		event.preventDefault();
		$(this).next().slideToggle('slow');
		$(this).parents('ul').find('.content-service').not($(this).parent()).find('.text').slideUp('slow');
		$(this).parents('ul').find('.content-service').not($(this).parent()).removeClass('active');
		$(this).parent().toggleClass('active');
	});
	//detail-product
	if ($(".widget .carousel").length>0){
		$(".widget .carousel").jCarouselLite({
			btnNext: ".widget .next",
			btnPrev: ".widget .prev",
			speed: 800,
			vertical: true,
			visible: 4,
		}); 
	}
	$(".widget img").on('click',function() {
		$(".widget .mid img").attr("src", $(this).attr("src"));
	})
	$('.custom-container .carousel ul li>a').on('click',function(event){
		event.preventDefault();
		$('.custom-container .carousel ul li').removeClass('active');
		$(this).parent().addClass('active');
	});
	//slide upsell product of detail
	if($('.upsell-product').length>0){
		$('.upsell-product').each(function(){
			$(this).find('.product-content').owlCarousel({
				items: 4,
				itemsCustom: [ 
				[0, 1], 
				[480, 2], 
				[768, 3], 
				[992, 4], 
				[1200, 4] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 500,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
		$('.upsell-product .navslider .prev').on('click', function(e) {
			e.preventDefault();
			$('.upsell-product div.item').trigger('owl.next');
		});
		$('.upsell-product .navslider .next').on('click', function(e) {
			e.preventDefault();
			$('.upsell-product div.item').trigger('owl.prev');
		});
	}
	// banner2
	if($('.banner2').length>0){
		$('.banner2').each(function(){
			$(this).find('.wrap-slide').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: true,
				navigation: false,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//new-product index2
	if($('.new-product').length>0){
		$('.new-product').each(function(){
			$(this).find('.product-content').owlCarousel({
				items: 4,
				itemsCustom: [ 
				[0, 1], 
				[480, 2], 
				[768, 3], 
				[992, 4], 
				[1200, 4] 
				],
				pagination: false,
				navigation: true,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
			$('.new-product .load-more').on('click', function(e) {
				e.preventDefault();
				$('.new-product .product-content').trigger('owl.next');
			});
		});
	}
	// slide clothing index2
	if($('.clothing').length>0){
		$('.clothing').each(function(){
			$(this).find('.wrap-box-product').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: false,
				navigation: true,
				navigationText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
				afterAction: after_action,
			});
			var numItems = $(this).find('.owl-item').length;
			var totalItems = numItems*6;
			$(this).find(".total-items").html(totalItems);
		});
	}
	// box-categorie index2
	if($('.main-content2').length>0){
		$('.main-content2 .box-ifo .button').on('click',function(event){
			event.preventDefault();
			$(this).parent().toggleClass('active');
		});
	}
	//footer index2
	if($('.main-footer-2 .testimonials').length>0){
		$('.main-footer-2 .testimonials').each(function(){
			$(this).find('.box-feedback').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: true,
				navigation: false,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : true,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	if($('.main-footer-2 .testimonials').length>0){
		$(".new-product .load-more").insertAfter(".new-product .owl-wrapper");
	}
	if($('.banner-slider-3').length>0){
		$('.banner-slider-3').each(function(){
			$(this).find('.wrap-slide').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: false,
				navigation: true,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//sale product home 3
	if($('.box-list-table-3 .sale-product .wrap-product-content').length>0){
		$('.box-list-table-3 .sale-product .wrap-product-content').each(function(){
			$(this).find('.product-content').owlCarousel({
				items: 1,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 1], 
				[1200, 1] 
				],
				pagination: true,
				navigation: false,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : true,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//brand-showcase
	if($('.main-content-3 .brand-showcase').length>0){
		$('.main-content-3 .brand-showcase').each(function(){
			$(this).find('.tab-content').owlCarousel({
				items: 2,
				itemsCustom: [ 
				[0, 1], 
				[480, 1], 
				[768, 1], 
				[992, 2], 
				[1200, 2] 
				],
				pagination: true,
				navigation: false,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	if($('.main-content-3 .box-show').length>0){
		$('.main-content-3 .box-show').each(function(){
			$(this).find('.wrap-box-show').owlCarousel({
				items: 3,
				itemsCustom: [ 
				[0, 1], 
				[480, 2], 
				[768, 2], 
				[992, 3], 
				[1200, 3] 
				],
				pagination: true,
				navigation: false,
				stopOnHover : false,
				addClassActive : true,
				autoPlay : false,
				slideSpeed : 800,
				paginationSpeed : 800,
				rewindSpeed : 1000,
			});
		});
	}
	//popup index1
	$(".various").fancybox({
		maxWidth	: 900,
		maxHeight	: 680,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
})(jQuery); // End of use strict