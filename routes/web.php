<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Language Route
Route::post('/lang', array(
    'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@index',
))->name('lang');
// For Language direct URL link
Route::get('/lang/{lang}', array(
    'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@change',
))->name('langChange');

Route::get('/lang/{lang}', array(
    'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@select',
))->name('selectLang');

// Route::get('/lang/{lang}', function($lang){

// });
// .. End of Language Route


// Backend Routes
Auth::routes();
Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
})->name('logout');

// Social Auth
Route::get('/oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('/oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');

// default path after login
Route::get('/admin', function () {
    return redirect()->route('adminHome');
});

Route::Group(['prefix' => env('BACKEND_PATH')], function () {

    // No Permission
    Route::get('/403', function () {
        return view('errors.403');
    })->name('NoPermission');

    // Not Found
    Route::get('/404', function () {
        return view('errors.404');
    })->name('NotFound');

    // Admin Home
    Route::get('/', 'HomeController@index')->name('adminHome');
    // Search
    Route::get('/search', 'HomeController@search')->name('adminSearch');
    Route::post('/find', 'HomeController@find')->name('adminFind');

    // Webmaster
    Route::get('/webmaster', 'WebmasterSettingsController@edit')->name('webmasterSettings');
    Route::post('/webmaster', 'WebmasterSettingsController@update')->name('webmasterSettingsUpdate');

    // Webmaster Banners
    Route::get('/webmaster/banners', 'WebmasterBannersController@index')->name('WebmasterBanners');
    Route::get('/webmaster/banners/create', 'WebmasterBannersController@create')->name('WebmasterBannersCreate');
    Route::post('/webmaster/banners/store', 'WebmasterBannersController@store')->name('WebmasterBannersStore');
    Route::get('/webmaster/banners/{id}/edit', 'WebmasterBannersController@edit')->name('WebmasterBannersEdit');
    Route::post('/webmaster/banners/{id}/update', 'WebmasterBannersController@update')->name('WebmasterBannersUpdate');
    Route::get('/webmaster/banners/destroy/{id}',
        'WebmasterBannersController@destroy')->name('WebmasterBannersDestroy');
    Route::post('/webmaster/banners/updateAll',
        'WebmasterBannersController@updateAll')->name('WebmasterBannersUpdateAll');

    // Webmaster Sections
    Route::get('/webmaster/sections', 'WebmasterSectionsController@index')->name('WebmasterSections');
    Route::get('/webmaster/sections/create', 'WebmasterSectionsController@create')->name('WebmasterSectionsCreate');
    Route::post('/webmaster/sections/store', 'WebmasterSectionsController@store')->name('WebmasterSectionsStore');
    Route::get('/webmaster/sections/{id}/edit', 'WebmasterSectionsController@edit')->name('WebmasterSectionsEdit');
    Route::post('/webmaster/sections/{id}/update',
        'WebmasterSectionsController@update')->name('WebmasterSectionsUpdate');

    Route::post('/webmaster/sections/{id}/seo', 'WebmasterSectionsController@seo')->name('WebmasterSectionsSEOUpdate');

    Route::get('/webmaster/sections/destroy/{id}',
        'WebmasterSectionsController@destroy')->name('WebmasterSectionsDestroy');
    Route::post('/webmaster/sections/updateAll',
        'WebmasterSectionsController@updateAll')->name('WebmasterSectionsUpdateAll');

    // Webmaster Sections :Custom Fields
    Route::get('/webmaster/{webmasterId}/fields', 'WebmasterSectionsController@webmasterFields')->name('webmasterFields');
    Route::get('/{webmasterId}/fields/create', 'WebmasterSectionsController@fieldsCreate')->name('webmasterFieldsCreate');
    Route::post('/webmaster/{webmasterId}/fields/store', 'WebmasterSectionsController@fieldsStore')->name('webmasterFieldsStore');
    Route::get('/webmaster/{webmasterId}/fields/{field_id}/edit', 'WebmasterSectionsController@fieldsEdit')->name('webmasterFieldsEdit');
    Route::post('/webmaster/{webmasterId}/fields/{field_id}/update', 'WebmasterSectionsController@fieldsUpdate')->name('webmasterFieldsUpdate');
    Route::get('/webmaster/{webmasterId}/fields/destroy/{field_id}', 'WebmasterSectionsController@fieldsDestroy')->name('webmasterFieldsDestroy');
    Route::post('/webmaster/{webmasterId}/fields/updateAll', 'WebmasterSectionsController@fieldsUpdateAll')->name('webmasterFieldsUpdateAll');

    // Settings
    Route::get('/settings', 'SettingsController@edit')->name('settings');
    Route::post('/settings', 'SettingsController@updateSiteInfo')->name('settingsUpdateSiteInfo');

    // Ad. Banners
    Route::get('/banners', 'BannersController@index')->name('Banners');
    Route::get('/banners/create/{sectionId}', 'BannersController@create')->name('BannersCreate');
    Route::post('/banners/store', 'BannersController@store')->name('BannersStore');
    Route::get('/banners/{id}/edit', 'BannersController@edit')->name('BannersEdit');
    Route::post('/banners/{id}/update', 'BannersController@update')->name('BannersUpdate');
    Route::get('/banners/destroy/{id}', 'BannersController@destroy')->name('BannersDestroy');
    Route::post('/banners/updateAll', 'BannersController@updateAll')->name('BannersUpdateAll');

    // Sections
    Route::get('/{webmasterId}/sections', 'SectionsController@index')->name('sections');
    Route::get('/{webmasterId}/sections/create', 'SectionsController@create')->name('sectionsCreate');
    Route::post('/{webmasterId}/sections/store', 'SectionsController@store')->name('sectionsStore');
    Route::get('/{webmasterId}/sections/{id}/edit', 'SectionsController@edit')->name('sectionsEdit');
    Route::post('/{webmasterId}/sections/{id}/update', 'SectionsController@update')->name('sectionsUpdate');
    Route::post('/{webmasterId}/sections/{id}/seo', 'SectionsController@seo')->name('sectionsSEOUpdate');
    Route::get('/{webmasterId}/sections/destroy/{id}', 'SectionsController@destroy')->name('sectionsDestroy');
    Route::post('/{webmasterId}/sections/updateAll', 'SectionsController@updateAll')->name('sectionsUpdateAll');

    // Topics
    Route::get('/{webmasterId}/topics', 'TopicsController@index')->name('topics');
    Route::get('/{webmasterId}/topics/create', 'TopicsController@create')->name('topicsCreate');
    Route::post('/{webmasterId}/topics/store', 'TopicsController@store')->name('topicsStore');
    Route::get('/{webmasterId}/topics/{id}/edit', 'TopicsController@edit')->name('topicsEdit');
    Route::post('/{webmasterId}/topics/{id}/update', 'TopicsController@update')->name('topicsUpdate');
    Route::get('/{webmasterId}/topics/destroy/{id}', 'TopicsController@destroy')->name('topicsDestroy');
    Route::post('/{webmasterId}/topics/updateAll', 'TopicsController@updateAll')->name('topicsUpdateAll');
    // Topics :SEO
    Route::post('/{webmasterId}/topics/{id}/seo', 'TopicsController@seo')->name('topicsSEOUpdate');
    // Topics :Photos
    Route::post('/{webmasterId}/topics/{id}/photos', 'TopicsController@photos')->name('topicsPhotosEdit');
    Route::get('/{webmasterId}/topics/{id}/photos/{photo_id}/destroy',
        'TopicsController@photosDestroy')->name('topicsPhotosDestroy');
    Route::post('/{webmasterId}/topics/{id}/photos/updateAll',
        'TopicsController@photosUpdateAll')->name('topicsPhotosUpdateAll');

    // Topics :Files
    Route::get('/{webmasterId}/topics/{id}/files', 'TopicsController@topicsFiles')->name('topicsFiles');
    Route::get('/{webmasterId}/topics/{id}/files/create',
        'TopicsController@filesCreate')->name('topicsFilesCreate');
    Route::post('/{webmasterId}/topics/{id}/files/store',
        'TopicsController@filesStore')->name('topicsFilesStore');
    Route::get('/{webmasterId}/topics/{id}/files/{file_id}/edit',
        'TopicsController@filesEdit')->name('topicsFilesEdit');
    Route::post('/{webmasterId}/topics/{id}/files/{file_id}/update',
        'TopicsController@filesUpdate')->name('topicsFilesUpdate');
    Route::get('/{webmasterId}/topics/{id}/files/destroy/{file_id}',
        'TopicsController@filesDestroy')->name('topicsFilesDestroy');
    Route::post('/{webmasterId}/topics/{id}/files/updateAll',
        'TopicsController@filesUpdateAll')->name('topicsFilesUpdateAll');


    // Topics :Related
    Route::get('/{webmasterId}/topics/{id}/related', 'TopicsController@topicsRelated')->name('topicsRelated');
    Route::get('/relatedLoad/{id}', 'TopicsController@topicsRelatedLoad')->name('topicsRelatedLoad');
    Route::get('/{webmasterId}/topics/{id}/related/create',
        'TopicsController@relatedCreate')->name('topicsRelatedCreate');
    Route::post('/{webmasterId}/topics/{id}/related/store',
        'TopicsController@relatedStore')->name('topicsRelatedStore');
    Route::get('/{webmasterId}/topics/{id}/related/destroy/{related_id}',
        'TopicsController@relatedDestroy')->name('topicsRelatedDestroy');
    Route::post('/{webmasterId}/topics/{id}/related/updateAll',
        'TopicsController@relatedUpdateAll')->name('topicsRelatedUpdateAll');
    // Topics :Comments
    Route::get('/{webmasterId}/topics/{id}/comments', 'TopicsController@topicsComments')->name('topicsComments');
    Route::get('/{webmasterId}/topics/{id}/comments/create',
        'TopicsController@commentsCreate')->name('topicsCommentsCreate');
    Route::post('/{webmasterId}/topics/{id}/comments/store',
        'TopicsController@commentsStore')->name('topicsCommentsStore');
    Route::get('/{webmasterId}/topics/{id}/comments/{comment_id}/edit',
        'TopicsController@commentsEdit')->name('topicsCommentsEdit');
    Route::post('/{webmasterId}/topics/{id}/comments/{comment_id}/update',
        'TopicsController@commentsUpdate')->name('topicsCommentsUpdate');
    Route::get('/{webmasterId}/topics/{id}/comments/destroy/{comment_id}',
        'TopicsController@commentsDestroy')->name('topicsCommentsDestroy');
    Route::post('/{webmasterId}/topics/{id}/comments/updateAll',
        'TopicsController@commentsUpdateAll')->name('topicsCommentsUpdateAll');
    // Topics :Maps
    Route::get('/{webmasterId}/topics/{id}/maps', 'TopicsController@topicsMaps')->name('topicsMaps');
    Route::get('/{webmasterId}/topics/{id}/maps/create', 'TopicsController@mapsCreate')->name('topicsMapsCreate');
    Route::post('/{webmasterId}/topics/{id}/maps/store', 'TopicsController@mapsStore')->name('topicsMapsStore');
    Route::get('/{webmasterId}/topics/{id}/maps/{map_id}/edit', 'TopicsController@mapsEdit')->name('topicsMapsEdit');
    Route::post('/{webmasterId}/topics/{id}/maps/{map_id}/update',
        'TopicsController@mapsUpdate')->name('topicsMapsUpdate');
    Route::get('/{webmasterId}/topics/{id}/maps/destroy/{map_id}',
        'TopicsController@mapsDestroy')->name('topicsMapsDestroy');
    Route::post('/{webmasterId}/topics/{id}/maps/updateAll',
        'TopicsController@mapsUpdateAll')->name('topicsMapsUpdateAll');

    // Contacts Groups
    Route::post('/contacts/storeGroup', 'ContactsController@storeGroup')->name('contactsStoreGroup');
    Route::get('/contacts/{id}/editGroup', 'ContactsController@editGroup')->name('contactsEditGroup');
    Route::post('/contacts/{id}/updateGroup', 'ContactsController@updateGroup')->name('contactsUpdateGroup');
    Route::get('/contacts/destroyGroup/{id}', 'ContactsController@destroyGroup')->name('contactsDestroyGroup');
    // Contacts
    Route::get('/contacts/{group_id?}', 'ContactsController@index')->name('contacts');
    Route::post('/contacts/store', 'ContactsController@store')->name('contactsStore');
    Route::post('/contacts/search', 'ContactsController@search')->name('contactsSearch');
    Route::get('/contacts/{id}/edit', 'ContactsController@edit')->name('contactsEdit');
    Route::post('/contacts/{id}/update', 'ContactsController@update')->name('contactsUpdate');
    Route::get('/contacts/destroy/{id}', 'ContactsController@destroy')->name('contactsDestroy');
    Route::post('/contacts/updateAll', 'ContactsController@updateAll')->name('contactsUpdateAll');

    // WebMails Groups
    Route::post('/webmails/storeGroup', 'WebmailsController@storeGroup')->name('webmailsStoreGroup');
    Route::get('/webmails/{id}/editGroup', 'WebmailsController@editGroup')->name('webmailsEditGroup');
    Route::post('/webmails/{id}/updateGroup', 'WebmailsController@updateGroup')->name('webmailsUpdateGroup');
    Route::get('/webmails/destroyGroup/{id}', 'WebmailsController@destroyGroup')->name('webmailsDestroyGroup');
    // WebMails
    Route::post('/webmails/store', 'WebmailsController@store')->name('webmailsStore');
    Route::post('/webmails/search', 'WebmailsController@search')->name('webmailsSearch');
    Route::get('/webmails/{id}/edit', 'WebmailsController@edit')->name('webmailsEdit');
    Route::get('/webmails/{group_id?}/{wid?}/{stat?}/{contact_email?}', 'WebmailsController@index')->name('webmails');
    Route::post('/webmails/{id}/update', 'WebmailsController@update')->name('webmailsUpdate');
    Route::get('/webmails/destroy/{id}', 'WebmailsController@destroy')->name('webmailsDestroy');
    Route::post('/webmails/updateAll', 'WebmailsController@updateAll')->name('webmailsUpdateAll');

    // Calendar
    Route::get('/calendar', 'EventsController@index')->name('calendar');
    Route::get('/calendar/create', 'EventsController@create')->name('calendarCreate');
    Route::post('/calendar/store', 'EventsController@store')->name('calendarStore');
    Route::get('/calendar/{id}/edit', 'EventsController@edit')->name('calendarEdit');
    Route::post('/calendar/{id}/update', 'EventsController@update')->name('calendarUpdate');
    Route::get('/calendar/destroy/{id}', 'EventsController@destroy')->name('calendarDestroy');
    Route::get('/calendar/updateAll', 'EventsController@updateAll')->name('calendarUpdateAll');
    Route::post('/calendar/{id}/extend', 'EventsController@extend')->name('calendarExtend');

    // Analytics
    Route::get('/ip/{ip_code?}', 'AnalyticsController@ip')->name('visitorsIP');
    Route::post('/ip/search', 'AnalyticsController@search')->name('visitorsSearch');
    Route::post('/analytics/{stat}', 'AnalyticsController@filter')->name('analyticsFilter');
    Route::get('/analytics/{stat?}', 'AnalyticsController@index')->name('analytics');
    Route::get('/visitors', 'AnalyticsController@visitors')->name('visitors');

    // Users & Permissions
    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/users/create/', 'UsersController@create')->name('usersCreate');
    Route::post('/users/store', 'UsersController@store')->name('usersStore');
    Route::get('/users/{id}/edit', 'UsersController@edit')->name('usersEdit');
    Route::post('/users/{id}/update', 'UsersController@update')->name('usersUpdate');
    Route::get('/users/destroy/{id}', 'UsersController@destroy')->name('usersDestroy');
    Route::post('/users/updateAll', 'UsersController@updateAll')->name('usersUpdateAll');

    Route::get('/users/permissions/create/', 'UsersController@permissions_create')->name('permissionsCreate');
    Route::post('/users/permissions/store', 'UsersController@permissions_store')->name('permissionsStore');
    Route::get('/users/permissions/{id}/edit', 'UsersController@permissions_edit')->name('permissionsEdit');
    Route::post('/users/permissions/{id}/update', 'UsersController@permissions_update')->name('permissionsUpdate');
    Route::get('/users/permissions/destroy/{id}', 'UsersController@permissions_destroy')->name('permissionsDestroy');


    // Menus
    Route::post('/menus/store/parent', 'MenusController@storeMenu')->name('parentMenusStore');
    Route::get('/menus/parent/{id}/edit', 'MenusController@editMenu')->name('parentMenusEdit');
    Route::post('/menus/{id}/update/{ParentMenuId}', 'MenusController@updateMenu')->name('parentMenusUpdate');
    Route::get('/menus/parent/destroy/{id}', 'MenusController@destroyMenu')->name('parentMenusDestroy');

    Route::get('/menus/{ParentMenuId?}', 'MenusController@index')->name('menus');
    Route::get('/menus/create/{ParentMenuId?}', 'MenusController@create')->name('menusCreate');
    Route::post('/menus/store/{ParentMenuId?}', 'MenusController@store')->name('menusStore');
    Route::get('/menus/{id}/edit/{ParentMenuId?}', 'MenusController@edit')->name('menusEdit');
    Route::post('/menus/{id}/update', 'MenusController@update')->name('menusUpdate');
    Route::get('/menus/destroy/{id}', 'MenusController@destroy')->name('menusDestroy');
    Route::post('/menus/updateAll', 'MenusController@updateAll')->name('menusUpdateAll');
    
    //sales bills (pos)
    // Route::resource('sales-bills', 'SalesBillsController');
    Route::get('/sales-bills', 'SalesBillsController@index')->name('sales-bills');
    Route::get('/sales-bills/orderlist/{duration?}', 'SalesBillsController@orderList')->name('sb-orderlist');

    Route::get('/sales-bills/invoice/{id}', 'SalesBillsController@invoice')->name('sb-invoice');

    Route::get('/sales-bills/create', 'SalesBillsController@createView')->name('sales-bills-create');
    Route::get('/sales-bills/{id}/edit','SalesBillsController@updateView')->name('sales-bills-edit');
    Route::get('/sales-bills/all', 'SalesBillsController@getAll');
    Route::post('/sales-bills/create','SalesBillsController@createAction');
    Route::get('/sales-bills/getOrder/{id}', 'SalesBillsController@getOrder');
    Route::put('/sales-bills/updateAction/{id}', 'SalesBillsController@updateAction');
    Route::delete('/sales-bills/removeItemFromOrder/{id}', 'SalesBillsController@removeItemFromOrder');
    Route::delete('/sales-bills/destroy/{id}', 'SalesBillsController@destroy');
    Route::delete('sales-bills/destroy/all', 'SalesBillsController@multi_delete');



    //daily sales bills (pos)
    // Route::resource('sales-bills', 'SalesBillsController');
    Route::get('/daily-sales-bills', 'DailySalesBillsController@index')->name('daily-sales-bills');
    Route::get('/daily-sales-bills/orderlist/{duration?}', 'DailySalesBillsController@orderList')->name('dsb-orderlist');
    Route::get('/daily-sales-bills/invoice/{id}', 'DailySalesBillsController@invoice')->name('daily-sb-invoice');
    Route::get('/daily-sales-bills/create', 'DailySalesBillsController@createView')->name('daily-sales-bills-create');
    Route::get('/daily-sales-bills/{id}/edit','DailySalesBillsController@updateView')->name('daily-sales-bills-edit');
    Route::get('/daily-sales-bills/all', 'DailySalesBillsController@getAll');
    Route::post('/daily-sales-bills/create','DailySalesBillsController@createAction');
    Route::get('/daily-sales-bills/getOrder/{id}', 'DailySalesBillsController@getOrder');
    Route::put('/daily-sales-bills/updateAction/{id}', 'DailySalesBillsController@updateAction');
    Route::delete('/daily-sales-bills/removeItemFromOrder/{id}', 'DailySalesBillsController@removeItemFromOrder');
    Route::delete('/daily-sales-bills/destroy/{id}', 'DailySalesBillsController@destroy');
    Route::delete('daily-sales-bills/destroy/all', 'DailySalesBillsController@multi_delete');

    //Buy Orders   
    Route::get('/buy-orders', 'BuyOrdersController@index')->name('buy-orders');
    Route::get('/buy-orders/create', 'BuyOrdersController@createView')->name('buy-orders-create');
    Route::get('/buy-orders/{id}/edit','BuyOrdersController@updateView')->name('buy-orders-edit');
    Route::get('/buy-orders/all', 'BuyOrdersController@getAll');
    Route::post('/buy-orders/create','BuyOrdersController@createAction');
    Route::get('/buy-orders/getOrder/{id}', 'BuyOrdersController@getOrder');
    Route::put('/buy-orders/updateAction/{id}', 'BuyOrdersController@updateAction');
    Route::delete('/buy-orders/removeItemFromOrder/{id}', 'BuyOrdersController@removeItemFromOrder');
    Route::delete('/buy-orders/destroy/{id}', 'BuyOrdersController@destroy');
    Route::get('/buy-orders/invoice/{id}', 'BuyOrdersController@invoice')->name('bo-invoice');
    Route::get('/buy-orders/orderlist/{duration?}','BuyOrdersController@orderList')->name('bo-orderlist');
    
    //pruchases orders
    Route::get('/purchases-orders', 'PurchaseOrdersController@index')->name('purchases-orders');
    Route::get('/purchases-orders/create', 'PurchaseOrdersController@createView')->name('purchases-orders-create');
    Route::get('/purchases-orders/{id}/edit','PurchaseOrdersController@updateView')->name('purchases-orders-edit');

    Route::get('/purchases-orders/all', 'PurchaseOrdersController@getAll');
    Route::post('/purchases-orders/create','PurchaseOrdersController@createAction');
    Route::get('/purchases-orders/getOrder/{id}', 'PurchaseOrdersController@getOrder');
    Route::put('/purchases-orders/updateAction/{id}', 'PurchaseOrdersController@updateAction');
    Route::delete('/purchases-orders/removeItemFromOrder/{id}', 'PurchaseOrdersController@removeItemFromOrder');
    Route::delete('/purchases-orders/destroy/{id}', 'PurchaseOrdersController@destroy');
    Route::get('/purchases-orders/orderlist/{duration?}','PurchaseOrdersController@orderList')->name('po-orderlist');
    Route::get('/purchases-orders/invoice/{id}', 'PurchaseOrdersController@invoice')->name('po-invoice');

    //sales orders
    Route::get('/sales-orders', 'SalesOrdersController@index')->name('sales-orders');
    Route::get('/sales-orders/create', 'SalesOrdersController@createView')->name('sales-orders-create');
    Route::get('/sales-orders/{id}/edit','SalesOrdersController@updateView')->name('sales-orders-edit');

    Route::get('/sales-orders/all', 'SalesOrdersController@getAll');
    Route::post('/sales-orders/create','SalesOrdersController@createAction');
    Route::get('/sales-orders/getOrder/{id}', 'SalesOrdersController@getOrder');
    Route::put('/sales-orders/updateAction/{id}', 'SalesOrdersController@updateAction');
    Route::delete('/sales-orders/removeItemFromOrder/{id}', 'SalesOrdersController@removeItemFromOrder');
    Route::get('/sales-orders/invoice/{id}','SalesOrdersController@invoice')->name('so-invoice');
    Route::get('/sales-orders/orderlist/','SalesOrdersController@orderlist')->name('so-orderlist');
    
    //products
    Route::resource('products', 'ProductsController');
    Route::get('/products', 'ProductsController@index')->name('products');
    Route::get('/tries', 'ProductsController@alltries')->name('tries');


    // Route::get('/products/create/', 'ProductsController@create')->name('productsCreate');
    // Route::post('/products/store', 'ProductsController@store')->name('productsStore');
    Route::post('upload/image/{id}','ProductsController@upload_file');
    Route::post('delete/image','ProductsController@delete_file');
     Route::get('load/image','ProductsController@getImg')->name('products.img.load'); 
     Route::get('search/image','ProductsController@getSearch')->name('products.img.search');
    
    Route::post('update/image/{id}','ProductsController@update_product_image');
    Route::post('delete/product/image/{id}','ProductsController@delete_main_image');
    
    Route::post('load/weight/size','ProductsController@prepare_weight_size');

    Route::delete('products/destroy/all', 'ProductsController@multi_delete');

    //Product Stocks
    // Route::resource('productStocks', 'ProductStocksController');
    Route::get('/allNonRegProductStocks', 'ProductStocksController@allNonRegProductStocks')->name('allNonRegProductStocks');
    Route::get('/allRegProductStocks', 'ProductStocksController@allRegProductStocks')->name('allRegProductStocks');
    // Route::get('/allRegProductStocks/{id}', 'ProductStocksController@allRegProductStocks');

    Route::get('/productStocks/{id}', 'ProductStocksController@x');

    Route::get('/productStocks/create', 'ProductStocksController@createView')->name('product-stock-create');
    Route::post('/productStocks/create','ProductStocksController@createAction');
    //manufacturers
    Route::resource('manufacturers', 'ManufacturersController');
    Route::get('/manufacturers', 'ManufacturersController@index')->name('manufacturers');
    Route::get('/manufacturers/create', 'ManufacturersController@create')->name('manufacturersCreate');
    Route::delete('manufacturers/destroy/all', 'ManufacturersController@multi_delete');
   
    //sizes
    Route::resource('sizes', 'SizesController');
    Route::get('/sizes', 'SizesController@index')->name('sizes');
	Route::delete('sizes/destroy/all', 'SizesController@multi_delete');

    //weights
    Route::resource('weights', 'WeightsController');
    Route::get('/weights', 'WeightsController@index')->name('weights');
    Route::delete('weights/destroy/all', 'WeightsController@multi_delete');

    
    //colors
    Route::resource('colors', 'ColorsController');
    Route::get('/colors', 'ColorsController@index')->name('colors');
    Route::delete('colors/destroy/all', 'ColorsController@multi_delete');
  
    //fills
   Route::resource('fills', 'FillsController');
   Route::get('/fills', 'FillsController@index')->name('fills');
   Route::delete('fills/destroy/all', 'FillsController@multi_delete');

   //Clients
   Route::resource('clients', 'ClientsController');
   Route::get('/clients', 'ClientsController@index')->name('clients');
   Route::delete('clients/destroy/all', 'ClientsController@multi_delete');

        
    //department
    Route::resource('departments', 'DepartmentsController');
    Route::get('/departments', 'DepartmentsController@index')->name('departments');
    
    //countries
    Route::resource('countries', 'CountriesController');
    Route::delete('countries/destroy/all', 'CountriesController@multi_delete');
    
    //cities
    Route::resource('cities', 'CitiesController');
    Route::delete('cities/destroy/all', 'CitiesController@multi_delete');
    
    //staties
    Route::resource('states', 'StatesController');
    Route::delete('states/destroy/all', 'StatesController@multi_delete');
  
    //currencies
    Route::resource('currencies', 'CurrenciesController');
    Route::delete('currencies/destroy/all', 'CurrenciesController@multi_delete');
    
    //trademarks
    Route::resource('trademarks', 'TradeMarksController');
    Route::delete('trademarks/destroy/all', 'TradeMarksController@multi_delete');

    //event_coordinators
    Route::resource('event_coordinators', 'EventCoordinatorsController');
    Route::delete('event_coordinators/destroy/all', 'EventCoordinatorsController@multi_delete');

    // Clear Cache
    Route::get('/cache-clear', function () {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        return redirect()->route('adminHome')->with('doneMessage', trans('backLang.cashClearDone'));
    })->name('cacheClear');
});

// .. End of Backend Routes

// RESTful API routes
Route::Group(['prefix' => '/api/v1'], function () {
    Route::get('/', 'APIsController@api');
    // general
    Route::get('/website/status', 'APIsController@website_status');
    Route::get('/website/info/{lang?}', 'APIsController@website_info');
    Route::get('/website/contacts/{lang?}', 'APIsController@website_contacts');
    Route::get('/website/style/{lang?}', 'APIsController@website_style');
    Route::get('/website/social', 'APIsController@website_social');
    Route::get('/website/settings', 'APIsController@website_settings');
    Route::get('/menu/{menu_id}/{lang?}', 'APIsController@menu');
    Route::get('/banners/{group_id}/{lang?}', 'APIsController@banners');
    // section & topics
    Route::get('/section/{section_id}/{lang?}', 'APIsController@section');
    Route::get('/categories/{section_id}/{lang?}', 'APIsController@categories');
    Route::get('/topics/{section_id}/page/{page_number?}/count/{topics_count?}/{lang?}', 'APIsController@topics');
    // topic sub details
    Route::get('/topic/fields/{topic_id}/{lang?}', 'APIsController@topic_fields');
    Route::get('/topic/photos/{topic_id}/{lang?}', 'APIsController@topic_photos');
    Route::get('/topic/photo/{photo_id}/{lang?}', 'APIsController@topic_photo');
    Route::get('/topic/maps/{topic_id}/{lang?}', 'APIsController@topic_maps');
    Route::get('/topic/map/{map_id}/{lang?}', 'APIsController@topic_map');
    Route::get('/topic/files/{topic_id}/{lang?}', 'APIsController@topic_files');
    Route::get('/topic/file/{file_id}/{lang?}', 'APIsController@topic_file');
    Route::get('/topic/comments/{topic_id}/{lang?}', 'APIsController@topic_comments');
    Route::get('/topic/comment/{comment_id}/{lang?}', 'APIsController@topic_comment');
    Route::get('/topic/related/{topic_id}/{lang?}', 'APIsController@topic_related');
    // topic page
    Route::get('/topic/{topic_id}/{lang?}', 'APIsController@topic');
    // user topics
    Route::get('/user/{user_id}/topics/page/{page_number?}/count/{topics_count?}/{lang?}', 'APIsController@user_topics');
    // Forms Submit
    Route::post('/subscribe', 'APIsController@subscribeSubmit');
    Route::post('/comment', 'APIsController@commentSubmit');
    Route::post('/order', 'APIsController@orderSubmit');
    Route::post('/contact', 'APIsController@ContactPageSubmit');
});
// .. End of RESTful API route


//////////////// Shop front end Routes

Route::get('/shop/index', 'ShopController@home')->name('shop.home');



Route::get('/shop/checkout', 'ShopController@checkout')->name('shop.checkout.create');

Route::get('/shop/soon', 'ShopController@soonPage')->name('shop.soon.index');

//// shop product
Route::get('/shop/product-compare', 'ShopController@compareProduct')->name('shop.product.compare');

Route::get('/shop/product-index', 'ShopController@indexProduct')->name('shop.product.index');

Route::get('/shop/product-show', 'ShopController@showProduct')->name('shop.product.show');

//// end shop product



// Cart shop

Route::get('/shop/cart/index', 'ShopController@cartIndex')->name('shop.cart.index');

Route::get('shop/cart/store', 'ShopController@cartStore')->name('shop.cart.store');

Route::get('shop/cart/delete', 'ShopController@cartDelete')->name('shop.cart.delete');


Route::get('shop/cart/update', 'ShopController@cartUpdate')->name('shop.cart.update');

// end Cart shop 





//////////////// Shop front end Routes

/// Auth client
Route::get('/shop/user/login', 'ShopController@loginView')->name('shop.user.login');

Route::post('/shop/user/login', 'ShopController@login')->name('shop.user.login');


Route::get('/shop/user/register', 'ShopController@registerView')->name('shop.user.register');

Route::post('/shop/user/register', 'ShopController@register')->name('shop.user.register');

/// End Auth client

Route::get('/shop/user/profile', 'ShopController@profile')->name('shop.user.show');


Route::get('/shop/blog', 'ShopController@blog')->name('shop.blog');

Route::get('/shop/about', 'ShopController@about')->name('shop.about');

Route::get('/shop/contact', 'ShopController@contact')->name('shop.contact');





//////////////// End Shop front end Routes

// Frontend Routes
Route::get('/', 'FrontendHomeController@HomePage')->name('Home');



// ../site map
Route::get('/sitemap.xml', 'SiteMapController@siteMap')->name('siteMap');
Route::get('/{lang}/sitemap', 'SiteMapController@siteMap')->name('siteMapByLang');

Route::get('/home', 'FrontendHomeController@HomePage')->name('Home');
// // ../home url
// Route::get('/home', 'FrontendHomeController@HomePage')->name('HomePage');
// Route::get('/{lang?}/home', 'FrontendHomeController@HomePageByLang')->name('HomePageByLang');
// // ../subscribe to newsletter submit  (ajax url)
// Route::post('/subscribe', 'FrontendHomeController@subscribeSubmit')->name('subscribeSubmit');
// // ../Comment submit  (ajax url)
// Route::post('/comment', 'FrontendHomeController@commentSubmit')->name('commentSubmit');
// // ../Order submit  (ajax url)
// Route::post('/order', 'FrontendHomeController@orderSubmit')->name('orderSubmit');
// // ..Custom URL for contact us page ( www.site.com/contact )
// Route::get('/contact', 'FrontendHomeController@ContactPage')->name('contactPage');
// Route::get('/{lang?}/contact', 'FrontendHomeController@ContactPageByLang')->name('contactPageByLang');
// // ../contact message submit  (ajax url)
// Route::post('/contact/submit', 'FrontendHomeController@ContactPageSubmit')->name('contactPageSubmit');
// // ..if page by name ( ex: www.site.com/about )
// // Route::get('/topic/{id}', 'FrontendHomeController@topic')->name('FrontendPage');
// // ..if page by user id ( ex: www.site.com/user )
// Route::get('/user/{id}', 'FrontendHomeController@userTopics')->name('FrontendUserTopics');
// Route::get('/{lang?}/user/{id}', 'FrontendHomeController@userTopicsByLang')->name('FrontendUserTopicsByLang');
// // ../search
// Route::post('/search', 'FrontendHomeController@searchTopics')->name('searchTopics');

// // ..Topics url  ( ex: www.site.com/news/topic/32 )
// Route::get('/{section}/topic/{id}', 'FrontendHomeController@topic')->name('FrontendTopic');
// Route::get('/{lang?}/{section}/topic/{id}', 'FrontendHomeController@topicByLang')->name('FrontendTopicByLang');

// // ..Sub category url for Section  ( ex: www.site.com/products/2 )
// Route::get('/{section}/{cat}', 'FrontendHomeController@topics')->name('FrontendTopicsByCat');
// Route::get('/{lang?}/{section}/{cat}', 'FrontendHomeController@topicsByLang')->name('FrontendTopicsByCatWithLang');

// // ..Section url by name  ( ex: www.site.com/news )
// Route::get('/{section}', 'FrontendHomeController@topics')->name('FrontendTopics');
// Route::get('/{lang?}/{section}', 'FrontendHomeController@topicsByLang')->name('FrontendTopicsByLang');

// // ..SEO url  ( ex: www.site.com/title-here )
// Route::get('/{seo_url_slug}', 'FrontendHomeController@SEO')->name('FrontendSEO');
// Route::get('/{lang?}/{seo_url_slug}', 'FrontendHomeController@SEOByLang')->name('FrontendSEOByLang');

// ..if page by name and language( ex: www.site.com/ar/about )
// Route::get('/{lang?}/topic/{id}', 'FrontendHomeController@topicByLang')->name('FrontendPageByLang');

// .. End of Frontend Route
/*
 !! Important note:
    For new routes add them before // Frontend Routes
    If you added them after Frontend Routes they wouldn't work.
 */


Route::group(['middleware' => 'Website'], function () {

    Route::get('/', function () {
            return view('website.home');
        });
        Route::get('/contact', function () {
        	return view('website.contact');
        });
            Route::get('/about', function () {
            return view('website.about');
        });
        Route::get('/contact', 'ContactUSController@contactUS');
      Route::post('/contact', ['as'=>'contactus.store','uses'=>'ContactUSController@contactUSPost']);
      
          Route::get('/reviews', 'ReviewsController@review');
      Route::post('/reviews', ['as'=>'reviewStore.store','uses'=>'ReviewsController@reviewStore']);
        
});

// Route::get('website', function () {

//     if (Helper::setting()->site_status == '0') {
//         return redirect('/');
//     }

//     return view('style.maintenance');
// });


Route::get('{vue?}', 'PageController@index')->where('vue', '[\/\w\.-]*');
