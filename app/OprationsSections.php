<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OprationsSections extends Model
{
    
    protected $fillable = [
        'name_ar',
        'name_en'
    ];

 

    public function productStocks()
    {
        return $this->hasMany('App\ProductsStocks','pro_stocks_id');
    }
    public function op_section()
    {
  
        return $this->belongsTo('App\ProductsStocks', 'opration_sec_id');
    }
    
}