<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Website extends Model {
	//

	protected $table    = 'websites';
	protected $fillable = [
	
		'video_thamnail',
		'video_url',
		'email1',
		'email2',
		'email3',
		'email4',
		'number1',
		'number2',
		'number3',
		'number4',
		'number5',
		'location1',
		'location2',
		'location3',
		'location4',
		'location5',
		'instagram',
		'youtube',
		'snapchat',
		'twitter',
		'footer_ar',
		'footer_en',
		'about_title_ar',
		'about_title_en',
		'about_bodys_ar',
		'about_bodys_en',
		'about_bodyb_ar',
		'about_bodyb_en',
		'extra',

		
	];

}
