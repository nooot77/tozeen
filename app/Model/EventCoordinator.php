<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EventCoordinator extends Model
{
  protected $table    = 'event_coordinators';
  protected $fillable = [
      'name',
      'office',
      'phone',
      'commission',
      'city_id',
      'b_city_id',
      'start_date',
      'end_date',

  ];

  public function city_id() {
    return $this->hasOne('App\Model\City', 'id', 'city_id');
  }
    public function b_city_id() {
    return $this->hasOne('App\Model\City', 'id', 'b_city_id');
  }
}
