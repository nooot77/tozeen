<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{
    
public $table = 'reviews';
 
public $fillable = [
    'name',
    'message',
    'phone',
    'status'
];

}
