<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model {

	protected $table    = 'countries';
	protected $fillable = [
		'title_ar',
		'title_en',
		'tel',
		'code',
		// 'currency',
		// 'logo',
	];

}
