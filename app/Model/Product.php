<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   protected $table = 'products';
   protected $fillable = [
       'title',
       'photo',
       'content',
       'code',
       'fill_id',
       'department_id',
       'trade_id',
       'manu_id',
       'country_id',
       'color_id',
       'size',
       'size_id',
       'weight',
       'weight_id',
       'currency_id',
       'price',
       'price_offer',
       'stock',
       'other_data',
       'status',
       'reason',
       'start_at',
       'end_at',
       'start_offer_at',
       'end_offer_at',
       'cal',
       'prd_date',
       'exp_date',
   ];
   public function files()
   {
       return $this->hasMany('App\File','relation_id','id')->where('file_type','product');
   }
      public function department_id()
   {
     return $this->hasOne('App\Model\Department','id','department_id');
   }

   	public function fill_id() {
		return $this->hasOne('App\Model\Fill', 'id', 'fill_id');
  }
  
  public function pro_quantatiy()
  {

      return $this->belongsTo('App\ProductsStocks', 'pro_stock_id');
  }
  
  



}
