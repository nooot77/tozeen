<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TradeMark extends Model {
	use SoftDeletes;


	protected $dates = ['deleted_at'];
	protected $table    = 'trade_marks';
	protected $fillable = [
		'name_ar',
		'name_en',
		'logo',
	];
}
