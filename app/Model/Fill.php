<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fill extends Model
{

  protected $table    = 'fills';
  protected $fillable = [
     'name_ar',
     'name_en',
  ];

}
