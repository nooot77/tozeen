<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model {

	protected $table    = 'currencies';
	protected $fillable = [
		'name',
		'code',
		'decimal_place',
		
	];

}
