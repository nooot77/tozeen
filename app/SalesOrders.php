<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrders extends Model
{
    public function toArray(){
        $order = Parent::toArray();
        $order['order_items'] = $this->orderItems;
        return $order;
    }

    protected $fillable = [
        'user_id',
        'client_id',
        'number',
        'reference',
        'date',
        'total',
        'city_id',
        'currency_id',
        'payment_method',
        'terms',
        'status_id',
        'is_received',
    ];

    public function orderItems()
    {
        return $this->hasMany('App\SalesOrdersItems','sales_order_id');
    }
}