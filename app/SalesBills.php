<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesBills extends Model
{
    public function toArray(){
        $order = Parent::toArray();
        $order['order_items'] = $this->orderItems;
        return $order;
    }

    protected $fillable = [
        'number',
        'branch_id',
        'user_id',
        'city_id',
        'mobile',
        'event_date',
        'client_name',
        'traies',
        'notes',
        'total',
        'paid',
        'insurance',
        'residual',
        'delivery_date',
        'payment_status',
        'ads_status',
        'delivery_status',
        

    ];

    public function orderItems()
    {
        return $this->hasMany('App\SalesBillsItems','sales_bills_id');
    }

    #bi
    public function bi() {
        return $this->belongsTo('App\OprationsSections',  'branch_id');
    }

    #oc
    public function oc() {
        return $this->belongsTo('App\OprationsSections',     'city_id');
    }


    public function ec() {
        return $this->belongsTo('App\Model\EventCoordinator', 'ec_id');
    }



    
}