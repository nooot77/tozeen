<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesBillsItems extends Model
{
    protected $fillable = [
        'sales_bills_id',
        'product_id',
        'vendor_reference',
        'qty',
        'qty_received',
        'free_kilos',
        'unit_price',
        'discount',
    ];

    public function product() {
        return $this->belongsTo('App\Model\Product');
    }
}