<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrdersItems extends Model
{
    protected $fillable = [
        'purchase_order_id',
        'product_id',
        'vendor_reference',
        'qty',
        'qty_received',
        'free_kilos',
        'unit_price',
        'subtotal',
        'discount',
        'total',
    ];

    public function product() {
        return $this->belongsTo('App\Model\Product','product_id');
    }
}