<?php

namespace App\DataTables;
use Auth;
use DB;
use App\Model\City;
use App\BuyOrders;
use Yajra\DataTables\Services\DataTable;
use Helper;
class BuyOrdersDatatable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query) {
		return datatables($query)
			->addColumn('checkbox', 'backEnd.buyorders.btn.checkbox')
			->addColumn('edit', 'backEnd.buyorders.btn.edit')
			->addColumn('delete', 'backEnd.buyorders.btn.delete')
            ->addColumn('print','backEnd.buyorders.btn.print')
			->rawColumns([
				'edit',
				'delete',
				'checkbox',
                'print'
			]);
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\User $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query() {
		$src = Auth::user()->permissionsGroup->opration_sec_id;

		if($src == 10 || $src == 7 )
		return BuyOrders::query()->with('branch_id')->select('buy_orders.*');
		if($src == 3 ||$src == 4 ||$src == 5 ||$src == 6 ||$src == 8)
		return BuyOrders::query()->with('branch_id')->where('city_id','=',$src)->select('buy_orders.*');
		// $userId = Auth::user()->id;
		// dd(BuyOrders::all()->where('branch_id','=',$src)->toArray());exit;
		// $buyOrderUser = BuyOrders::where("user_id", "=", $user->id)->get();
	// 	dd(DB::table('buy_orders')
	// 	->join('oprations_sections', 'buy_orders.branch_id', '=', 'oprations_sections.id')
	//    ->select('buy_orders.*','oprations_sections.name_ar')->get());exit;
		$cities = City::all(['id','city_name_ar','city_name_en'])->toArray();

		//  if($src == 10 || $src == 7 )
		//  return DB::table('buy_orders')
		//   ->join('oprations_sections', 'buy_orders.branch_id', '=', 'oprations_sections.id')
		//  ->select('buy_orders.*','oprations_sections.name_ar')->get();
		//  if($src == 3 ||$src == 4 ||$src == 5 ||$src == 6 ||$src == 8)
		    
		// 	return DB::table('buy_orders')->where('city_id','=',$src)
		//  	->join('oprations_sections', 'buy_orders.city_id', '=', 'oprations_sections.id')
		// 	->select('buy_orders.*','oprations_sections.name_ar')->get();
		 
		// }else{
		// 	return redirect(Helper::aurl('/'));
		// }
		

	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html() {
		return $this->builder()
		            ->columns($this->getColumns())
			->minifiedAjax()
			->parameters([
				'dom'        => 'Blfrtip',
				'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, trans('admin.all_record')]],
				'buttons'    => [
					[
						'text' => '<i class="fa fa-plus"></i> '.trans('admin.add'), 'className' => 'btn btn-info', "action" => "function(){

							window.location.href = '".\URL::current()."/create';
						}"],

					['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
					['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.trans('admin.ex_csv')],
					['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.trans('admin.ex_excel')],
					['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
					[
						'text' => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-danger delBtn'],

				],
				'initComplete' => " function () {
		            this.api().columns([1,2,3,4,5,6,7,8,9,10]).every(function () {
		                var column = this;
		                var input = document.createElement(\"input\");
		                $(input).appendTo($(column.footer()).empty())
		                .on('keyup', function () {
		                    column.search($(this).val(), false, false, true).draw();
		                });
		            });
		        }",

				'language' => Helper::datatable_lang(),

			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns() {
		return [
			[
				'name'       => 'checkbox',
				'data'       => 'checkbox',
				'title'      => '<input type="checkbox" id="checkAll" class="check_all" onclick="check_all()" />',
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			]
			// , [
			// 	'name'  => 'id',
			// 	'data'  => 'id',
			// 	'title' => '#',
			// ]
			, [
				'name'  => 'number',
				'data'  => 'number',
				'title' => trans('admin.Number'),
			], [
				'name'  => 'client_name',
				'data'  => 'client_name',
				'title' => trans('admin.client_name'),
			]
			, [
				'name'  => 'mobile',
				'data'  => 'mobile',
				'title' => trans('admin.mobile'),
			]
			, [
				'name'  => 'branch_id.name_ar',
				'data'  => 'branch_id.name_ar',
				'title' => trans('admin.branch'),
			]
			, [
				'name'  => 'traies',
				'data'  => 'traies',
				'title' => trans('admin.traies'),
			]
			, [
				'name'  => 'total',
				'data'  => 'total',
				'title' => trans('admin.total'),
			]
			, [
				'name'  => 'paid',
				'data'  => 'paid',
				'title' => trans('admin.paid'),
			]
			, [
				'name'  => 'event_date',
				'data'  => 'event_date',
				'title' => trans('admin.event_date'),
			]
			, [
				'name'  => 'delivery_date',
				'data'  => 'delivery_date',
				'title' => trans('admin.delivery_date'),
			]
			, [
				'name'  => 'created_at',
				'data'  => 'created_at',
				'title' => trans('admin.created_at'),
			]
			, [
				'name'       => 'edit',
				'data'       => 'edit',
				'title'      => trans('admin.edit'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			],
            [
				'name'       => 'delete',
				'data'       => 'delete',
				'title'      => trans('admin.delete'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			],[
                'name' =>'print',
                'data' => 'print',
                'title' => 'طباعة',
                'exportable' => false,
                'printable'  => false,
                'orderable'  => false,
                'searchable' => false,
            ]




		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'BuyOrders'.date('YmdHis');
	}
}
