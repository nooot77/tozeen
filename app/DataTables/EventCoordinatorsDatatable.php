<?php

namespace App\DataTables;

use App\Model\EventCoordinator;
use Yajra\DataTables\Services\DataTable;
use Helper;
class EventCoordinatorsDatatable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query) {
		return datatables($query)
			->addColumn('checkbox', 'admin.event_coordinators.btn.checkbox')
			->addColumn('edit', 'admin.event_coordinators.btn.edit')
			->addColumn('delete', 'admin.event_coordinators.btn.delete')
			->rawColumns([
				'edit',
				'delete',
				'checkbox',
			]);
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param \App\User $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query() {
		return EventCoordinator::query()->with('city_id','b_city_id')->select('event_coordinators.*');
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html() {
		return $this->builder()
		            ->columns($this->getColumns())
			->minifiedAjax()
			->parameters([
				'dom'        => 'Blfrtip',
				'lengthMenu' => [[10, 25, 50, 100], [10, 25, 50, trans('admin.all_record')]],
				'buttons'    => [
					[
						'text' => '<i class="fa fa-plus"></i> '.trans('admin.add'), 'className' => 'btn btn-info', "action" => "function(){

							window.location.href = '".\URL::current()."/create';
						}"],

					['extend' => 'print', 'className' => 'btn btn-primary', 'text' => '<i class="fa fa-print"></i>'],
					['extend' => 'csv', 'className' => 'btn btn-info', 'text' => '<i class="fa fa-file"></i> '.trans('admin.ex_csv')],
					['extend' => 'excel', 'className' => 'btn btn-success', 'text' => '<i class="fa fa-file"></i> '.trans('admin.ex_excel')],
					['extend' => 'reload', 'className' => 'btn btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
					[
						'text' => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-danger delBtn'],

				],
				'initComplete' => " function () {
		            this.api().columns([2,3]).every(function () {
		                var column = this;
		                var input = document.createElement(\"input\");
		                $(input).appendTo($(column.footer()).empty())
		                .on('keyup', function () {
		                    column.search($(this).val(), false, false, true).draw();
		                });
		            });
		        }",

				'language' => Helper::datatable_lang(),

			]);
	}

	//
	// 'name'      => 'required',
	// 'office'      => 'required',
	// 'phone'       => 'required|numeric',
	// 'commission'   => 'required|numeric',
	// 'city_id'   => 'required|numeric',
	// 'start_date'      => 'sometimes|nullable',
	// 'end_date'
	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns() {
		return [
			[
				'name'       => 'checkbox',
				'data'       => 'checkbox',
				'title'      => '<input type="checkbox"  id="checkAll" class="check_all" onclick="check_all()" />',
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			], [
				'name'  => 'id',
				'data'  => 'id',
				'title' => '#',
			], [
				'name'  => 'name',
				'data'  => 'name',
				'title' => trans('admin.name'),
			], [
				'name'  => 'office',
				'data'  => 'office',
				'title' => trans('admin.office'),
			], [
				'name'  => 'phone',
				'data'  => 'phone',
				'title' => trans('admin.phone'),
			], [
				'name'  => 'city_id.city_name_'.session('locale'),
				'data'  => 'city_id.city_name_'.session('locale'),
				'title' => trans('admin.city_id'),
			],
			 [
				'name'  => 'b_city_id.city_name_'.session('locale'),
				'data'  => 'b_city_id.city_name_'.session('locale'),
				'title' => trans('admin.b_city_id'),
			], [
				'name'  => 'commission',
				'data'  => 'commission',
				'title' => trans('admin.commission'),
			], [
				'name'  => 'start_date',
				'data'  => 'start_date',
				'title' => trans('admin.start_date'),
			],
			[
			 'name'  => 'end_date',
			 'data'  => 'end_date',
			 'title' => trans('admin.end_date'),
		 ], [
				'name'       => 'edit',
				'data'       => 'edit',
				'title'      => trans('admin.edit'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			], [
				'name'       => 'delete',
				'data'       => 'delete',
				'title'      => trans('admin.delete'),
				'exportable' => false,
				'printable'  => false,
				'orderable'  => false,
				'searchable' => false,
			],

		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'event_coordinators_'.date('YmdHis');
	}
}
