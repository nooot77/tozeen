<?php

namespace App\DataTables;
use Helper;
use App\ProductsStocks;
use Yajra\DataTables\Services\DataTable;

class ProductStocksNonRegisteredDatatable extends DataTable
{
   /**
    * Build DataTable class.
    *
    * @param mixed $query Results from query() method.
    * @return \Yajra\DataTables\DataTableAbstract
    */
   public function dataTable($query)
   {
      return datatables($query)
         ->addColumn('checkbox', 'admin.productStocks.btn.checkbox')
         ->addColumn('edit', 'admin.productStocks.btn.edit')
         ->addColumn('delete', 'admin.productStocks.btn.delete')
         ->rawColumns([
            'edit',
            'delete',
            'product',
            'checkbox',
         ]);
   }

   /**
    * Get query source of dataTable.
    *
    * @param \App\User $model
    * @return \Illuminate\Database\Eloquent\Builder
    */

   public function query()
   {
      return ProductsStocks::query()->where('first_quantity','<=','0')->with('os_name','product_name')->select('products_stocks.*');
   }

   /**
    * Optional method if you want to use html builder.
    *
    * @return \Yajra\DataTables\Html\Builder
    */
   public function html()
   {
      return $this->builder()
         ->columns($this->getColumns())
         ->minifiedAjax()
         ->parameters([
            'dom'          => 'Blfrtip',
            'lengthMenu'   => [[10, 25, 50, 100], [10, 25, 50, trans('admin.all_record')]],
            'buttons'      => [
               [
                  'text' => '<i class="fa fa-plus"></i> ' . trans('admin.add'), 'className' => 'btn btn-sm btn-info', "action" => "function(){

							window.location.href = '" . \URL::current() . "/create';
						}", ],

               ['extend' => 'print', 'className' => 'btn btn-sm btn-primary', 'text' => '<i class="fa fa-print"></i>'],
               ['extend' => 'csv', 'className' => 'btn btn-sm btn-info', 'text' => '<i class="fa fa-file"></i> ' . trans('admin.ex_csv')],
               ['extend' => 'excel', 'className' => 'btn btn-sm btn-success', 'text' => '<i class="fa fa-file"></i> ' . trans('admin.ex_excel')],
               ['extend' => 'reload', 'className' => 'btn btn-sm btn-default', 'text' => '<i class="fa fa-refresh"></i>'],
               [
                  'text' => '<i class="fa fa-trash"></i>', 'className' => 'btn btn-sm btn-danger delBtn'],

            ],
            'initComplete' => " function () {
		            this.api().columns([1,2,3]).every(function () {
		                var column = this;
		                var input = document.createElement(\"input\");
		                $(input).appendTo($(column.footer()).empty())
		                .on('keyup', function () {
		                    column.search($(this).val(), false, false, true).draw();
		                });
		            });
		        }",

            'language'     => Helper::datatable_lang(),

         ]);
   }

   /**
    * Get columns.
    *
    * @return array
    */
   protected function getColumns()
   {
      return [
         [
            'name'       => 'checkbox',
            'data'       => 'checkbox',
            'title'      => '<input type="checkbox" class="check_all" id="checkAll" onclick="check_all()" />',
            'exportable' => false,
            'printable'  => false,
            'orderable'  => false,
            'searchable' => false,
         ], [
            'name'  => 'os_name.name_ar',
            'data'  => 'os_name.name_ar',
            'title' => trans('admin.branch'),
         ],
         [
            'name'  => 'product_name.title',
            'data'  => 'product_name.title',
            'title' => trans('admin.product'),
         ],  [
            'name'  => 'first_quantity',
            'data'  => 'first_quantity',
            'title' => trans('admin.kiloQty'),
         ], 
         // [
         //    'name'  => 'exp_date',
         //    'data'  => 'exp_date',
         //    'title' => trans('admin.exp_date'),
         // ],  
         [
            'name'       => 'edit',
            'data'       => 'edit',
            'title'      => trans('admin.edit'),
            'exportable' => false,
            'printable'  => false,
            'orderable'  => false,
            'searchable' => false,
         ], [
            'name'       => 'delete',
            'data'       => 'delete',
            'title'      => trans('admin.delete'),
            'exportable' => false,
            'printable'  => false,
            'orderable'  => false,
            'searchable' => false,
         ],

      ];
   }

   /**
    * Get filename for export.
    *
    * @return string
    */
   protected function filename()
   {
      return 'productStocks_' . date('YmdHis');
   }
}
