<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrdersItems extends Model
{
    protected $fillable = [
        'sales_order_id',
        'product_id',
        'vendor_reference',
        'qty',
        'qty_received',
        'free_kilos',
        'unit_price',
        'discount',
    ];
}