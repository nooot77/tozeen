<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyOrdersItems extends Model
{
    protected $fillable = [
        'buy_orders_id',
        'product_id',
        'vendor_reference',
        'qty',
        'qty_received',
        'free_kilos',
        'unit_price',
        'discount',
    ];

    public function product() {
        return $this->belongsTo('App\Model\Product');
    }
}