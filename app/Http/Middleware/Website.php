<?php

namespace App\Http\Middleware;

use Closure;
use Helper;
class Website {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {

		if (Helper::setting()->status == '0') {
			return redirect('website');
		} else {
			return $next($request);
			
		}
	}
}