<?php

namespace App\Http\Controllers;

use App\DataTables\ClientsDatatable;
use App\Http\Controllers\Controller;
use App\Clients;
use Helper;
use View;
use App\WebmasterSection;
use Illuminate\Http\Request;
use Storage;


class ClientsController extends Controller
{
    public function __construct()
   {
       //$this->middleware('auth');

       // Check Permissions
       /*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
           return Redirect::to(route('NoPermission'))->send();
       }*/

       // Share GeneralWebmasterSections
       $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
      View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index(ClientsDatatable $query)
   {
      return $query->render('admin.clients.index', ['title' => trans('admin.clients')]);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      return view('admin.clients.create', ['title' => trans('admin.add')]);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store()
   {

      $data = $this->validate(request(),
         [
            'name_ar' => 'required',
            'name_en' => 'required',
            'color'   => 'required|string',

         ], [], [
            'name_ar' => trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'color'   => trans('admin.color'),
         ]);

      Color::create($data);
      session()->flash('success', trans('admin.record_added'));
      return redirect(Helper::aurl('colors'));
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      $color = Color::find($id);
      $title = trans('admin.edit');
      return view('admin.clients.edit', compact('color', 'title'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $r, $id)
   {

      $data = $this->validate(request(),
         [
            'name_ar' => 'required',
            'name_en' => 'required',
            'color'   => 'required|string',
         ], [], [
            'name_ar' => trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'color'   => trans('admin.color'),
         ]);

      Color::where('id', $id)->update($data);
      session()->flash('success', trans('admin.updated_record'));
      return redirect(Helper::aurl('colors'));
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      $colors = Color::find($id);
      $colors->delete();
      session()->flash('success', trans('admin.deleted_record'));
      return redirect(Helper::aurl('colors'));
   }

   public function multi_delete()
   {
      if (is_array(request('item'))) {
         foreach (request('item') as $id) {
            $colors = Color::find($id);
            $colors->delete();
         }
      } else {
         $colors = Color::find(request('item'));
         $colors->delete();
      }
      session()->flash('success', trans('admin.deleted_record'));
      return redirect(Helper::aurl('colors'));
   }

}
