<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Department;
use App\Model\Product;
use App\Model\Color;
use App\Model\Fill;
use App\User;
use App\Model\Cart;

class ShopController extends Controller
{
    public function home() {
    	$department = Department::all();
    	$products = Product::orderBy('id', 'asc')->take(20)->get();
    	$fills = Fill::all();

    	return view('shop.home', compact('department', 'products', 'fills'));
    }

    public function cartIndex(){

    	if(\Auth::check()){

    	return view('shop.cart');
    	}else{
    		return redirect()->back()->with('warning' , 'you have to regiaster to enter this page');
    	}
    }

    public function cartStore(Request $request){
    	 
    	if(\Auth::check()){
    		
    		$cartCount = Cart::where('product_id', $request->product_id)->where('user_id', \Auth::user()->id)->get();

    		// if product exist or not
    		if($cartCount->count() > 0){

    			$cart = Cart::where('product_id', $request->product_id)->where('user_id', \Auth::user()->id)->update(['quantity' => $cartCount[0]->quantity + 1]);

    		}else{

    			$cart = new Cart;
    		$cart->product_id = $request->product_id;
    		$cart->user_id = \Auth::user()->id;
    		$cart->quantity += 1 ;
    		$cart->save();
    		}
    		// end if product exist or not


    		$data = [
    			'result' => $request->product_id,
    			'count' => \Auth::user()->cart->sum('quantity'),

    			];
    	 return json_encode($data); 

    	 } else{
    	 	$data = ['result' => 'you cant add to cart before register'];
    	 	return json_encode($data); 
    		
    	}
    }

    public function cartDelete(Request $request){
    	Cart::find($request->id)->delete();
    	return redirect()->back()->with('success', 'deleted successfully');
    }

     public function cartUpdate(Request $request){
    	$cart = Cart::find($request->cart_id);

    	$cart->quantity = $request->cart_qty;

    	$cart->save();

    	
    }

    public function checkout(){
    	return view('shop.checkout');
    }


    public function soonPage(){
    	return view('shop.soon');
    }


    public function compareProduct(){
    	return view('shop.compare_product');
    }

     public function indexProduct(){
    	return view('shop.index_product');
    }

     public function showProduct(Request $request){
     	//return $request->id;
     	$products = Product::all()->take(10);
     	$product = Product::find($request->id);

    	return view('shop.show_product', compact('product', 'products'));
    }


    public function loginView(){
    	return view('shop.login');
    }

     public function login(Request $request){
     	$remember = $request->has('remember') ? true:false;
     	if(auth()->attempt(['email' => $request->email, 'password' => $request->password] , $remember)){
     		return redirect()->route('shop.home');

     	}else{
     		return redirect()->back();

     	}
     	

    	// return redirect()->route('shop.user.login');
    }

    public function registerView(){
    	return view('shop.register');
    }

     public function register(Request $request){

     	$this->validate($request, [
            
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'min:8|required',
            
        ]);
    	$user = new User;

    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->permissions_id = 12;
    	$user->save();

    	return redirect()->back()->with('success', 'created succssfally');


    }



    public function profile(){
    	return view('shop.profile');
    }

    public function blog(){
    	return view('shop.blog');
    }

     public function about(){
    	return view('shop.about');
    }

     public function contact(){
    	return view('shop.contact');
    }
}
