<?php
namespace App\Http\Controllers;

use App\DataTables\FillsDatatable;
use App\Http\Controllers\Controller;
use App\Model\Fill;
use Helper;
use View;
use App\WebmasterSection;
use Illuminate\Http\Request;
use Storage;

class FillsController extends Controller
{

   public function __construct()
   {
       //$this->middleware('auth');

       // Check Permissions
       /*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
           return Redirect::to(route('NoPermission'))->send();
       }*/

       // Share GeneralWebmasterSections
       $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
      View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index(FillsDatatable $trade)
   {
      return $trade->render('admin.fills.index', ['title' => trans('admin.fills')]);
   }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      return view('admin.fills.create', ['title' => trans('admin.add')]);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store()
   {

      $data = $this->validate(request(),
         [
            'name_ar' => 'required',
            'name_en' => 'required',


         ], [], [
            'name_ar' => trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),

         ]);

      Fill::create($data);
      session()->flash('success', trans('admin.record_added'));
      return redirect(Helper::aurl('fills'));
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      $fill = Fill::find($id);
      $title = trans('admin.edit');
      return view('admin.fills.edit', compact('fill', 'title'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $r, $id)
   {

      $data = $this->validate(request(),
         [
            'name_ar' => 'required',
            'name_en' => 'required',

         ], [], [
            'name_ar' => trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),

         ]);

      Fill::where('id', $id)->update($data);
      session()->flash('success', trans('admin.updated_record'));
      return redirect(Helper::aurl('fills'));
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      $fills = Fill::find($id);
      $fills->delete();
      session()->flash('success', trans('admin.deleted_record'));
      return redirect(Helper::aurl('fills'));
   }

   public function multi_delete()
   {
      if (is_array(request('item'))) {
         foreach (request('item') as $id) {
            $fills = Fill::find($id);
            $fills->delete();
         }
      } else {
         $fills = Fill::find(request('item'));
         $fills->delete();
      }
      session()->flash('success', trans('admin.deleted_record'));
      return redirect(Helper::aurl('fills'));
   }
}
