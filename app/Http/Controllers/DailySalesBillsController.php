<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\DailySalesBills;
use App\DataTables\DailySalesBillsDatatable;
use App\DailySalesBillsItems;
use App\ProductsStocks;
use App\WebmasterSection;
use DB;
use App\Currency;
use App\Branch;
use App\OprationsSections;
use App\Model\City;
use Helper;
use App\Model\Manufacturers;
use App\Model\Product;
use App\Model\EventCoordinator;
use Auth;
use View;
use Illuminate\Http\Request;
use Redirect;
use Carbon\Carbon;


class DailySalesBillsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check Permissions
        if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DailySalesBillsDatatable $DailySalesBills)
    {
        return $DailySalesBills->render('backend.dailysalesbills.index', ['title' => trans('backlang.all_daily_sales_bills')]);    }

    public function getAll(){
        
        $orders = DailySalesBills::all();
        return $orders;
    }
    public function orderList(Request $request){
        // $src =Auth::user()->permissionsGroup->opration_sec_id;
        $carbon = function ($date) {
            $date = new carbon($date);
            return $date;
        };

        $calc = function ($price,$qty,$dis) {
            $total = $price * $qty;
            if ($dis > 0) $total -= $total* ($dis /100);
            return $total;
        };


        if ($request->has('duration')) {
            $now = Carbon::now('EET');
            switch ($request->duration) {

                case 1 :

                    $timing = (Carbon::now('EET'))->subHour();

                    $salesBills = DailySalesBills::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();






                    break;

                case 2 :

                    $timing = (Carbon::now('EET'))->subHour(6);
                    $salesBills = DailySalesBills::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;

                case 3 :
                    $timing = (Carbon::now('EET'))->subDay();
                    $salesBills = DailySalesBills::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();

                    break;
                case 4 :
                    $timing = (Carbon::now('EET'))->subDay(7);
                    $salesBills = DailySalesBills::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;
            }
            return view("backEnd.dailysalesbills.orderlist",['salesBills'=>$salesBills,'carbon'=>new Carbon(),'date'=>$carbon,'calc'=>$calc]
            );
        } else {
            $salesBills = DailySalesBills::all();
        }

        
        return view("backEnd.dailysalesbills.orderlist",['salesBills'=>$salesBills,'carbon'=>new Carbon(),'date'=>$carbon,'calc'=>$calc]
        );
    }

    



    

    public function invoice($id) {
         $salesBills  = DailySalesBills::find($id);
      
        return view("backEnd.dailysalesbills.invoice",
            compact("salesBills"));
    }

    public function createView()
    {
     
        $src =Auth::user()->permissionsGroup->opration_sec_id;
        if($src == 7){
            $branches = OprationsSections::all(['id','name_ar','name_en'])->whereIn('id',[3,4,5,6])->toArray();

        }elseif($src == 6){
            $branches = OprationsSections::all(['id','name_ar','name_en'])->whereIn('id',[3,4,5])->toArray();
        }
        else{
            $branches = OprationsSections::all(['id','name_ar','name_en'])->toArray();
        }
        $ec = EventCoordinator::all(['id','name'])->toArray();
        $cities = $src;
        $items = Product::all(['id','title','code','price'])->toArray();
        $lastNumber = DailySalesBills::orderBy('id', 'desc')->first()['id'];
        $userId = Auth::user()->id;
        return view("backEnd.dailysalesbills.create",compact('branches','cities','ec','items','lastNumber','userId'));
    }

    public function createAction(Request $request,ProductsStocks $products_stock)
    {
        $orderData = $request->poData;
        $items = $request->items;

        $newOrder = new DailySalesBills();
        $newOrder = $newOrder::Create($orderData);

        $src =Auth::user()->permissionsGroup->opration_sec_id;

        if($newOrder){
            foreach ($items as $item) {
                $item['sales_bills_id'] = $newOrder->id;
                $newItem = new DailySalesBillsItems();
                $newItem = $newItem::Create($item);
            }
         
            }
     
            return redirect(Helper::aurl('sales-bills'));
    }

    public function updateView($id)
    {
        $branches = Branch::all(['id','name_ar','name_en'])->toArray();
        $ec = EventCoordinator::all(['id','name'])->toArray();
        $cities = City::all(['id','city_name_ar','city_name_en'])->toArray();
        $items = Product::all(['id','title'])->toArray();
        $lastNumber = DailySalesBills::orderBy('id', 'desc')->first()['id'];
        $userId = Auth::user()->id;
       
        return view("backEnd.dailysalesbills.edit",compact('branches','cities','ec','items','id','userId'));
    }

    public function updateAction(Request $request,$id)
    {


        
        $orderData = $request->poData;
        $items = $request->items;

        $order = DailySalesBills::find($id);
        $order->branch_id = $orderData['branch_id'];
        $order->number = $orderData['number'];
        $order->city_id = $orderData['city_id'];
        $order->ec_id = $orderData['ec_id'];
        $order->mobile = $orderData['mobile'];
        $order->event_date = $orderData['event_date'];
        $order->delivery_date = $orderData['delivery_date'];
        $order->client_name = $orderData['client_name'];
        $order->traies = $orderData['traies'];
        $order->notes = $orderData['notes'];
        $order->total = $orderData['total'];
        $order->paid = $orderData['paid'];
        $order->insurance = $orderData['insurance'];
        $order->residual = $orderData['residual'];
        $order->payment_status = $orderData['payment_status'];
        $order->ads_status = $orderData['ads_status'];
        $order->delivery_status = $orderData['delivery_status'];

        $order->save();

        if($order){
            foreach ($items as $item) {
                if(isset($item['id'])){
                    $itemRow = DailySalesBillsItems::find($item['id']);
                    $itemRow->product_id = $item['product_id'];
                    $itemRow->vendor_reference = $item['vendor_reference'];
                    $itemRow->qty = $item['qty'];
                    $itemRow->unit_price = $item['unit_price'];
                    $itemRow->save();
                }else {
                    $item['sales_bills_id'] = $order->id;
                    $newItem = new DailySalesBillsItems();
                    $newItem = $newItem::Create($item);
                }
                
            }
        }
        
        return $order;
    }

    public function removeItemFromOrder($id){
        $item = DailySalesBillsItems::find($id);
        if($item){
           $item->delete();
    }
    return $item;
    }

    public function getOrder($id)
    {
        $order = DailySalesBills::find($id);
        return $order;
    }

    public function destroy($id)
    {
          $item = DailySalesBills::find($id);
          if($item){
          $item->delete();
          return redirect('admin/daily-sales-bills');
        }
     }
     public function multi_delete()
     {
      
        if (is_array(request('item'))) {
           foreach (request('item') as $id) {
              $salesBills = DailySalesBills::find($id);
              $salesBills->delete();
           }
        } else {
           $salesBills = DailySalesBills::find(request('item'));
           $salesBills->delete();
        }
        session()->flash('success', trans('admin.deleted_record'));
        return redirect(Helper::aurl('sales-bills'));
      }
}
