<?php
namespace App\Http\Controllers;
use App\DataTables\EventCoordinatorsDatatable;
use App\Http\Controllers\Controller;
use App\Model\EventCoordinator;
use Helper;
use View;
use App\WebmasterSection;
use Illuminate\Http\Request;
use Storage;

class EventCoordinatorsController extends Controller {
	
	public function __construct()
    {
        //$this->middleware('auth');

        // Check Permissions
        /*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }*/

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(EventCoordinatorsDatatable $trade) {
		return $trade->render('admin.event_coordinators.index', ['title' => trans('admin.event_coordinators')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('admin.event_coordinators.create', ['title' => trans('admin.add')]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store() {


		$data = $this->validate(request(),
			[
				'name'      => 'required',
				'office'      => 'required',
				'phone'       => 'required|numeric',
				'commission'   => 'required|numeric',
				'city_id'   => 'required|numeric',
				'b_city_id'   => 'required|numeric',
				'start_date'      => 'sometimes|nullable',
				'end_date'     => 'sometimes|nullable',

			], [], [
				'name'      => trans('admin.name'),
				'office'      => trans('admin.office'),
				'address'      => trans('admin.address'),
				'phone'       => trans('admin.mobile'),
				'commission'        => trans('admin.commission'),
				'city_id'   => trans('admin.city_id'),
				'b_city_id'   => trans('admin.city_id'),
				'start_date'     => trans('admin.start_date'),
				'end_date'      => trans('admin.end_date'),

			]);



		EventCoordinator::create($data);
		session()->flash('success', trans('admin.record_added'));
		return redirect(Helper::aurl('event_coordinators'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$event_coordinators = EventCoordinator::find($id);
		$title    = trans('admin.edit');
		return view('admin.event_coordinators.edit', compact('event_coordinators', 'title'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $r, $id) {

		$data = $this->validate(request(),
			[
				'name'      => 'required',
				'office'      => 'required',
				'phone'       => 'required|numeric',
				'commission'   => 'required|numeric',
				'city_id'   => 'required|numeric',
				'b_city_id'   => 'required|numeric',
				'start_date'      => 'sometimes|nullable',
				'end_date'     => 'sometimes|nullable',

			], [], [
				'name'      => trans('admin.name'),
				'office'      => trans('admin.office'),
				'address'      => trans('admin.address'),
				'phone'       => trans('admin.mobile'),
				'commission'        => trans('admin.commission'),
				'city_id'   => trans('admin.city_id'),
				'b_city_id'   => trans('admin.city_id'),
				'start_date'     => trans('admin.start_date'),
				'end_date'      => trans('admin.end_date'),

			]);

		EventCoordinator::where('id', $id)->update($data);
		session()->flash('success', trans('admin.updated_record'));
		return redirect(Helper::aurl('event_coordinators'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$event_coordinators = EventCoordinator::find($id);
		$event_coordinators->delete();
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('event_coordinators'));
	}

	public function multi_delete() {
		if (is_array(request('item'))) {
			foreach (request('item') as $id) {
				$event_coordinators = EventCoordinator::find($id);
				$event_coordinators->delete();
			}
		} else {
			$event_coordinators = EventCoordinator::find(request('item'));
			$event_coordinators->delete();
		}
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('event_coordinators'));
	}
}
