<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\PurchaseOrders;
use App\DataTables\PurchaseOrdersDataTable;
use App\PurchaseOrdersItems;

use App\WebmasterSection;
use Carbon\Carbon;
use DB;
use App\Currency;
use App\Model\Manufacturers;
use App\Model\Product;
 
use Auth;
use View;
use Illuminate\Http\Request;
use Redirect;


class PurchaseOrdersController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');

        // Check Permissions
        /*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }*/

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PurchaseOrdersDataTable $PurchaseOrders)
    {
        return $PurchaseOrders->render('backend.purchaseorders.index', ['title' => trans('backlang.purchases')]);
    }

    public function getAll(){
        
        $orders = PurchaseOrders::all();
        return $orders;
    }

    public function createView()
    {
     
        $vendors = Manufacturers::all(['id','name_ar','name_en'])->toArray();
        $currencies = Currency::all(['id','code'])->toArray();
        $items = Product::all(['id','title','code','price'])->toArray();
        $lastNumber = PurchaseOrders::orderBy('id', 'desc')->first()['id'];

        return view("backEnd.purchaseorders.create",compact('vendors','currencies','items','lastNumber'));
    }

    public function createAction(Request $request)
    {
        $orderData = $request->poData;
        $items = $request->items;

        $newOrder = new PurchaseOrders();
        $newOrder = $newOrder::Create($orderData);

        if($newOrder){
            foreach ($items as $item) {
                $item['purchase_order_id'] = $newOrder->id;
                $newItem = new PurchaseOrdersItems();
                $newItem = $newItem::Create($item);
            }
        }
        
        return $newOrder;
    }

    public function updateView($id)
    {
        $vendors = Manufacturers::all(['id','name_ar','name_en'])->toArray();
        $currencies = Currency::all(['id','code'])->toArray();
        $items = Product::all(['id','title'])->toArray();

       
        return view("backEnd.purchaseorders.edit",compact('vendors','currencies','items','id'));
    }

    public function updateAction(Request $request,$id)
    {
        $orderData = $request->poData;
        $items = $request->items;

        $order = PurchaseOrders::find($id);
        $order->vendor_id = $orderData['vendor_id'];
        $order->number = $orderData['number'];
        $order->reference = $orderData['reference'];
        $order->date = $orderData['date'];
        $order->total = $orderData['total'];
        $order->terms = $orderData['terms'];
        $order->save();

        if($order){
            foreach ($items as $item) {
                if(isset($item['id'])){
                    $itemRow = PurchaseOrdersItems::find($item['id']);
                    $itemRow->product_id = $item['product_id'];
                    $itemRow->vendor_reference = $item['vendor_reference'];
                    $itemRow->qty = $item['qty'];
                    $itemRow->unit_price = $item['unit_price'];
                    $itemRow->save();
                }else {
                    $item['purchase_order_id'] = $order->id;
                    $newItem = new PurchaseOrdersItems();
                    $newItem = $newItem::Create($item);
                }
                
            }
        }
        
        return $order;
    }

    public function removeItemFromOrder($id){
        $item = PurchaseOrdersItems::find($id);
        if($item){
            $item->delete();
        }

        return $item;
    }

    public function destroy($id)
    {
        $del = PurchaseOrders::find($id);
            $del->delete();
            return $del;
    }

    public function getOrder($id)
    {
        $order = PurchaseOrders::find($id);
        return $order;
    }


    //orderlist
    public function orderList(Request $request){
        // $src =Auth::user()->permissionsGroup->opration_sec_id;
        $carbon = function ($date) {
            $date = new carbon($date);
            return $date;
        };




        if ($request->has('duration')) {
            $now = Carbon::now('EET');
            switch ($request->duration) {

                case 1 :

                    $timing = (Carbon::now('EET'))->subHour();

                    $PurchaseOrders = PurchaseOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();






                    break;

                case 2 :

                    $timing = (Carbon::now('EET'))->subHour(6);
                    $PurchaseOrders = PurchaseOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;

                case 3 :
                    $timing = (Carbon::now('EET'))->subDay();
                    $PurchaseOrders = PurchaseOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();

                    break;
                case 4 :
                    $timing = (Carbon::now('EET'))->subDay(7);
                    $PurchaseOrders = PurchaseOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;
            }
            return view("backEnd.PurchaseOrders.orderlist",
                ['PurchaseOrders'=>$PurchaseOrders,'carbon'=>new Carbon(),'date'=>$carbon]
            );
        } else {
            $PurchaseOrders = PurchaseOrders::all();
            return view("backEnd.PurchaseOrders.orderlist",
                ['PurchaseOrders'=>$PurchaseOrders,'carbon'=>new Carbon(),'date'=>$carbon]);
        }




    }

    // get the invoice of purchase
    public function invoice($id) {
        $purchaseOrder = PurchaseOrders::findOrFail($id);

        return view('backEnd.purchases.invoice',['purchaseOrder'=> $purchaseOrder]);
    }

}
