<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\SalesOrders;
use App\SalesOrdersItems;
use App\DataTables\SalesOrdersDataTable;
use App\WebmasterSection;
use Carbon\Carbon;
use DB;
use App\Currency;
use App\Clients;
use App\Model\City;
use App\Model\Product;

use Auth;
use View;
use Illuminate\Http\Request;
use Redirect;


class SalesOrdersController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');

        // Check Permissions
        /*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }*/

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SalesOrdersDataTable $SalesOrders)
    {
        return $SalesOrders->render('backend.salesorders.index', ['title' => trans('backlang.salesorders')]);
        
    }

    public function getAll(){
        
        $orders = SalesOrders::all();
        return $orders;
    }

    public function createView()
    {
     
        $clients = Clients::all(['id','name_ar','name_en','type'])->toArray();
        $cities = City::all(['id','city_name_ar','city_name_en'])->toArray();
        $currencies = Currency::all(['id','code'])->toArray();
        $items = Product::all(['id','title','code','price'])->toArray();
        $lastNumber = SalesOrders::orderBy('id', 'desc')->first()['id'];
        $userId = Auth::user()->id;
        return view("backEnd.salesorders.create",compact('clients','cities','currencies','items','lastNumber','userId'));
    }

    public function createAction(Request $request)
    {
        $orderData = $request->poData;
        // $orderDate = $request->user_id = Auth::user()->id;
        $items = $request->items;

        $newOrder = new SalesOrders();
        $newOrder = $newOrder::Create($orderData);

        if($newOrder){
            foreach ($items as $item) {
                $item['sales_order_id'] = $newOrder->id;
                $newItem = new SalesOrdersItems();
                $newItem = $newItem::Create($item);
            }
        }
        
        return $newOrder;
    }

    public function updateView($id)
    {
        $clients = Clients::all(['id','name_ar','name_en','type'])->toArray();
        $cities = City::all(['id','city_name_ar','city_name_en'])->toArray();
        $currencies = Currency::all(['id','code'])->toArray();
        $items = Product::all(['id','title'])->toArray();
        $userId = Auth::user()->id;
       
        return view("backEnd.salesorders.edit",compact('clients','cities','currencies','items','id','userId'));
    }

    public function updateAction(Request $request,$id)
    {
        $orderData = $request->poData;
        $items = $request->items;

        $order = SalesOrders::find($id);
        // $order->user_id = Auth::user()->id;
        $order->vendor_id = $orderData['vendor_id'];
        $order->number = $orderData['number'];
        $order->reference = $orderData['reference'];
        $order->date = $orderData['date'];
        $order->total = $orderData['total'];
        $order->terms = $orderData['terms'];
        $order->save();

        if($order){
            foreach ($items as $item) {
                if(isset($item['id'])){
                    $itemRow = SalesOrdersItems::find($item['id']);
                    $itemRow->product_id = $item['product_id'];
                    $itemRow->vendor_reference = $item['vendor_reference'];
                    $itemRow->qty = $item['qty'];
                    $itemRow->unit_price = $item['unit_price'];
                    $itemRow->save();
                }else {
                    $item['sales_order_id'] = $order->id;
                    $newItem = new SalesOrdersItems();
                    $newItem = $newItem::Create($item);
                }
                
            }
        }
        
        return $order;
    }

    public function removeItemFromOrder($id){
        $item = SalesOrdersItems::find($id);
        if($item){
            $item->delete();
        }

        return $item;
    }

    public function getOrder($id)
    {
        $order = SalesOrders::find($id);
        return $order;
    }

    // method to show the invoice for id 
    public function invoice($id) {
        $salesOrder = $this->getOrder($id);
        return view('backEnd.salesorders.invoice',['salesOrder'=>$salesOrder])->with('product');
    }

    public function orderList(Request $request){
        // $src =Auth::user()->permissionsGroup->opration_sec_id;
        $carbon = function ($date) {
            $date = new carbon($date);
            return $date;
        };




        if ($request->has('duration')) {
            $now = Carbon::now('EET');
            switch ($request->duration) {

                case 1 :

                    $timing = (Carbon::now('EET'))->subHour();

                    $SalesOrders = SalesOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();






                    break;

                case 2 :

                    $timing = (Carbon::now('EET'))->subHour(6);
                    $SalesOrders = SalesOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;

                case 3 :
                    $timing = (Carbon::now('EET'))->subDay();
                    $SalesOrders = SalesOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();

                    break;
                case 4 :
                    $timing = (Carbon::now('EET'))->subDay(7);
                    $SalesOrders = SalesOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;
            }
            return view("backEnd.SalesOrders.orderlist",['SalesOrders'=>$SalesOrders,'carbon'=>new Carbon(),'date'=>$carbon]
            );
        } else {
            $SalesOrders = SalesOrders::all();
            return view("backEnd.SalesOrders.orderlist",['SalesOrders'=>$SalesOrders,'carbon'=>new Carbon(),'date'=>$carbon])->with('client');
        }




    }

}
