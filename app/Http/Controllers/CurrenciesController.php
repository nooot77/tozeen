<?php
namespace App\Http\Controllers;
use App\DataTables\CurrencyDatatable;
use App\Http\Controllers\Controller;
use Helper;
use View;
use App\WebmasterSection;
use App\Model\Currency;
use Illuminate\Http\Request;
use Storage;

class CurrenciesController extends Controller {
	
	public function __construct()
	{
		//$this->middleware('auth');
 
		// Check Permissions
		/*if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
			return Redirect::to(route('NoPermission'))->send();
		}*/
 
		// Share GeneralWebmasterSections
		$GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	   View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(CurrencyDatatable $currency) {
		return $currency->render('admin.currencies.index', ['title' => trans('admin.currencies')]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('admin.currencies.create', ['title' => trans('admin.create_currencies')]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store() {

		$data = $this->validate(request(),
			[
				'name' => 'required',
				'code' => 'required',
				'decimal_place'             => 'required',
		
			], [], [
				'name' => trans('admin.currency_name'),
				'code' => trans('admin.code'),
				'decimal_place'             => trans('admin.mob'),
				
			]);


		Currency::create($data);
		session()->flash('success', trans('admin.record_added'));
		return redirect(Helper::aurl('currencies'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$currency = Currency::find($id);
		$title   = trans('admin.edit');
		return view('admin.currencies.edit', compact('currency', 'title'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $r, $id) {

		$data = $this->validate(request(),
		[
			'name' => 'required',
			'code' => 'required',
			'decimal_place'             => 'required',
			
		], [], [
			'name' => trans('admin.currency_name'),
			'code' => trans('admin.code'),
			'decimal_place'             => trans('admin.decimal_place'),		
		]);

		

		Currency::where('id', $id)->update($data);
		session()->flash('success', trans('admin.updated_record'));
		return redirect(Helper::aurl('currencies'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$currencies = Currency::find($id);

		$currencies->delete();
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('currencies'));
	}

	public function multi_delete() {
		if (is_array(request('item'))) {
			foreach (request('item') as $id) {
				$currencies = Currency::find($id);;
				$currencies->delete();
			}
		} else {
			$currencies = Currency::find(request('item'));
			
			$currencies->delete();
		}
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('currencies'));
	}
}
