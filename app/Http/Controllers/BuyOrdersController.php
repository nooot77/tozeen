<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\BuyOrders;
use App\DataTables\BuyOrdersDataTable;
use App\BuyOrdersItems;
use App\ProductsStocks;
use App\WebmasterSection;
use Carbon\Carbon;
use DB;

use App\Currency;
use App\Branch;
use App\OprationsSections;
use App\Model\City;
use App\Model\Manufacturers;
use App\Model\Product;
use App\Model\EventCoordinator;
use Auth;
use View;
use Helper;
use Illuminate\Http\Request;
use Redirect;


class BuyOrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        // Check Permissions
        if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }

    public function invoice($id) {
        $buy_orders = BuyOrders::find($id);
      
        return view("backEnd.BuyOrders.invoice",
            compact("buy_orders"));
    }

    public function orderList(Request $request){
        // $src =Auth::user()->permissionsGroup->opration_sec_id;
        $carbon = function ($date) {
            $date = new carbon($date);
            return $date;
        };




        if ($request->has('duration')) {
            $now = Carbon::now('EET');
            switch ($request->duration) {

                case 1 :

                    $timing = (Carbon::now('EET'))->subHour();

                    $BuyOrders = BuyOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();






                    break;

                case 2 :

                    $timing = (Carbon::now('EET'))->subHour(6);
                    $BuyOrders = BuyOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;

                case 3 :
                    $timing = (Carbon::now('EET'))->subDay();
                    $BuyOrders = BuyOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();

                    break;
                case 4 :
                    $timing = (Carbon::now('EET'))->subDay(7);
                    $BuyOrders = BuyOrders::where('created_at','>=',$timing)
                        ->Where('created_at','<=',$now)->get();
                    break;
            }
            return view("backEnd.BuyOrders.orderlist",['BuyOrders'=>$BuyOrders,'carbon'=>new Carbon(),'date'=>$carbon]
            );
        } else {
            $BuyOrders = BuyOrders::all();
            return view("backEnd.BuyOrders.orderlist",['BuyOrders'=>$BuyOrders,'carbon'=>new Carbon(),'date'=>$carbon])->with('client');
        }




    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BuyOrdersDatatable $BuyOrders)
    {
        return $BuyOrders->render('backEnd.BuyOrders.index', ['title' => trans('backlang.orders')]);
    }

    public function getAll(){
        
        $orders = BuyOrders::all();
        return $orders;
    }

    public function createView()
    {
     
        $src = Auth::user()->permissionsGroup->opration_sec_id;
        if($src == 6){
            $branches = OprationsSections::all(['id','name_ar','name_en'])->whereIn('id',[3,4,5])->toArray();

        }else{
            $branches = OprationsSections::all(['id','name_ar','name_en'])->where('id','=','6')->toArray();
        }
        $ec = EventCoordinator::all(['id','name'])->toArray();
        if($src == 6 || $src == 1 || $src == 2){
            $cities = City::all(['id','city_name_ar','city_name_en','country_id'])->where('country_id','=','170')->toArray();

        }else{
        $cities = City::all(['id','city_name_ar','city_name_en','country_id'])->where('country_id','=','170')->toArray();

        }
        $items = Product::all(['id','title','code','price','department_id'])->whereNotIn('department_id',[37,38,39])->toArray();
        $tries = Product::all(['id','title','code','price','department_id'])
        ->whereIn('department_id',[37,38,39])
        ->where('code','!=','null')->toArray();
        $lastNumber = BuyOrders::orderBy('id', 'desc')->first()['id'];
        $userId = Auth::user()->id;
        $branch =Auth::user()->permissionsGroup->opration_sec_id;

        return view("backEnd.BuyOrders.create",compact('branch','cities','ec','items','lastNumber','userId','tries'));
    }

    public function createAction(Request $request,ProductsStocks $products_stock)
    {
        $orderData = $request->poData;
        $items = $request->items;
        $tries = $request->tries;

        $newOrder = new BuyOrders();
        $newOrder = $newOrder::Create($orderData);

        // $src =Auth::user()->permissionsGroup->opration_sec_id;

       
        // get old stock
      $getOldStock = ProductsStocks::where('opration_sec_id', $orderData['branch_id'])->whereIn('product_id',$request->input('items.*.product_id'))->get();

    

       // get new stock
        $getNewStock = ProductsStocks::where('opration_sec_id', $orderData['city_id'])->whereIn('product_id',$request->input('items.*.product_id'))->get();



        foreach($items as $key => $value ) {

        // update old stock
       $oldStock = ProductsStocks::where('opration_sec_id', $orderData['branch_id'])->where('product_id', $value['product_id'])->update([
        'first_quantity' => $getOldStock[$key]->first_quantity - $value['qty'],
         'total_quantity' => $getOldStock[$key]->first_quantity - $value['qty'] + $getOldStock[$key]->warehouse_quantity + $getOldStock[$key]->events_quantity 
        ]);

      

        // update new stock
       $newStock = ProductsStocks::where('opration_sec_id', $orderData['city_id'])->where('product_id', $value['product_id'])->update([
        'first_quantity' => $getNewStock[$key]->first_quantity + $value['qty'],
        'total_quantity' => $getNewStock[$key]->first_quantity + $value['qty']  + $getNewStock[$key]->warehouse_quantity + $getNewStock[$key]->events_quantity 

        ]);

       }
        if($newOrder){
            foreach ($items as $item) {
                $item['buy_orders_id'] = $newOrder->id;
                $newItem = new BuyOrdersItems();
                $newItem = $newItem::Create($item);
            }
            foreach ($tries as $trie) {
                $trie['buy_orders_id'] = $newOrder->id;
                $newTrie = new BuyOrdersItems();
                $newTrie = $newTrie::Create($trie);
            }
         
        }
       

  

       
        
        return redirect(Helper::aurl('buy-orders'));
    }

    public function updateView($id)
    {
        $branches = OprationsSections::all(['id','name_ar','name_en'])->where('id','=','6')->toArray();
        $ec = EventCoordinator::all(['id','name'])->toArray();
        $cities = City::all(['id','city_name_ar','city_name_en'])->toArray();
        $items = Product::all(['id','title'])->toArray();
        $lastNumber = BuyOrders::orderBy('id', 'desc')->first()['id'];
        $userId = Auth::user()->id;
       
        return view("backEnd.BuyOrders.edit",compact('branches','cities','ec','items','id','userId'));
    }

    public function updateAction(Request $request,$id)
    {


        
        $orderData = $request->poData;
        $items = $request->items;

        $order = BuyOrders::find($id);
        $order->branch_id = $orderData['branch_id'];
        $order->number = $orderData['number'];
        $order->city_id = $orderData['city_id'];
        $order->ec_id = $orderData['ec_id'];
        $order->mobile = $orderData['mobile'];
        $order->event_date = $orderData['event_date'];
        $order->delivery_date = $orderData['delivery_date'];
        $order->client_name = $orderData['client_name'];
        $order->traies = $orderData['traies'];
        $order->notes = $orderData['notes'];
        $order->total = $orderData['total'];
        $order->paid = $orderData['paid'];
        $order->insurance = $orderData['insurance'];
        $order->residual = $orderData['residual'];
        $order->payment_status = $orderData['payment_status'];
        $order->ads_status = $orderData['ads_status'];
        $order->delivery_status = $orderData['delivery_status'];

        $order->save();

        if($order){
            foreach ($items as $item) {
                if(isset($item['id'])){
                    $itemRow = BuyOrdersItems::find($item['id']);
                    $itemRow->product_id = $item['product_id'];
                    $itemRow->vendor_reference = $item['vendor_reference'];
                    $itemRow->qty = $item['qty'];
                    $itemRow->unit_price = $item['unit_price'];
                    $itemRow->save();
                }else {
                    $item['sales_bills_id'] = $order->id;
                    $newItem = new BuyOrdersItems();
                    $newItem = $newItem::Create($item);
                }
                
            }
        }
        
        return $order;
    }

    public function removeItemFromOrder($id){
        $item = BuyOrdersItems::find($id);
        if($item){
            $item->delete();
        }

        return $item;
    }

    public function destroy($id)
    {
        $del = BuyOrders::find($id);
        $del->delete();
        return redirect(Helper::aurl('buy-orders'));
    }

    public function getOrder($id)
    {
        $order = BuyOrders::find($id);
        return $order;
    }

}
