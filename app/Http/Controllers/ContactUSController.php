<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ContactUS;
use App\Mail\ContactUsMail;
use App\Model\Setting;
use Carbon\Carbon;
use DB;
use Mail;
class ContactUSController extends Controller
{
    public function contactUS()
   {
      
       return view('website.contact');
   }
   
   public function contactUsPost(Request $request){
   $data =  $this->validate($request, [
        'name' => 'required',
        'title' => 'required',
		'phone' => 'required',    
        'email' => 'required|email',
        'message' => 'required'
        ]);
        // dd($request->all());
     ContactUS::create($data);
    //  ContactUS::create($request->all()); 
      $email = DB::table('settings')->where('id', '1')->value('email');
     
       Mail::to($email)->send(
           new ContactUsMail(
               [
            'name' => $request->get('name'),
           'email' => $request->get('email'),
           'phone' => $request->get('phone'),


		   'title' => $request->get('title'),
           'message' => $request->get('message') 
               
               ]
        ) );
        session()->flash('success', trans('admin.done'));
			return back();

   }
 
}

