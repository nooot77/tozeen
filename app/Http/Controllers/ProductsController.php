<?php
namespace App\Http\Controllers;
use App\DataTables\ProductsDatatable;
use App\DataTables\TriesDatatable;
use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\Size;
use App\Model\Weight;
use App\Model\Fill;
use App\WebmasterSection;
use Illuminate\Http\Request;
use  Exception, File, Storage;
use View;
use Helper;
class ProductsController extends Controller {
	public $output;
	public $depId;
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct()
    {
       // this to make it available only for members
        $this->middleware('auth');

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);

    }
	public function index(ProductsDatatable $product) {
		
		// General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        // General END
		return $product->render('admin.products.index', ['title' => trans('backLang.products')]);
	}

	public function alltries(TriesDatatable $product) {
		
		// General for all pages
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
        // General END
		return $product->render('admin.products.index', ['title' => trans('backLang.tries')]);
	}
	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$product = Product::create(['title'=>'']);
		if(!empty($product)) 
		{
			return redirect(Helper::aurl('products/'.$product->id.'/edit'));
		} 
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store() {
   
		$data = $this->validate(request(),

		
		[
			'title'             => 'required',
			'content'           => 'sometimes|nullable',
			'code'              => 'required|numeric',
			'department_id'     => 'required|numeric',
			'fill_id'           => 'required|numeric',
			'price'             => 'required|numeric',
			'price_offer'       => 'required|numeric',
			'stock'             => 'required|numeric',	
			'trade_id'          => 'sometimes|nullable',
			'manu_id'           => 'sometimes|nullable',
			'country_id'        => 'sometimes|nullable',
			'status'            => 'sometimes|nullable',
			'start_at'          => 'sometimes|nullable',
			'end_at'            => 'sometimes|nullable',
			'start_offer_at'    => 'sometimes|nullable',
			'end_offer_at'      => 'sometimes|nullable',
			'cal'               => 'sometimes|nullable',
			'exp_date'          => 'sometimes|nullable',
			'prd_date'          => 'sometimes|nullable',
			'size'              => 'sometimes|nullable',
			'size_id'           => 'sometimes|nullable',
			'weight'            => 'sometimes|nullable',
			'weight_id'         => 'sometimes|nullable',


		], [], [
			
			'title'             => trans('admin.title'),
			'content' 	    	=> trans('admin.content'),
			'code'          	=> trans('admin.pcode'),
			'department_id'     => trans('admin.department'),
			'fill_id'           => trans('admin.fill'),
			'price'             => trans('admin.price'),
			'price_offer'       => trans('admin.price_offer'),
			'trade_id'          => trans('admin.trademark'),
			'manu_id'           => trans('admin.manufacturer'),
			'country_id'        => trans('admin.country'),
			'status'            => trans('admin.status'),
			'stock'             => trans('admin.stock'),
			'start_at'          => trans('admin.start_at'),
			'end_at'            => trans('admin.end_at'),
			'start_offer_at'    => trans('admin.start_offer_at'),
			'end_offer_at'      => trans('admin.end_offer_at'),
			'cal'               => trans('admin.cal'),
			'exp_date'          => trans('admin.exp_date'),
			'prd_date'          => trans('admin.prd_date'),
			'size'              => trans('admin.size'),
			'size_id'           => trans('admin.size'),
			'weight'            => trans('admin.weight'),
			'weight_id'         => trans('admin.weight'),

		]);

        //  Product::where('id', $id)->update($data);
		// Product::create($data);
		// session()->flash('success', trans('admin.record_added'));
		// return redirect(Helper::aurl('products'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}
   
	  
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		// dd(request()->route('product'));exit;
	
		$product = Product::find($id);
		return view('admin.products.product',
		 [
		 'title' => trans('admin.create_or_edit_product',['title'=>$product->title]),
		 'product'=>$product
		 ]);
		// return view('admin.products.edit', compact('product', 'title'));
	}

		public function upload_file($id){
			
				if (request()->hasFile('file')) {
				return  Helper::up()->upload([
						'file'        => 'file',
						'path'        => 'products/'.$id,
						'file_type'   => 'product',
						'upload_type' => 'files',
						'relation_id' => $id,
					]);
					return response(['status'=>true,'id'=>$file_id],200);
			}     
		}

		public function update_product_image($id){
			$product = Product::where('id',$id)->update([
				'photo' =>  Helper::up()->upload([
					'file'        => 'file',
					'path'        => 'products/'.$id,
					'upload_type' => 'single',
					'delete_file' => '',
				]),
			]);
			// 'photo'=>$product->photo
			return response(['status'=>true],200);
		}
		
		public function delete_file(){
	      
                if (request()->has('id')) {
					Helper::up()->delete(request('id'));
			}     
		}

		public function delete_main_image($id){
					$product = Product::find($id);
					Storage::delete($product->photo);
					$product->photo = null;
					$product->save(); 
			
			return response(['status'=>true],200);
		}
		public function prepare_weight_size(){

			if( request()->ajax() and request()->has('dep_id')){
			  $dep_list = array_diff(explode(',',Helper::get_parent(request('dep_id'))),[request('dep_id')]);
			 
			  $sizes = Size::where('is_public','yes')
			  ->whereIn('department_id',$dep_list)
			  ->orWhere('department_id',request('dep_id'))
			  ->pluck('name_'.session('locale'),'id');
			
			  $weights = Weight::pluck('name_'.session('locale'),'id');
			  return view('admin.products.ajax.size_weight',[
				  'sizes'=>$sizes,
				  'weights'=>$weights,
				  'product'=>product::find(request('product_id')),
				  ]);
			}else{
				return 'الرجاء اختيار القسم';
			}
		}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update($id) {
 
		$data = $this->validate(request(),

		
		[
			'title'             => 'required',
			'content'           => 'sometimes|nullable',
			'code'              => 'required|numeric',
			'department_id'     => 'required|numeric',
			'fill_id'           => 'required|numeric',
			'price'             => 'required|numeric',
			'price_offer'       => 'required|numeric',
			'stock'             => 'required|numeric',	
			'trade_id'          => 'sometimes|nullable',
			'manu_id'           => 'sometimes|nullable',
			'country_id'        => 'sometimes|nullable',
			'status'            => 'sometimes|nullable',
			'start_at'          => 'sometimes|nullable',
			'end_at'            => 'sometimes|nullable',
			'start_offer_at'    => 'sometimes|nullable',
			'end_offer_at'      => 'sometimes|nullable',
			'cal'               => 'sometimes|nullable',
			'exp_date'          => 'sometimes|nullable',
			'prd_date'          => 'sometimes|nullable',
			'size'              => 'sometimes|nullable',
			'size_id'           => 'sometimes|nullable',
			'weight'            => 'sometimes|nullable',
			'weight_id'         => 'sometimes|nullable',


		], [], [
			
			'title'             => trans('admin.title'),
			'content' 	    	=> trans('admin.content'),
			'code'          	=> trans('admin.pcode'),
			'department_id'     => trans('admin.department'),
			'fill_id'           => trans('admin.fill'),
			'price'             => trans('admin.price'),
			'price_offer'       => trans('admin.price_offer'),
			'trade_id'          => trans('admin.trademark'),
			'manu_id'           => trans('admin.manufacturer'),
			'country_id'        => trans('admin.country'),
			'status'            => trans('admin.status'),
			'stock'             => trans('admin.stock'),
			'start_at'          => trans('admin.start_at'),
			'end_at'            => trans('admin.end_at'),
			'start_offer_at'    => trans('admin.start_offer_at'),
			'end_offer_at'      => trans('admin.end_offer_at'),
			'cal'               => trans('admin.cal'),
			'exp_date'          => trans('admin.exp_date'),
			'prd_date'          => trans('admin.prd_date'),
			'size'              => trans('admin.size'),
			'size_id'           => trans('admin.size'),
			'weight'            => trans('admin.weight'),
			'weight_id'         => trans('admin.weight'),

		]);

	

		Product::where('id', $id)->update($data);
		return response(['status'=>true,'message'=> trans('admin.updated_record'),200]);
	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$products = Product::find($id);
		// Storage::delete($products->logo);
		$products->delete();
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('products'));
	}

	public function multi_delete() {
		if (is_array(request('item'))) {
			foreach (request('item') as $id) {
				$products = Product::find($id);
				// Storage::delete($products->logo);
				$products->delete();
			}
		} else {
			$products = Product::find(request('item'));
			// Storage::delete($products->logo);
			$products->delete();
		}
		session()->flash('success', trans('admin.deleted_record'));
		return redirect(Helper::aurl('products'));
	}


	public function getImg() {
		$products = Product::paginate(20);
		$product = Product::all();

		return view('admin.products.get_img', compact('products', 'product'));
	}

	public function getSearch(Request $request) {
		$builder = Product::query();
		$term = $request->all();

		if(!empty($term['depId'])){
			$builder->where('department_id','like', $term['depId']);
		}



		if(!empty($term['imgSearch'])){
			$builder->where('title','like', '%'.$term['imgSearch'].'%')->orWhere('code','like', '%'.$term['imgSearch'].'%');
		}


		$results = $builder->orderBy('id')->get();

		 foreach($results as $result) {

       



        $this->output .= " <div class='col-md-2 m-a'>
          <div class='img-conetn-manage mt-3'>
            <h6 class='text-dark'>".$result->title."</h6>
            <p>".$result->code."</p>

       
            <img width='100%' height='100%' src='".Storage::url($result->photo)."'>  
            


            <div class='content-icon'>



              <a href='#' type='button'  data-toggle='modal' data-target='#exampleModalCenter".$result->id."'> <i class='far fa-trash-alt'></i> Zoom in</a>



            </div>
          </div>

        </div>

        


        
        <div class='modal fade' id='exampleModalCenter".$result->id."' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'>
          <div class='modal-dialog modal-dialog-centered' role='document'>
            <div class='modal-content'>
              <div class='modal-header'>
                <h5 class='modal-title' id='exampleModalCenterTitle'>". $result->title."   || ".$result->code."</h5>
                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                  <span aria-hidden='true'>&times;</span>
                </button>
              </div>
              <div class='modal-body'>

               
               <img width='100%' height='100%' src='".Storage::url($result->photo)."'>  
               

             </div>
             <div class='modal-footer'>
              <button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>

            </div>
          </div>
        </div>
      </div>";

        
    


    }

    if($results->count() < 1){

    			$data = ['result' => '<h3 class="text-danger text-center">result not found</h3>'];

				return json_encode($data);
    		} else{



    
   
			$data = ['result' => $this->output];

		return json_encode($data);
		}




	}
}
