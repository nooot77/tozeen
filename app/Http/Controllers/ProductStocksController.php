<?php
namespace App\Http\Controllers;

use App\DataTables\ProductStocksDatatable;
use App\DataTables\ProductStocksNonRegisteredDatatable;
use App\DataTables\ProductStocksRegisteredDatatable;
use App\Http\Controllers\Controller;
use App\Model\Size;
use Auth;
use Helper;
use View;
use App\WebmasterSection;
use Illuminate\Http\Request;
use Storage;
use App\BuyOrders;
use App\DataTables\BuyOrdersDataTable;
use App\BuyOrdersItems;
use App\ProductsStocks;
use DB;
use App\Currency;
use App\Branch;
use App\OprationsSections;
use App\Model\City;
use App\Model\Manufacturers;
use App\Model\Product;
use App\Model\EventCoordinator;
use Redirect;


class ProductStocksController extends Controller
{

   public function __construct()
    {
        $this->middleware('auth');

        // Check Permissions
        if (@Auth::user()->permissions != 0 && Auth::user()->permissions != 1) {
            return Redirect::to(route('NoPermission'))->send();
        }

        // Share GeneralWebmasterSections
        $GeneralWebmasterSections = WebmasterSection::where('status', '=', '1')->orderby('row_no', 'asc')->get();
	    View::share(['GeneralWebmasterSections' => $GeneralWebmasterSections]);
    }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function x(ProductStocksDatatable $stocks,$id)
   {
      return $stocks->with('id',$id)->render('admin.ProductStocks.index', ['title' => trans('admin.ProductStocks')]);
   }

   public function allNonRegProductStocks(ProductStocksNonRegisteredDatatable $pnStocks)
   {
      return $pnStocks->render('admin.productStocks.index', ['title' => trans('admin.productStocks')]);
   }

   public function allRegProductStocks(ProductStocksRegisteredDatatable $pStocks)
   {
      return $pStocks->render('admin.productStocks.index', ['title' => trans('admin.ProductStocks')]);
   }

   public function createView(Request $request)
    {
     
      //   $src = Auth::user()->permissionsGroup->opration_sec_id;
       
        $branches = OprationsSections::all(['id','name_ar','name_en'])->toArray();
        $orderData = $request->poData;
      //   $ec = EventCoordinator::all(['id','name'])->toArray();
      //   if($src == 6 || $src == 1 || $src == 2){
      //       $cities = OprationsSections::all(['id','name_ar','name_en'])->toArray();
      //   }else{
      //   $cities = OprationsSections::all(['id','name_ar','name_en'])->where('id','=',$src)->toArray();
      //   }
      // $items = Product::all(['id','title','code','price','department_id'])->toArray();

      $items = DB::table('products')
      ->join('products_stocks','products.id','=','products_stocks.product_id')
      ->select('products_stocks.id','products_stocks.product_id','products_stocks.first_quantity','products_stocks.opration_sec_id','products.title','products.code','products.price')
      ->where('first_quantity','<=','0')
      ->get();
      

      // dd($items);exit;
      // ->where('opration_sec_id','=',$orderData['branch_id'])
      //   $items = ProductsStocks::all(['id','product_id','first_quantity','opration_sec_id'])
      //   ->where(
      //      ['opration_sec_id','=',$orderData['branch_id']],
      //      ['first_quantity','<=','0']
      //      )->leftJoin('products', 'ProductsStocks.product_id', '=', 'products.id')
      //      ->toArray();
    
        return view("admin.productStocks.addStockQty",compact('branches','cities','ec','items','lastNumber','userId'));
    }

    public function createAction(Request $request,ProductsStocks $products_stock)
    {
        $orderData = $request->poData;
        $items = $request->items;
       

      //   $newOrder = new ProductsStocks();
      //   $newOrder = $newOrder::Create($orderData);

        // $src =Auth::user()->permissionsGroup->opration_sec_id;

       
        // get old stock
       $getOldStock =   DB::table('products_stocks')
       ->join('products','products_stocks.product_id','=','products.id')
       ->select('products_stocks.id','products_stocks.product_id','products_stocks.first_quantity','products_stocks.opration_sec_id','products.title','products.code','products.price')
       ->where('opration_sec_id', $orderData['branch_id'])
       ->whereIn('product_id',$request->input('items.*.product_id'))
       ->get();

        foreach($items as $key => $value ) {
        // update old stock
       $oldStock = ProductsStocks::where('opration_sec_id', $orderData['branch_id'])->where('product_id', $value['product_id'])->get();
        if($getOldStock){
         $oldStock = ProductsStocks::where('opration_sec_id', $orderData['branch_id'])->where('product_id', $value['product_id'])->update([
            'first_quantity' => $getOldStock[$key]->first_quantity+ $value['qty'],
            ]);
        }

      return ['redirect' => Helper::aurl('allRegProductStocks')];
     
    }
   }

   

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
      return view('admin.ProductStocks.create', ['title' => trans('admin.add')]);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store()
   {

      $data = $this->validate(request(),
         [
            'name_ar' => 'required',
            'name_en' => 'required',
            'department_id'   => 'required|numeric',
            'is_public'   => 'required|in:yes,no',


         ], [], [
            'name_ar' => trans('admin.name_ar'),
            'name_en' => trans('admin.name_en'),
            'department_id'   => trans('admin.department_id'),
            'is_public'   => trans('admin.is_public'),

         ]);

      Size::create($data);
      session()->flash('success', trans('admin.record_added'));
      return redirect(Helper::aurl('ProductStocks'));
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
      //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function edit($id)
   {
      $size = Size::find($id);
      $title = trans('admin.edit');

      return view('admin.ProductStocks.edit', compact('size', 'title'));
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $r, $id)
   {

     $data = $this->validate(request(),
        [
           'name_ar' => 'required',
           'name_en' => 'required',
           'department_id'   => 'required|numeric',
           'is_public'   => 'required|in:yes,no',


        ], [], [
           'name_ar' => trans('admin.name_ar'),
           'name_en' => trans('admin.name_en'),
           'department_id'   => trans('admin.department_id'),
           'is_public'   => trans('admin.is_public'),

        ]);

      Size::where('id', $id)->update($data);
      session()->flash('success', trans('admin.updated_record'));
      return redirect(Helper::aurl('ProductStocks'));
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      // $ProductStocks = Size::find($id);
      // $ProductStocks->delete();
      // session()->flash('success', trans('admin.deleted_record'));
      // return redirect(Helper::aurl('ProductStocks'));
   }

   public function multi_delete()
   {
   //    if (is_array(request('item'))) {
   //       foreach (request('item') as $id) {
   //          $ProductStocks = Size::find($id);
   //          $ProductStocks->delete();
   //       }
   //    } else {
   //       $ProductStocks = Size::find(request('item'));
   //       $ProductStocks->delete();
   //    }
   //    session()->flash('success', trans('admin.deleted_record'));
   //    return redirect(Helper::aurl('ProductStocks'));
    }
}
