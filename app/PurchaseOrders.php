<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrders extends Model
{
    public function toArray(){
        $order = Parent::toArray();
        $order['order_items'] = $this->orderItems;
        return $order;
    }

    protected $fillable = [
        'user_id',
        'vendor_id',
        'number',
        'reference',
        'date',
        'subtotal',
        'discount',
        'taxs',
        'total',
        'terms',
        'status_id',
        'is_received',
    ];

    public function orderItems()
    {
        return $this->hasMany('App\PurchaseOrdersItems','purchase_order_id');
    }

    public function vendor() {

        return $this->belongsTo('App\Model\Manufacturers','vendor_id');
    }


}