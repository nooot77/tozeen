<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsStocks extends Model
{

    protected $fillable = [
        'opration_sec_id',
        'sec_src_id',
        'product_id',
        'first_quantity',
        'warehouse_quantity',
        'events_quantity',
        'remaining_quantity',
        'total_quantity',
        'event_date',

    ];

    public function oprationsSections()
    {
        return $this->belongsTo('App\OprationsSections', 'opration_sec_id');
    }

    public function proStocks()
    {
        return $this->hasMany('App\Model\Product','product_id');
    }

    public function product_name()
    {
      return $this->hasOne('App\Model\Product','id','product_id');
    }
    public function os_name()
    {
      return $this->hasOne('App\OprationsSections', 'id','opration_sec_id');
    }

}