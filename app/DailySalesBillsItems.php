<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailySalesBillsItems extends Model
{
    protected $fillable = [
        'daily_sales_bills_id',
        'product_id',
        'vendor_reference',
        'qty',
        'qty_received',
        'free_kilos',
        'unit_price',
        'discount',
    ];

    public function product() {
        return $this->belongsTo('App\Model\Product');
    }
}