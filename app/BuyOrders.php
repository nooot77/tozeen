<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BuyOrders extends Model
{
    use SoftDeletes;

    public function toArray(){
        $order = Parent::toArray();

        $order['order_items'] = $this->orderItems;

        return $order;
    }

    protected $fillable = [
        'number',
        'branch_id',
        'user_id',
        'city_id',
        'ec_id',
        'mobile',
        'event_date',
        'client_name',
        'traies',
        'notes',
        'total',
        'paid',
        'insurance',
        'residual',
        'delivery_date',
        'payment_status',
        'ads_status',
        'delivery_status',
        

    ];

    public function orderItems()
    {
        return $this->hasMany('App\BuyOrdersItems','buy_orders_id');
    }
    public function branch_id() {
        return $this->hasOne('App\OprationsSections', 'id', 'branch_id');
    }
    public function city_id() {
        return $this->hasOne('App\OprationsSections', 'id', 'city_id');
    }
    public function city() {
        return $this->belongsTo('App\OprationsSections', 'city_id');
    }
    public function branch() {
        return $this->belongsTo('App\OprationsSections', 'branch_id');
    }
    public function ec() {
        return $this->belongsTo('App\Model\EventCoordinator', 'ec_id');
    }
    protected $dates = ['deleted_at'];
}